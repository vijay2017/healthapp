package com.obhaiyya.health

import android.content.Context
import android.net.ConnectivityManager
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.widget.Toast
import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho

/**
 * Created by Vijay on 10/3/20.
 */

class HealthApplication : MultiDexApplication() {

    companion object {

        private var sMainThreadHandler: Handler? = null

        var mAppContext: HealthApplication? = null

        fun getAppInstance(): HealthApplication? {
            return mAppContext
        }

        fun showToast(msg: String?) {
            fun showToast(message: String?, duration: Int) {
                getMainThreadHandler()
                    ?.post(Runnable {
                        if (!TextUtils.isEmpty(message)) {
                            Toast.makeText(
                                getContext(),
                                message,
                                duration
                            ).show()
                        }
                    })
            }
        }


        fun getContext(): Context? {
            return getAppInstance()?.getApplicationContext()
        }

        /**
         * @return a [Handler] tied to the main thread.
         */
        fun getMainThreadHandler(): Handler? {
            if (HealthApplication.sMainThreadHandler == null) {
                // No need to synchronize -- it's okay to create an extra Handler,
                // which will be used only once and then thrown away.
                HealthApplication.sMainThreadHandler =
                    Handler(Looper.getMainLooper())
            }
            return HealthApplication.sMainThreadHandler
        }


    }

    override fun onCreate() {
        super.onCreate()
        mAppContext = this
        Stetho.initializeWithDefaults(this)
//        CrashlyticsWrapper.initCrashLytics(this);
    }


    fun isNetworkConnected(): Boolean {
        val cm = getAppInstance()
            ?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

}