package com.obhaiyya.health.data

import android.content.Context
import android.content.SharedPreferences
import com.obhaiyya.health.HealthApplication
import com.obhaiyya.health.R


/**
 * Created by Vijay on 3/12/19.
 */

object SharedPreferenceManager {

    // key used in the app
    val KEY_SHOULD_SHOW_NOT_WALKTHROUGH = "shouldShowWalkthrough"
    val KEY_USER_LOGGED_IN = "keyUserLoggedIn"
    val KEY_MOBILE_NUMBER = "keyUserMobileNumber"
    val KEY_PAYMENT_SUCCESS = "keyPaymentSuccess"
    val KEY_SHOULD_SHOW_NOT_USER_DETAILS = "shouldShowUserDetails"
    val KEY_SHOULD_SHOW_NOT_Package = "shouldShowPackage"
    val KEY_SHOULD_SHOW_NOT_DASHBOARD = "shouldShowDashboard"
    val KEY_USER_EMAIL = "keyUserEmail"
    val KEY_USER_ID = "keyUserId"
    val KEY_USER_DETAILS = "keyUserDetails"
    val KEY_ACCESS_CODE = "keyAccessCode"
    val KEY_SHOULD_SHOW_ONBOARD = "shouldShowOnboard"
    val KEY_USER_STATUS_ACTIVE = ""


    var sharedPreferenceManger: SharedPreferenceManager? = null
    var sharedPreferences: SharedPreferences? = null

    init {
        sharedPreferences = HealthApplication.getAppInstance()
            ?.getSharedPreferences(
                HealthApplication.getAppInstance()?.getString(R.string.preference_name),
                Context.MODE_PRIVATE
            )
    }

    fun saveString(key: String, value: String) {
        val editor = sharedPreferences?.edit()
        editor?.putString(key, value)
        editor?.commit()
        editor?.apply()
    }

    fun containsKey(key: String): Boolean? {
        return sharedPreferences?.contains(key)
    }

    fun getString(key: String): String? {
        return sharedPreferences?.getString(key, "")
    }

    fun saveInt(key: String, value: Int) {
        sharedPreferences?.edit()?.putInt(key, value)?.apply()
    }

    fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences?.edit()?.putBoolean(key, value)?.apply()
    }

    fun getBoolean(key: String): Boolean? {
        return sharedPreferences?.getBoolean(key, false)
    }

    fun saveLong(key: String, value: Long) {
        sharedPreferences?.edit()?.putLong(key, value)?.apply()
    }

    fun getLong(key: String): Long? {
        return sharedPreferences?.getLong(key, 0L)
    }

    fun getInt(key: String): Int? {
        return sharedPreferences?.getInt(key, 0)
    }

    fun clearData() {
        val editor = sharedPreferences?.edit()
        editor?.clear()
        editor?.commit()
    }

}