package com.appwithmeflutter.mype.data.network


import com.obhaiyya.health.presentation.PaymentAppIdResponse
import com.obhaiyya.health.presentation.dashboard.GetAllDashboardHealthPlanResponse
import com.obhaiyya.health.presentation.dashboard.TodayDashboardResponse
import com.obhaiyya.health.presentation.dashboard.ValidateAccessCodeRequest
import com.obhaiyya.health.presentation.history.HistoryResponse
import com.obhaiyya.health.presentation.history.LogsResponse
import com.obhaiyya.health.presentation.history.TrendsLogsResponse
import com.obhaiyya.health.presentation.login.GoogleLoginResponse
import com.obhaiyya.health.presentation.login.LoginResponse
import com.obhaiyya.health.presentation.login.OTPResponse
import com.obhaiyya.health.presentation.navigation.GetNavPackageResponse
import com.obhaiyya.health.presentation.onboard.OnboardQuestionResponse
import com.obhaiyya.health.presentation.onboard.UsersAnswerRequest
import com.obhaiyya.health.presentation.onboard.UsersAnswerResponse
import com.obhaiyya.health.presentation.paygateway.NotifyPaymentResponse
import com.obhaiyya.health.presentation.paygateway.TokenOtherDataModel
import com.obhaiyya.health.presentation.posthealthquestion.CheckRiskRequest
import com.obhaiyya.health.presentation.posthealthquestion.CheckRiskAssesmentResponse
import com.obhaiyya.health.presentation.scanner.HealthDataRequest
import com.obhaiyya.health.presentation.scanner.HealthDataResponse
import com.obhaiyya.health.presentation.scanner.LicenseKeyResponse
import com.obhaiyya.health.presentation.Package.*
import com.obhaiyya.health.presentation.dashboard.GetHealthScoreResponse
import com.obhaiyya.health.presentation.userdetails.GetUserDetailsResponse
import com.obhaiyya.health.presentation.userdetails.UserDetailsRequest
import com.obhaiyya.health.presentation.userdetails.UserRegistrationGmailResponse
import io.reactivex.rxjava3.core.Observable
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Vijay on 7/3/20.
 */
interface HealthService {

    @GET("users/otp/generate/{mobileNumber}")
    fun generateMobileNumber(@Path("mobileNumber") mobileNumber: String): Observable<LoginResponse>

    @GET("users/otp/validate/{mobileNumber}/{otp}")
    fun validateOTP(
        @Path("mobileNumber") mobileNumber: String,
        @Path("otp") otp: String
    ): Observable<OTPResponse>

    @GET("/payment/generate/orderId/for/gpay/{userId}/{amount}")
    fun getTxIdForGpay(
        @Path("userId") userId: String,
        @Path("amount") amount: String
    ): Call<TokenOtherDataModel>

    @POST("/payment/success/update/{orderId}")
    fun notifyBackendPaymentSuccessNew(
//        @Header("AccessToken") token: String,
        @Path("orderId") orderId: String,
        @Body requestBody: RequestBody
    ): Call<PaymentGatewayResponse>

    @GET("/users/gmail/{email}/{gmailToken}")
    fun registerGmailApi(
        @Path("email") email: String,
        @Path("gmailToken") gmailToken: String
    ): Observable<GoogleLoginResponse>

    @POST("vendorBackend/services/vendorPaymentsService/paymentFailDataUpdate/{orderId}")
    fun notifyBackendPaymentFailure(
        @Header("AccessToken") token: String,
        @Path("orderId") orderId: String,
        @Body requestBody: RequestBody
    ): Call<NotifyPaymentResponse>

    @GET("/planAndSubscription/get/plans")
    fun getAllPackageHealthPlans(): Observable<List<GetAllPlansResponse>>

    @POST("/planAndSubscription/create/user/subscription")
    fun createHealthPackage(@Body craReq: CreatePackageRequest): Observable<CreatePackageResponse>

    @GET("/planAndSubscription/get/user/active/subscription/{userId}")
    fun isActiveHealthPlan(@Path("userId") userId: String): Observable<List<GetNavPackageResponse>>

    @POST("/users/registration")
    fun userRegistrationUserGmail(@Body requestBody: UserDetailsRequest): Observable<UserRegistrationGmailResponse>

    @POST("/admin/emp/access/code/validate")
    fun validateAccessCode(@Body validateAcc: ValidateAccessCodeRequest): Observable<UserRegistrationGmailResponse>

    @GET("/health/information/user/health/data/{userId}")
    fun getAllDashboardHealthPlan(@Path("userId") userId: String): Observable<List<GetAllDashboardHealthPlanResponse>>

    @POST("/health/information/save/user/health/data")
    fun postHealthData(@Body healthDataRequest: HealthDataRequest): Observable<HealthDataResponse>

    @GET("/users/get/information/{userId}")
    fun getUserDetailsByUserId(@Path("userId") userId: String): Observable<GetUserDetailsResponse>

    @GET("/users/get/questions")
    fun getOnboardQuestion(): Observable<List<OnboardQuestionResponse>>

    @POST("/users/save/questions/answer")
    fun postUsersAnswer(@Body healthDataRequest: ArrayList<UsersAnswerRequest>): Observable<ArrayList<UsersAnswerResponse>>

    @GET("/users/get/answers/{userId}")
    fun getAllUsersAnswers(@Path("userId") userId: String): Observable<List<OnboardQuestionResponse>>

    @GET("/health/information/user/week/health/data/{userId}/{noOFDays}")
    fun getHistoryData(
        @Path("userId") userId: String,
        @Path("noOFDays") noOFDays: String
    ): Observable<HistoryResponse>

    @PUT("/users/update/user/information/{userId}")
    fun updateUserDetails(
        @Path("userId") userId: String,
        @Body requestBody: UserDetailsRequest
    ): Observable<UserDetails>

    @GET("/health/information/user/today/data/{userId}")
    fun getTodayHealthData(@Path("userId") userId: String): Observable<List<TodayDashboardResponse>>

    @GET("/health/information/user/logs/data/datewise/{userId}/{days}")
    fun getLogsData(
        @Path("userId") userId: String,
        @Path("days") days: String
    ): Observable<List<LogsResponse>>

    @GET("/health/information/user/graph/data/{userId}/{days}")
    fun getGroupsTrendsGraph(
        @Path("userId") userId: String,
        @Path("days") days: String
    ): Observable<TrendsLogsResponse>

    @GET("/rest/get/binah/license/key")
    fun getLicenseKey(): Observable<LicenseKeyResponse>

    @GET("/payment/orderId/and/key/for/cashfree/{mobileNumber}/{amount}")
    fun getCashFreeAppId(
        @Path("mobileNumber") mobileNumber: String?,
        @Path("amount") amount: String?
    ): Observable<PaymentAppIdResponse>

    @GET("/payment/generate/orderId/and/key/for/cashfree/{userId}/{amount}")
    fun getPaymentCashFreeAppId(
        @Path("userId") mobileNumber: String?,
        @Path("amount") amount: String?
    ): Observable<PaymentAppIdResponse>

    /* @POST("/admin/checkCovidPossibility")
     fun getRiskAssesmentPossibility(@Body riskRequest: CheckRiskRequest): Observable<CheckRiskAssesmentResponse>*/

    @GET("/planAndSubscription/get/user/subscriptions/{userId}")
    fun getUsersPackagePlan(@Path("userId") mobileNumber: String?):
            Observable<List<GetNavPackageResponse>>

    @GET("/health/information/user/health/scrore/graph/{userId}/{days}")
    fun getHealthScoreDashboard(
        @Path("userId") userId: String?,
        @Path("days") days: String?
    ): Observable<List<GetHealthScoreResponse>>

    @GET("/users/getByEmail/{userEmail}")
    fun getUserDataByEmail(
        @Path("userEmail") userEmail: String?
    ): Observable<GetUserDetailsResponse>

}