package com.obhaiyya.health.presentation.onboard

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OnboardQuestionResponse : Serializable {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("question")
    @Expose
    var question: String? = null

    @SerializedName("answer")
    @Expose
    var answer: String? = null

    @SerializedName("weightage")
    @Expose
    var weightage: String? = null

    @SerializedName("userId")
    @Expose
    var userId: String? = null


}