package com.obhaiyya.health.presentation.onboard

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_first_question.*
import kotlinx.android.synthetic.main.fragment_second_question.*
import kotlinx.android.synthetic.main.fragment_third_question.*

class ThirdQuestionFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    var firstResponse: OnboardQuestionResponse? = null
    var secondResponse: OnboardQuestionResponse? = null
    var thirdResponse: OnboardQuestionResponse? = null


    companion object {

        val KEY_QUESTION_FIRST = "KEY_QUESTION_FIRST"
        val KEY_QUESTION_SECOND = "KEY_QUESTION_SECOND"
        val KEY_QUESTION_THIRD = "KEY_QUESTION_THIRD"

        fun newInstance(
                mListResponse: OnboardQuestionResponse,
                secondQues: OnboardQuestionResponse
        ): ThirdQuestionFragment {
            val firstQuestionFragment = ThirdQuestionFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_QUESTION_FIRST, mListResponse)
            bundle.putSerializable(KEY_QUESTION_SECOND, secondQues)
            firstQuestionFragment.arguments = bundle
            return firstQuestionFragment
        }

    }

    override fun getLayout(): Int {
        return R.layout.fragment_third_question
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firstResponse = arguments?.getSerializable(KEY_QUESTION_FIRST) as OnboardQuestionResponse
        secondResponse = arguments?.getSerializable(KEY_QUESTION_SECOND) as OnboardQuestionResponse

        txt7Question.text = firstResponse?.question
        txt8Question.text = secondResponse?.question

        rdGrp8.setOnCheckedChangeListener(this)
        rdGrp7.setOnCheckedChangeListener(this)
        btnThirdFrag.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        (activity as OnboardQuestionActivity).hideBtnVisibility()
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtn7Yes -> {
                checkAnswer(7, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtn7No -> {
                checkAnswer(7, OnboardQuestionActivity.KEY_NO)
            }
            R.id.rdBtn8Yes -> {
                checkAnswer(8, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtn8No -> {
                checkAnswer(8, OnboardQuestionActivity.KEY_NO)
            }
        }
    }

    fun checkAnswer(pos: Int, ans: String) {
        when (pos) {
            7 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID)
                        ?: ""
                ansReq.answer = ans
                ansReq.question = firstResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(7, ansReq)
                checkAllAnswered()
            }
            8 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID)
                        ?: ""
                ansReq.answer = ans
                ansReq.question = secondResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(8, ansReq)
                checkAllAnswered()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnThirdFrag -> {
            }
        }
    }

    fun checkAllAnswered() {
        val answer7 = rdBtn7Yes.isChecked || rdBtn7No.isChecked
        val answer8 = rdBtn8Yes.isChecked || rdBtn8No.isChecked

        if (answer7 && answer8) {
            btnThirdFrag.visibility = View.GONE
            (activity as OnboardQuestionActivity).showBtnVisibility(3)
        }

    }


}