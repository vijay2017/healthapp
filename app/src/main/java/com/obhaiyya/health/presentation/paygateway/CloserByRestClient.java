package com.obhaiyya.health.presentation.paygateway;


import android.text.TextUtils;

import com.appwithmeflutter.mype.data.network.HealthService;
import com.example.obhaiyyahealth.dataProvider.ApiService;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.JsonObject;
import com.obhaiyya.health.HealthApplication;
import com.obhaiyya.health.presentation.Package.PaymentGatewayResponse;

import java.security.cert.CertificateException;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class CloserByRestClient {

    public static final String NETWORK_UNAVAILABLE = "network_unavailable";
    private static HealthService mHealthService = ApiService.Companion.getInstance().call();

    public void getTxIdForGpay(String customerPhone, double paymentAmt, Callback<
            TokenOtherDataModel> callback) {
        Call<TokenOtherDataModel> call = mHealthService.getTxIdForGpay(customerPhone, String.valueOf(paymentAmt));
        call.enqueue(callback);
    }


    public void notifyBackendPaymentSuccess
            (Map<String, String> map, Callback<PaymentGatewayResponse> responseListener) {
        if (HealthApplication.Companion.getAppInstance().isNetworkConnected()) {
            String orderId = map.get(JSONConstants.ORDER_ID);
            JsonObject obj = new JsonObject();
            obj.addProperty(JSONConstants.TX_STATUS, map.get(JSONConstants.TX_STATUS));
            obj.addProperty(JSONConstants.ORDER_AMT, Double.parseDouble(map.get(JSONConstants.ORDER_AMT)));
            obj.addProperty(JSONConstants.ORDER_ID, orderId);
            obj.addProperty(JSONConstants.PAYMENT_MODE, map.get(JSONConstants.PAYMENT_MODE));
            String txTime = map.get(JSONConstants.TX_TIME);
            if (!TextUtils.isEmpty(txTime)) {
                obj.addProperty(JSONConstants.TX_TIME, txTime);
            }
            String sign = map.get(JSONConstants.SIGN);
            if (!TextUtils.isEmpty(sign)) {
                obj.addProperty(JSONConstants.SIGN, sign);
            }
            String txMsg = map.get(JSONConstants.TX_MSG);
            if (!TextUtils.isEmpty(txMsg)) {
                obj.addProperty(JSONConstants.TX_MSG, txMsg);
            }
            obj.addProperty(JSONConstants.REF_ID, map.get(JSONConstants.REF_ID));
            String body = obj.toString();
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);

            Call<PaymentGatewayResponse> call = mHealthService.notifyBackendPaymentSuccessNew(orderId, requestBody);

            call.enqueue(responseListener);
        } else {
            HealthApplication.Companion.showToast("No internet connected");
        }
    }

    public void notifyBackendPaymentFailure
            (Map<String, String> map, Callback<NotifyPaymentResponse> responseListener) {
        String orderId = map.get(JSONConstants.ORDER_ID);
        JsonObject obj = new JsonObject();
        obj.addProperty(JSONConstants.TX_STATUS, map.get(JSONConstants.TX_STATUS));
        obj.addProperty(JSONConstants.ORDER_AMT, Double.parseDouble(map.get(JSONConstants.ORDER_AMT)));
        obj.addProperty(JSONConstants.ORDER_ID, orderId);
        obj.addProperty(JSONConstants.TX_TIME, map.get(JSONConstants.TX_TIME));
        obj.addProperty(JSONConstants.PAYMENT_MODE, map.get(JSONConstants.PAYMENT_MODE));
        obj.addProperty(JSONConstants.SIGN, map.get(JSONConstants.SIGN));
        obj.addProperty(JSONConstants.TX_MSG, map.get(JSONConstants.TX_MSG));
        obj.addProperty(JSONConstants.REF_ID, map.get(JSONConstants.REF_ID));

        String body = obj.toString();
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), body);

        Call<NotifyPaymentResponse> call = mHealthService.notifyBackendPaymentFailure("", orderId, requestBody);

        call.enqueue(responseListener);
    }

}
