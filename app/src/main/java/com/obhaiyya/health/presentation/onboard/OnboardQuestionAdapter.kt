package com.obhaiyya.health.presentation.onboard

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.obhaiyya.health.presentation.base.BaseFragment


class OnboardQuestionAdapter(
        fa: FragmentActivity,
        val mListResponse: List<OnboardQuestionResponse>
) : FragmentStateAdapter(fa) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): BaseFragment {
        when (position) {
            0 -> return FirstQuestionFragment.newInstance(
                    mListResponse.get(0),
                    mListResponse.get(1),
                    mListResponse.get(2)
            )
            1 -> return SecondQuestionFragment.newInstance(
                    mListResponse.get(3),
                    mListResponse.get(4),
                    mListResponse.get(5)
            )
            2 -> return ThirdQuestionFragment.newInstance(
                    mListResponse.get(6),
                    mListResponse.get(7)
            )
            else -> {
                return FirstQuestionFragment()
            }
        }
    }


}

