package com.obhaiyya.health.presentation.dashboard

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.userdetails.UserRegistrationGmailResponse
import com.obhaiyya.health.presentation.utils.Navigator
import com.obhaiyya.health.presentation.utils.RxBusNew
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.nav_header_main.view.*

class DashboardActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
    DrawerLayout.DrawerListener {

    var mDrawerToggle: ActionBarDrawerToggle? = null

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, DashboardActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        navMenu?.setNavigationItemSelectedListener(this)
        initDrawer()
        replaceFragment(
            R.id.frame_dashboard,
            DashboardFragment()
        )
    }

    override fun onResume() {
        super.onResume()
        setHamburgerUserName()
    }

    private fun setHamburgerUserName() {
        val userDetailJson =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_DETAILS)
        val jsonUserDetails =
            Gson().fromJson(userDetailJson, UserRegistrationGmailResponse::class.java)
        try {
            val header: View = navMenu.getHeaderView(0)
            header.txtCustomerName.setText("" + jsonUserDetails.name)
        } catch (e: Exception) {

        }
    }

    private fun initDrawer() {
        mDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbarhome,
            R.string.open_drawer,
            R.string.close_drawer
        ) {
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
                invalidateOptionsMenu() // creates call to onPrepareOptionsMenu()
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                invalidateOptionsMenu() // creates call to onPrepareOptionsMenu()
            }
        }
        drawer_layout.addDrawerListener(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                drawer_layout?.openDrawer(GravityCompat.START)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mDrawerToggle?.onConfigurationChanged(newConfig)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        mDrawerToggle?.syncState()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawer_layout.closeDrawers()
        when (item.itemId) {
            R.id.nav_profile -> {
                Log.d("onNavigation", "nav_home")
                Navigator.navigateToProfile(this)
                return true
            }
            R.id.nav_history -> {
                Log.d("onNavigation", "nav_photos")
                Navigator.navigateToHistory(this)
                return true
            }
            R.id.nav_information -> {
                Log.d("onNavigation", "nav_information")
                Navigator.navigateToInformation(this)
                return true
            }
            R.id.nav_logout -> {
                Log.d("onNavigation", "nav_logout")
                val logoutModel = LogoutModel()
                logoutModel.isLogoutRequest = true
                SharedPreferenceManager.clearData()
                RxBusNew.publish(logoutModel)
                finish()
                Navigator.navigateToSplash(this)
                return true
            }
            R.id.nav_Package -> {
                Log.d("onNavigation", "nav_Package")
                Navigator.navigateToNavPackage(this)
                return true
            }
            else -> {
                return false
            }

        }
    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {

    }

    override fun onDrawerOpened(drawerView: View) {

    }

}