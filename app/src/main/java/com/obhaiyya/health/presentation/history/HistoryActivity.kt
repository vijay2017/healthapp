package com.obhaiyya.health.presentation.history

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseActivity

class HistoryActivity : BaseActivity() {

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, HistoryActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        setToolbarWithTitle("History", false)
        replaceFragment(R.id.frame_history, HistoryParentHolderFragment())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}