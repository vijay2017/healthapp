package com.obhaiyya.health.presentation.dashboard

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.obhaiyya.health.BuildConfig
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseFragment
import com.obhaiyya.health.presentation.posthealthquestion.PostHealthQuestionBS
import com.obhaiyya.health.presentation.restbottomsheet.RestOrWalkBottomSheet
import com.obhaiyya.health.presentation.userdetails.UserRegistrationGmailResponse
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import com.obhaiyya.health.presentation.utils.Navigator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_dashboard.*


class DashboardFragment : BaseFragment(), View.OnClickListener, IDayClicked {

    var userId = ""
    var isRestOrWalk = false
    var mLicenseKey = ""

    override fun getLayout(): Int {
        return R.layout.fragment_dashboard
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnTrackNow.setOnClickListener(this)
        btnTrackNowoff.setOnClickListener(this)
        btnRiskAssesment.setOnClickListener(this)
        userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
        observeLicenseKeyResponse()
        Log.d("VIJAY----USERID", "" + userId)
    }

    private fun observeLicenseKeyResponse() {
        mCustomerService?.getLicenseKey()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                mLicenseKey = it.binahLicenseKey ?: ""
            }, {
                mLicenseKey = ""
            })
    }

    private fun observeHealthScoreResponse() {
        val userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
        mCustomerService?.getHealthScoreDashboard(userId, "5")
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({

                val data0 = Math.round(
                    it.get(0).healthScore
                        ?: 0.0
                ).toInt()
                val data1 = Math.round(
                    it.get(1).healthScore
                        ?: 0.0
                ).toInt()
                val data2 = Math.round(
                    it.get(2).healthScore
                        ?: 0.0
                ).toInt()
                val data3 = Math.round(
                    it.get(3).healthScore
                        ?: 0.0
                ).toInt()
                val data4 = Math.round(
                    it.get(4).healthScore
                        ?: 0.0
                ).toInt()

                if (data0 == -1 && data1 == -1 && data2 == -1 && data3 == -1 && data4 == -1) {
                    txtNoHealthScoreData.visibility = View.VISIBLE
                    cvHealthScore.visibility = View.GONE
                    txt_avg.visibility = View.GONE
                    txt_avg_2.visibility = View.GONE
                } else {
                    txtNoHealthScoreData.visibility = View.GONE
                    cvHealthScore.visibility = View.VISIBLE
                    txt_avg.visibility = View.VISIBLE
                    txt_avg_2.visibility = View.VISIBLE
                }

                txtToday.setText("" + data0)
                txtYesterday.setText("" + data1)
                txt3HealthScore.setText("" + data2)
                txt4HealthScore.setText("" + data3)
                txt5HealthScore.setText("" + data4)

                setHeightTodayValue(
                    txtTodayPillar,
                    Math.round(
                        it.get(0).healthScore
                            ?: 0.0
                    ).toInt()
                )
                setHeightYesterdayValue(
                    txtYesterdayPillar,
                    Math.round(
                        it.get(1).healthScore
                            ?: 0.0
                    ).toInt()
                )
                setHeight2Value(
                    txt3HealthScorePillar,
                    Math.round(
                        it.get(2).healthScore
                            ?: 0.0
                    ).toInt()
                )
                setHeight3Value(
                    txt4HealthPillar,
                    Math.round(
                        it.get(3).healthScore
                            ?: 0.0
                    ).toInt()
                )
                setHeight4Value(
                    txt5HealthPillar,
                    Math.round(
                        it.get(4).healthScore
                            ?: 0.0
                    ).toInt()
                )

                txtTodayDate.setText("" + it.get(0).date?.substring(0, 5) ?: "")
                txtYesterdayDate.setText("" + it.get(1).date?.substring(0, 5) ?: "")
                txt3HealthScoreDate.setText("" + it.get(2).date?.substring(0, 5) ?: "")
                txt4HealthScoreDate.setText("" + it.get(3).date?.substring(0, 5) ?: "")
                txt5HealthScoreDate.setText("" + it.get(4).date?.substring(0, 5) ?: "")

            }, {
                Log.d("VIJAY----", "" + it)
            })
    }

    fun setHeightTodayValue(txtTodayPillar: View, pos: Int) {
        when (pos) {
            -1 -> {
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker_no);
                txtMortalityRiskStatus.visibility = View.VISIBLE
                txtToday.visibility = View.GONE
                txtday.setTextColor(Color.parseColor("#959595"))
                txtToday.setTextColor(Color.parseColor("#959595"))
                val params = txtTodayPillar.layoutParams
                params.height = 225
                txtTodayPillar.layoutParams = params
            }

            0 -> {
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "10"
                txtToday.visibility = View.VISIBLE
                txtday.setTextColor(Color.parseColor("#179765"))
                txtToday.setTextColor(Color.parseColor("#179765"))
                val params = txtTodayPillar.layoutParams
                params.height = 650
                txtTodayPillar.layoutParams = params
            }
            1 -> {
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "09"
                txtToday.visibility = View.VISIBLE
                txtday.setTextColor(Color.parseColor("#179765"))
                txtToday.setTextColor(Color.parseColor("#179765"))
                val params = txtTodayPillar.layoutParams
                params.height = 650
                txtTodayPillar.layoutParams = params
            }
            2 -> {
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "08"
                txtToday.visibility = View.VISIBLE
                txtday.setTextColor(Color.parseColor("#179765"))
                txtToday.setTextColor(Color.parseColor("#179765"))
                val params = txtTodayPillar.layoutParams
                params.height = 600
                txtTodayPillar.layoutParams = params


            }
            3 -> {

                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "07"
                txtToday.visibility = View.VISIBLE
                txtday.setTextColor(Color.parseColor("#179765"))
                txtToday.setTextColor(Color.parseColor("#179765"))
                val params = txtTodayPillar.layoutParams
                params.height = 575
                txtTodayPillar.layoutParams = params
            }


            4 -> {
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "06"
                txtToday.visibility = View.VISIBLE
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txtday.setTextColor(Color.parseColor("#FFAF0F"))
                txtToday.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtTodayPillar.layoutParams
                params.height = 550
                txtTodayPillar.layoutParams = params

            }
            5 -> {
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "05"
                txtToday.visibility = View.VISIBLE
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txtday.setTextColor(Color.parseColor("#FFAF0F"))
                txtToday.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtTodayPillar.layoutParams
                params.height = 500
                txtTodayPillar.layoutParams = params

            }
            6 -> {
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "04"
                txtToday.visibility = View.VISIBLE
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txtday.setTextColor(Color.parseColor("#FFAF0F"))
                txtToday.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtTodayPillar.layoutParams
                params.height = 450
                txtTodayPillar.layoutParams = params
            }
            7 -> {
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "03"
                txtToday.visibility = View.VISIBLE
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txtday.setTextColor(Color.parseColor("#F83C74"))
                txtToday.setTextColor(Color.parseColor("#F83C74"))
                val params = txtTodayPillar.layoutParams
                params.height = 400
                txtTodayPillar.layoutParams = params
            }
            8 -> {
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "02"
                txtToday.visibility = View.VISIBLE
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txtday.setTextColor(Color.parseColor("#F83C74"))
                txtToday.setTextColor(Color.parseColor("#F83C74"))
                val params = txtTodayPillar.layoutParams
                params.height = 350
                txtTodayPillar.layoutParams = params
            }
            9 -> {
                txtMortalityRiskStatus.visibility = View.GONE
                txtToday.text = "01"
                txtToday.visibility = View.VISIBLE
                txtTodayPillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txtday.setTextColor(Color.parseColor("#F83C74"))
                txtToday.setTextColor(Color.parseColor("#F83C74"))
                val params = txtTodayPillar.layoutParams
                params.height = 300
                txtTodayPillar.layoutParams = params

            }
//            10 -> {
//
//                txtMortalityRiskStatus.text = "Average"
//                txtToday.text = "01"
//                txtToday.visibility = View.VISIBLE
//                txtToday.setBackgroundResource(R.drawable.ic_marker_poor);
//                txtday.setTextColor(Color.parseColor("#F83C74"))
//                txtToday.setTextColor(Color.parseColor("#F83C74"))
//                val params = txtTodayPillar.layoutParams
//                params.height = 80
//                txtTodayPillar.layoutParams = params
//
//            }

        }

    }

    fun setHeightYesterdayValue(txtYesterdayPillar: View, pos: Int) {
        when (pos) {
            -1 -> {
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker_no);
                txtMortalityRiskStatusyesrerday.visibility = View.VISIBLE
                txtYesterday.visibility = View.GONE
                txtYesterday.setTextColor(Color.parseColor("#959595"))
                txtYesterday.setTextColor(Color.parseColor("#959595"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 225
                txtYesterdayPillar.layoutParams = params
            }
            0 -> {
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "10"
                txtYesterday.visibility = View.VISIBLE
                txtYesterday.setTextColor(Color.parseColor("#179765"))
                txtYesterday.setTextColor(Color.parseColor("#179765"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 650
                txtYesterdayPillar.layoutParams = params
            }
            1 -> {
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "09"
                txtYesterday.visibility = View.VISIBLE
                txtYesterday.setTextColor(Color.parseColor("#179765"))
                txtYesterday.setTextColor(Color.parseColor("#179765"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 650
                txtYesterdayPillar.layoutParams = params
            }
            2 -> {
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "08"
                txtYesterday.visibility = View.VISIBLE
                txtYesterday.setTextColor(Color.parseColor("#179765"))
                txtYesterdayDate.setTextColor(Color.parseColor("#179765"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 600
                txtYesterdayPillar.layoutParams = params


            }
            3 -> {

                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "07"
                txtYesterday.visibility = View.VISIBLE
                txtYesterday.setTextColor(Color.parseColor("#179765"))
                txtYesterdayDate.setTextColor(Color.parseColor("#179765"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 575
                txtYesterdayPillar.layoutParams = params
            }


            4 -> {
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "06"
                txtYesterday.visibility = View.VISIBLE
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txtYesterday.setTextColor(Color.parseColor("#FFAF0F"))
                txtYesterdayDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 550
                txtYesterdayPillar.layoutParams = params

            }
            5 -> {
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "05"
                txtYesterday.visibility = View.VISIBLE
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txtYesterday.setTextColor(Color.parseColor("#FFAF0F"))
                txtYesterdayDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 500
                txtYesterdayPillar.layoutParams = params

            }
            6 -> {
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "04"
                txtYesterday.visibility = View.VISIBLE
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txtYesterday.setTextColor(Color.parseColor("#FFAF0F"))
                txtYesterdayDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 450
                txtYesterdayPillar.layoutParams = params
            }
            7 -> {
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "03"
                txtYesterday.visibility = View.VISIBLE
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txtYesterday.setTextColor(Color.parseColor("#F83C74"))
                txtYesterdayDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 400
                txtYesterdayPillar.layoutParams = params
            }
            8 -> {
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "02"
                txtYesterday.visibility = View.VISIBLE
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txtYesterday.setTextColor(Color.parseColor("#F83C74"))
                txtYesterdayDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 350
                txtYesterdayPillar.layoutParams = params
            }
            9 -> {
                txtMortalityRiskStatusyesrerday.visibility = View.GONE
                txtYesterday.text = "01"
                txtYesterday.visibility = View.VISIBLE
                txtYesterdayPillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txtYesterday.setTextColor(Color.parseColor("#F83C74"))
                txtYesterdayDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 300
                txtYesterdayPillar.layoutParams = params

            }

        }

    }

    fun setHeight2Value(txt3HealthScorePillar: View, pos: Int) {
        when (pos) {
            -1 -> {
                txtMortalityRiskStatus3.visibility = View.VISIBLE
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_no);
                txt3HealthScore.visibility = View.GONE
                txt3HealthScore.setTextColor(Color.parseColor("#959595"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#959595"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 225
                txt3HealthScorePillar.layoutParams = params
            }
            0 -> {
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "10"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScore.setTextColor(Color.parseColor("#179765"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 650
                txt3HealthScorePillar.layoutParams = params
            }
            1 -> {
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "09"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScore.setTextColor(Color.parseColor("#179765"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 650
                txt3HealthScorePillar.layoutParams = params
            }
            2 -> {
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "08"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScore.setTextColor(Color.parseColor("#179765"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 600
                txt3HealthScorePillar.layoutParams = params


            }
            3 -> {

                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "07"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScore.setTextColor(Color.parseColor("#179765"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 575
                txt3HealthScorePillar.layoutParams = params
            }


            4 -> {

                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "06"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt3HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 550
                txt3HealthScorePillar.layoutParams = params

            }
            5 -> {
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "05"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt3HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 500
                txt3HealthScorePillar.layoutParams = params

            }
            6 -> {
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "04"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt3HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 450
                txt3HealthScorePillar.layoutParams = params
            }
            7 -> {
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "03"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt3HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 400
                txt3HealthScorePillar.layoutParams = params
            }
            8 -> {
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "02"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt3HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 350
                txt3HealthScorePillar.layoutParams = params
            }
            9 -> {
                txtMortalityRiskStatus3.visibility = View.GONE
                txt3HealthScore.text = "01"
                txt3HealthScore.visibility = View.VISIBLE
                txt3HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt3HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt3HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt3HealthScorePillar.layoutParams
                params.height = 300
                txt3HealthScorePillar.layoutParams = params

            }

        }

    }

    fun setHeight3Value(txt4HealthScorePillar: View, pos: Int) {
        when (pos) {
            -1 -> {
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_no);
                txtMortalityRiskStatus4.visibility = View.VISIBLE
                txt4HealthScore.visibility = View.GONE
                txt4HealthScore.setTextColor(Color.parseColor("#959595"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#959595"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 225
                txt4HealthScorePillar.layoutParams = params
            }
            0 -> {
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "10"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScore.setTextColor(Color.parseColor("#179765"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 650
                txt4HealthScorePillar.layoutParams = params
            }
            1 -> {
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "09"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScore.setTextColor(Color.parseColor("#179765"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 650
                txt4HealthScorePillar.layoutParams = params
            }
            2 -> {
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "08"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScore.setTextColor(Color.parseColor("#179765"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 600
                txt4HealthScorePillar.layoutParams = params


            }
            3 -> {

                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "07"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScore.setTextColor(Color.parseColor("#179765"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 575
                txt4HealthScorePillar.layoutParams = params
            }


            4 -> {

                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "06"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt4HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 550
                txt4HealthScorePillar.layoutParams = params

            }
            5 -> {
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "05"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt4HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 500
                txt4HealthScorePillar.layoutParams = params

            }
            6 -> {
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "04"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt4HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 450
                txt4HealthScorePillar.layoutParams = params
            }
            7 -> {
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "03"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt4HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 400
                txt4HealthScorePillar.layoutParams = params
            }
            8 -> {
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "02"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt4HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 350
                txt4HealthScorePillar.layoutParams = params
            }
            9 -> {
                txtMortalityRiskStatus4.visibility = View.GONE
                txt4HealthScore.text = "01"
                txt4HealthScore.visibility = View.VISIBLE
                txt4HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt4HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt4HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt4HealthScorePillar.layoutParams
                params.height = 300
                txt4HealthScorePillar.layoutParams = params

            }

        }

    }

    fun setHeight4Value(txt5HealthScorePillar: View, pos: Int) {
        when (pos) {
            -1 -> {
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_no);
                txtMortalityRiskStatus5.visibility = View.VISIBLE
                txt5HealthScore.visibility = View.GONE
                txt5HealthScore.setTextColor(Color.parseColor("#959595"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#959595"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 225
                txt5HealthScorePillar.layoutParams = params
            }
            0 -> {
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "10"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScore.setTextColor(Color.parseColor("#179765"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 650
                txt5HealthScorePillar.layoutParams = params
            }
            1 -> {
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "09"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScore.setTextColor(Color.parseColor("#179765"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 650
                txt5HealthScorePillar.layoutParams = params
            }
            2 -> {
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "08"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScore.setTextColor(Color.parseColor("#179765"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 600
                txt5HealthScorePillar.layoutParams = params


            }
            3 -> {

                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker);
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "07"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScore.setTextColor(Color.parseColor("#179765"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#179765"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 575
                txt5HealthScorePillar.layoutParams = params
            }


            4 -> {

                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "06"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt5HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 550
                txt5HealthScorePillar.layoutParams = params

            }
            5 -> {
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "05"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt5HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 500
                txt5HealthScorePillar.layoutParams = params

            }
            6 -> {
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "04"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_avreage);
                txt5HealthScore.setTextColor(Color.parseColor("#FFAF0F"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#FFAF0F"))
                val params = txtYesterdayPillar.layoutParams
                params.height = 450
                txt5HealthScorePillar.layoutParams = params
            }
            7 -> {
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "03"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt5HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 400
                txt5HealthScorePillar.layoutParams = params
            }
            8 -> {
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "02"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt5HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 350
                txt5HealthScorePillar.layoutParams = params
            }
            9 -> {
                txtMortalityRiskStatus5.visibility = View.GONE
                txt5HealthScore.text = "01"
                txt5HealthScore.visibility = View.VISIBLE
                txt5HealthScorePillar.setBackgroundResource(R.drawable.ic_marker_poor);
                txt5HealthScore.setTextColor(Color.parseColor("#F83C74"))
                txt5HealthScoreDate.setTextColor(Color.parseColor("#F83C74"))
                val params = txt5HealthScorePillar.layoutParams
                params.height = 300
                txt5HealthScorePillar.layoutParams = params

            }

        }

    }

    override fun onResume() {
        super.onResume()
        getAllDashboardHealthPlan()
        observeHealthScoreResponse()
    }

    fun isActiveHealthPlan() {
        mCustomerService?.isActiveHealthPlan(userId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                hideProgress()
                if (!BuildConfig.DEBUG) {
                    if (it.isEmpty()) {
                        activity?.let {
                            Navigator.navigateToPackage(it, true)
                            it.finish()
                        }
                    } else {
                        if (it.isNotEmpty()) {
                            activePlanSuccess()
                        } else {
                            showToastMessage("Package expired")
                        }
                    }
                } else {
                    activePlanSuccess()
                }
            }, {
                hideProgress()
                Log.d("VIJAY", "ERROR-OTP")
            })
    }

    fun getUserDetailsByUserId() {
        activity?.let {
            showProgress(it)
        }
        mCustomerService?.getUserDetailsByUserId(userId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doFinally {
                hideProgress()
            }
            ?.subscribe({
                Log.d("VIJAY", "SUCCESS-OTP")
                hideProgress()
                if (!BuildConfig.DEBUG) {
                    if (it.accessCode?.isEmpty() == true) {
                        isActiveHealthPlan()
                    } else {
                        if (it.active == true) {
                            activity?.let {
//                                if (mLicenseKey.isNotEmpty()) {

                                Navigator.navigateToMain(it, isRestOrWalk, mLicenseKey, false)
//                                } else {
//                                    showToastMessage("Invalid license key")
//                                }
                            }
                        } else {
                            showToastMessage("Your Package has been expired")
                        }
                    }
                } else {
                    activity?.let {
//                        if (mLicenseKey.isNotEmpty()) {
                        Navigator.navigateToMain(it, isRestOrWalk, mLicenseKey, false)

//                        } else {
//                            showToastMessage("Invalid license key")
//                        }
                    }
                }
            }, {
                hideProgress()
                showToastMessage("Server Error")
            })
    }

    private fun activePlanSuccess() {
        activity?.let {
            Navigator.navigateToMain(it, isRestOrWalk, mLicenseKey, false)
            /*if (mLicenseKey.isNotEmpty()) {
                Navigator.navigateToMain(it, isRestOrWalk, mLicenseKey)
            } else {
                showToastMessage("Invalid license key")
            }*/
        }
    }

    fun getAllDashboardHealthPlan() {
        mCustomerService?.getTodayHealthData(userId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                setDashAdapter(it)
                hideProgress()
            }, {
                hideProgress()
                Log.d("VIJAY", "ERROR-OTP")
            })
    }

    fun setDashAdapter(list: List<TodayDashboardResponse>) {
        if (list.isEmpty()) {
            txtZeroState.visibility = View.VISIBLE
            whenthereisdata.visibility = View.GONE
            txt_helth.visibility = View.GONE
        } else {
            txtZeroState.visibility = View.GONE
            val reverseList = list.reversed()
            whenthereisdata.visibility = View.VISIBLE
            txt_helth.visibility = View.VISIBLE
            activity?.let {
                val linearLayoutManager =
                    LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                rvTrackRecord.layoutManager = linearLayoutManager
                val rvAdapter = DashboardAdapter(it, this, reverseList)
                rvTrackRecord.adapter = rvAdapter
            }
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnTrackNow -> {
                observeUserDataByEmail()
            }
            R.id.btnTrackNowoff -> {
                observeUserDataByEmail()
            }
            R.id.btnRiskAssesment -> {
                activity?.let {
                    Navigator.navigateToMain(it, false, "", true)
                }

            }
        }
    }

    private fun observeUserDataByEmail() {
        val emailId =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL) ?: ""
        mCustomerService?.getUserDataByEmail(emailId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                isActiveHealthPlan(it.active, userId)
            }, {
//                showToastMessage("Failed to load user data" + it.message)
            })
    }

    fun isActiveHealthPlan(active: Boolean?, userId: String) {
        mCustomerService?.isActiveHealthPlan(userId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                if (active == null) {
                    SharedPreferenceManager.saveBoolean(
                        SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
                        !it.isEmpty()
                    )
                    if (it.isEmpty()) {
                        activity?.let {
                            Navigator.navigateToPackage(it, false)
                            it?.finish()
                        }
                    } else {
                        openRestOrWalkBottomSheet()
                    }
                    //company user
                } else if (active != null) {
                    SharedPreferenceManager.saveBoolean(
                        SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
                        active
                    )
                    if (!active) {
                        activity?.let {
                            Navigator.navigateToPackage(it, false)
                            it?.finish()
                        }
                    } else {
                        openRestOrWalkBottomSheet()
                    }
                }
                Log.d("VIJAY___", "ACTIVE HEALTH PLAN CHECK ")
            }, {
                Log.d("VIJAY", "ERROR-OTP")
            })
    }

    private fun showPostHealthBS() {
        val postHealthQuestionBS = PostHealthQuestionBS.newInstance().apply {
            mIPaymentBottomHandler = object : PostHealthQuestionBS.IPostHealthQuestionHandler {
                override fun onSubmitClick(temparature: String, lossOfTaste: Boolean) {
//
                }
            }
        }
        postHealthQuestionBS.show(childFragmentManager, "PostHealthQuestion")
    }

    private fun openRestOrWalkBottomSheet() {
        val restOrWalkBottomSheet = RestOrWalkBottomSheet.newInstance().apply {
            mIRestOrWalkHandler = object : RestOrWalkBottomSheet.IRestOrWalkHandler {
                override fun onStatusSelectedSuccess(status: Boolean) {
                    isRestOrWalk = status
                    activity?.let {
                        Navigator.navigateToMain(it, isRestOrWalk, mLicenseKey, false)
                    }
//                    getUserDetailsByUserId()
                }
            }
        }
        restOrWalkBottomSheet.show(childFragmentManager, "RestOrWalk")
    }

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                getAllDashboardHealthPlan()
                hideProgress()
            }
        }

    override fun onDayClicked(mortalityRisk: Int, totalSize: Int) {
        val avgValue = mortalityRisk / totalSize

        when (avgValue) {

            0 -> {
                txtMortalityRiskDashboard.text = "10"
                txtMortalitStatusDashboard.text = "Good"
                txtMortalitStatusDashboardaverage.text = "Good"
                txtMortalitStatusDashboardpoor.text = "Good"
                average.visibility = View.GONE
                poor.visibility = View.GONE
                good.visibility = View.VISIBLE

            }

            1 -> {
                txtMortalityRiskDashboard.text = "10"
                txtMortalitStatusDashboard.text = "Good"
                txtMortalitStatusDashboardaverage.text = "Good"
                txtMortalitStatusDashboardpoor.text = "Good"
                average.visibility = View.GONE
                poor.visibility = View.GONE
                good.visibility = View.VISIBLE

            }
            2 -> {
                txtMortalityRiskDashboard.text = "09"
                txtMortalitStatusDashboard.text = "Good"
                txtMortalitStatusDashboardaverage.text = "Good"
                txtMortalitStatusDashboardpoor.text = "Good"
                average.visibility = View.GONE
                poor.visibility = View.GONE
                good.visibility = View.VISIBLE

            }
            3 -> {
                txtMortalityRiskDashboard.text = "08"
                txtMortalitStatusDashboard.text = "Good"
                txtMortalitStatusDashboardaverage.text = "Good"
                txtMortalitStatusDashboardpoor.text = "Good"
                average.visibility = View.GONE
                poor.visibility = View.GONE
                good.visibility = View.VISIBLE

            }
            4 -> {
                txtMortalityRiskDashboardaverage.text = "07"
                txtMortalitStatusDashboardaverage.text = "Average"
                average.visibility = View.VISIBLE
                poor.visibility = View.GONE
                good.visibility = View.GONE

            }
            5 -> {
                txtMortalityRiskDashboardaverage.text = "06"
                txtMortalitStatusDashboardaverage.text = "Average"
                average.visibility = View.VISIBLE
                poor.visibility = View.GONE
                good.visibility = View.GONE

            }
            6 -> {
                txtMortalityRiskDashboardaverage.text = "05"
                txtMortalitStatusDashboardaverage.text = "Average"
                average.visibility = View.VISIBLE
                poor.visibility = View.GONE
                good.visibility = View.GONE

            }
            7 -> {
                txtMortalityRiskDashboardpoor.text = "04"
                txtMortalitStatusDashboardpoor.text = "Poor"
                average.visibility = View.GONE
                poor.visibility = View.VISIBLE
                good.visibility = View.GONE

            }
            8 -> {
                txtMortalityRiskDashboardpoor.text = "03"
                txtMortalitStatusDashboardpoor.text = "Poor"
                average.visibility = View.GONE
                poor.visibility = View.VISIBLE
                good.visibility = View.GONE


            }
            9 -> {
                txtMortalityRiskDashboardpoor.text = "02"
                txtMortalitStatusDashboardpoor.text = "Poor"
                average.visibility = View.GONE
                poor.visibility = View.VISIBLE
                good.visibility = View.GONE

            }
            10 -> {
                txtMortalityRiskDashboardpoor.text = "01"
                txtMortalitStatusDashboardpoor.text = "Poor"
                average.visibility = View.GONE
                poor.visibility = View.VISIBLE
                good.visibility = View.GONE

            }

//            1, 2, 3 -> {
//                txtMortalitStatusDashboard.text = "Good"
//                txtMortalitStatusDashboardaverage.text = "Good"
//                txtMortalitStatusDashboardpoor.text = "Good"
//                average.visibility = View.GONE
//                poor.visibility = View.GONE
//                good.visibility = View.VISIBLE
//            }
//            4, 5, 6 -> {
//                txtMortalitStatusDashboard.text = "Average"
//                txtMortalitStatusDashboardaverage.text = "Average"
//                txtMortalitStatusDashboardpoor.text = "Average"
//                average.visibility = View.VISIBLE
//                poor.visibility = View.GONE
//                good.visibility = View.GONE
//            }
//            7, 8, 9, 10 -> {
//                txtMortalitStatusDashboard.text = "Poor"
//                txtMortalitStatusDashboardaverage.text = "Poor"
//                txtMortalitStatusDashboardpoor.text = "Poor"
//                average.visibility = View.GONE
//                poor.visibility = View.VISIBLE
//                good.visibility = View.GONE
//            }

        }
    }

    override fun onShareClicked(data: TodayDashboardResponse) {
        openWhatsApp(data)
    }

    private fun openWhatsApp(data: TodayDashboardResponse) {

        val heartRate = data.heartRate
        val spo2 = data.spo2
        val respiration = data.respiration
        val sdnn = data.hrv
        val stress = data.stress

        val createdAtDate = DateFormatUtils.convertInDDMMYYYY(data.createdAt ?: "") ?: ""
        val createdTime = DateFormatUtils.convertInDateTime(data.createdAt)

        val allData =
            " HeartRate - " + heartRate + "\n" +
                    " Spo2 - " + spo2 + "\n" +
                    " Respiration - " + respiration + "\n" +
                    " HRV - " + sdnn + "\n" +
                    " Stress - " + stress + "\n\n" +
                    " Taken At :- " + createdAtDate + " " + createdTime

        val userDetailJson =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_DETAILS)
        val jsonUserDetails =
            Gson().fromJson(userDetailJson, UserRegistrationGmailResponse::class.java)

        val mobileNumber = ""

        try {
            val smsNumber = mobileNumber //without '+'
            val uri = Uri.parse("https://api.whatsapp.com/send?phone=$smsNumber&text=$allData")
            val sendIntent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(sendIntent)
        } catch (e: Exception) {
            val uri = Uri.parse("market://details?id=com.whatsapp")
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            startActivity(goToMarket)
        }
    }


}