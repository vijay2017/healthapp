package com.obhaiyya.health.presentation.Package

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioGroup
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.obhaiyyahealth.presentation.base.PackageVM
import com.google.gson.Gson
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.CustomCashFreeCB
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.userdetails.UserRegistrationGmailResponse
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import com.obhaiyya.health.presentation.utils.Navigator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_package.*


/**
 * Created by Vijay on 7/3/20.
 */

class PackageActivity : BaseActivity(), View.OnClickListener,
    RadioGroup.OnCheckedChangeListener {

    val mPackageVM: PackageVM by viewModels()
    val user = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
    var isMonthlySelected = true
    var mGetAllPlansMonthly: GetAllPlansResponse? = null
    var mGetAllPlansYearly: GetAllPlansResponse? = null


    companion object {

        val KEY_FROM_Package = "keyIsFromPackage"

        fun getCallingIntent(context: Context, isFromPackage: Boolean): Intent {
            val intent = Intent(context, PackageActivity::class.java)
            intent.putExtra(KEY_FROM_Package, isFromPackage)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_package)

        val bundle = intent.extras
        bundle?.let {
            val isFromPackage = it.getBoolean(KEY_FROM_Package)
        }

        ll499Rupess.setOnClickListener(this)
        ll799Rupess.setOnClickListener(this)
        btnPackage.setOnClickListener(this)
        rdGrpPackage.setOnCheckedChangeListener(this)
        rdBtnMonthlySubs.isChecked = true
        observeGetAllPlans()
        showProgress(this)
        mPackageVM.getAllPlans()
        ll799Rupess.background = resources.getDrawable(R.drawable.ic_rectangle_box_inactive)
        ll499Rupess.background = resources.getDrawable(R.drawable.ic_state_box_active)
    }

    private fun observeGetAllPlans() {
        mPackageVM.mMutableGetAllPlans.observe(this, Observer {
            if (it.success) {
                it.mlist?.map {
                    when (it.validity) {
                        30 -> {
                            rdBtnMonthlySubs.text = "₹" + it.amount.toString()
                            mGetAllPlansMonthly = it
                        }
                        365 -> {
                            rdBtnYearlySubs.text = "₹ " + it.amount.toString()
                            mGetAllPlansYearly = it
                        }
                    }
                }
            }
            hideProgress()
        })
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnPackage -> {
                var amount = 0.0
                if (isMonthlySelected) {
                    amount = mGetAllPlansMonthly?.amount?.toDouble() ?: 0.0
                } else {
                    amount = mGetAllPlansYearly?.amount?.toDouble() ?: 0.0
                }
                val paymentBottomSheet = PaymentBottomSheet.newInstance(amount).apply {
                    mIPaymentBottomHandler =
                        object : PaymentBottomSheet.IPaymentBottomSheethandler {
                            override fun onPaymentSuccess(res: PaymentGatewayResponse?) {
                                createPackage(res)
                            }

                            override fun onPayment() {
                                createDummy()
                            }
                        }
                }
                paymentBottomSheet.show(supportFragmentManager, "Payment BS")
            }
            R.id.ll499Rupess -> {
                isMonthlySelected = true
                ll799Rupess.background = resources.getDrawable(R.drawable.ic_rectangle_box_inactive)
                ll499Rupess.background = resources.getDrawable(R.drawable.ic_state_box_active)
            }
            R.id.ll799Rupess -> {
                isMonthlySelected = false
                ll799Rupess.background = resources.getDrawable(R.drawable.ic_state_box_active)
                ll499Rupess.background = resources.getDrawable(R.drawable.ic_rectangle_box_inactive)
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtnMonthlySubs -> {
                isMonthlySelected = true
            }
            R.id.rdBtnYearlySubs -> {
                isMonthlySelected = false
            }
        }
    }

    fun createDummy() {
        val isQuestionAnswered =
            SharedPreferenceManager.getBoolean(SharedPreferenceManager.KEY_SHOULD_SHOW_ONBOARD)
        if (isQuestionAnswered == false) {
            Navigator.navigateToOnboardQuestion(this)
        } else {
            Navigator.navigateToDashboard(this)
        }
        SharedPreferenceManager.saveBoolean(
            SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
            true
        )
        finish()
    }


    fun createPackage(res: PaymentGatewayResponse?) {
        val mCreatePackageReq = CreatePackageRequest()
        mCreatePackageReq.userId = user
        val date = DateFormatUtils.getCurrentDate()
        mCreatePackageReq.startDate = DateFormatUtils.getSelectedDateInUTCFormat(date) ?: ""
        mCreatePackageReq.paymentDoneDate = DateFormatUtils.getCurrentDate()
        // plan details object
        if (isMonthlySelected) {
            mCreatePackageReq.amount = mGetAllPlansMonthly?.amount.toString() ?: ""
            mCreatePackageReq.endDate = DateFormatUtils.getEndTimeDate(30)
            mCreatePackageReq.planDetails = setPlanDetails(mGetAllPlansMonthly)
        } else {
            mCreatePackageReq.amount = mGetAllPlansYearly?.amount.toString() ?: ""
            mCreatePackageReq.endDate = DateFormatUtils.getEndTimeDate(365)
            mCreatePackageReq.planDetails = setPlanDetails(mGetAllPlansYearly)
        }
        // paymentgateway response
        val mPayDetailsSubcription = PaymentGatewayPackage()
        mPayDetailsSubcription._id = res?.id
        mPayDetailsSubcription.orderAmount = res?.orderAmount
        mPayDetailsSubcription.orderCurrency = res?.orderCurrency
        mPayDetailsSubcription.orderId = res?.orderId
        mPayDetailsSubcription.lastUpdatedTime = res?.lastUpdatedTime
        mCreatePackageReq.paymentGatewayResponses = mPayDetailsSubcription
        // userDetails
        val userDetailJson =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_DETAILS)
        val jsonUserDetails =
            Gson().fromJson(userDetailJson, UserRegistrationGmailResponse::class.java)
        val mUserDetailsSub = UserDetailsPackage()
        mUserDetailsSub._id = jsonUserDetails._id
        mUserDetailsSub.dateOfBirth = jsonUserDetails.dateOfBirth
        mUserDetailsSub.email = jsonUserDetails.email
        mUserDetailsSub.gender = jsonUserDetails.gender
        mUserDetailsSub.gmailToken = jsonUserDetails.gmailToken
        mUserDetailsSub.height = jsonUserDetails.height
        mUserDetailsSub.weight = jsonUserDetails.weight
        mUserDetailsSub.name = jsonUserDetails.name
        mUserDetailsSub.updatedAt = jsonUserDetails.updatedAt
        mCreatePackageReq.userDetails = mUserDetailsSub

        mHealthService?.createHealthPackage(mCreatePackageReq)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                showToastMessage("Success")
                SharedPreferenceManager.saveBoolean(
                    SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
                    true
                )
                observeAllUsersAnswers()
            }, {
                Log.d("VIJAY", "createHealthPackageError")
                showToastMessage("Server Error" + it.message)
            })
    }

    fun observeAllUsersAnswers() {
        val userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
        mHealthService?.getAllUsersAnswers(userId)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(Schedulers.io())
            ?.subscribe({
                if (it.isEmpty()) {
                    Navigator.navigateToOnboardQuestion(this)
                } else {
                    Navigator.navigateToDashboard(this)
                    SharedPreferenceManager.saveBoolean(
                        SharedPreferenceManager.KEY_SHOULD_SHOW_ONBOARD,
                        true
                    )
                }
                finish()
            }, {
                showToastMessage("Server Error" + it.message)
            })
    }

    private fun setPlanDetails(mGetAllPlansResponse: GetAllPlansResponse?): PlanDetailsPackage {
        val mCPlanDetails = PlanDetailsPackage()
        mCPlanDetails.amount = mGetAllPlansResponse?.amount
        mCPlanDetails._id = mGetAllPlansResponse?.id
        mCPlanDetails.planName = mGetAllPlansResponse?.planName
        mCPlanDetails.validity = mGetAllPlansResponse?.validity
        return mCPlanDetails
    }


}