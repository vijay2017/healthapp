package com.example.obhaiyyahealth.presentation.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.appwithmeflutter.mype.presentation.base.BaseVM
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Created by Vijay on 22/3/20.
 */

class UserDetailsVM : BaseVM() {

    var mMutableRegisterGmail = MutableLiveData<RegisterGmailModell>()

    fun registerGmailUser(gmailId: String, gToken: String) {
        mCustomerService?.registerGmailApi(gmailId, gToken)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                Log.d("VIJAY", "SUCCESS-OTP")
            }, {
                Log.d("VIJAY", "ERROR-OTP")
            })
    }

}


class RegisterGmailModell {
    var isExistingUser: Boolean = false
    var success: Boolean? = null
}