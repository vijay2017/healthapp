package com.obhaiyya.health.presentation.summary

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.OnTouchListener
import androidx.core.content.ContextCompat
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.dashboard.GetAllDashboardHealthPlanResponse
import com.zhouyou.view.seekbar.SignSeekBar
import kotlinx.android.synthetic.main.activity_summary.*


class SummaryActivity : BaseActivity(), View.OnClickListener {

    companion object {

        val KEY_GET_ALL_HEALTH_RESPONSE = "keyGetAllHealthResponse"

        fun getCallingIntent(
            context: Context,
            response: GetAllDashboardHealthPlanResponse
        ): Intent {
            val intent = Intent(context, SummaryActivity::class.java)
            intent.putExtra(KEY_GET_ALL_HEALTH_RESPONSE, response)
            return intent
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)
        viewBack.setOnClickListener(this)
        setToolbarWithTitle("All seems to be fine", false)
        val bundle = intent.extras

        if (bundle != null) {

            val getAllHealthDataResponse =
                bundle.getSerializable(KEY_GET_ALL_HEALTH_RESPONSE) as GetAllDashboardHealthPlanResponse

            val heartRate = getAllHealthDataResponse.heartRate
            val spo2 = getAllHealthDataResponse.spo2
            val respiration = getAllHealthDataResponse.respiration
            val stress = getAllHealthDataResponse.stress
            val hrv = getAllHealthDataResponse.hrv

            if (heartRate != 0) {
                cvHeartRate.visibility = View.VISIBLE
                txtHeartRate.text = heartRate.toString()
                heartRate?.toFloat()?.let {
                    setHeartRate(it)
                }
            }
            if (heartRate != 0) {
                cvHRV.visibility = View.VISIBLE
                txtHRV.text = hrv.toString()
                hrv?.toFloat()?.let {
                    setHRV(it)
                }
            }
            if (spo2 != 0) {
                cvSpo2Rate.visibility = View.VISIBLE
                txtSpo2.text = spo2.toString()
                spo2?.toFloat()?.let {
                    setO2(it)
                }
            }
            if (respiration != 0) {
                cvRespiration.visibility = View.VISIBLE
                txtRespiration.text = respiration.toString()
                respiration?.toFloat()?.let {
                    setRR(it)
                }
            }
            if (stress != 0) {
                cvStress.visibility = View.VISIBLE
                txtStress.text = stress.toString()
                stress?.toFloat()?.let {
                    setStress(it)
                }
            }
        }


    }

    fun setO2(progress: Float) {
        seekbarO2.getConfigBuilder()
            .min(80F)
            .max(100F)
            .progress(progress)
            .sectionCount(20)
            .trackColor(ContextCompat.getColor(this, R.color.color_good))
            .secondTrackColor(ContextCompat.getColor(this, R.color.green))
            .thumbColor(ContextCompat.getColor(this, R.color.color_concern))
            .sectionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .sectionTextSize(16)
            .thumbTextColor(ContextCompat.getColor(this, R.color.color_bad))
            .thumbTextSize(18)
            .signColor(ContextCompat.getColor(this, R.color.color_good))
            .signTextSize(18)
            .autoAdjustSectionMark()
            .sectionTextPosition(SignSeekBar.TextPosition.BELOW_SECTION_MARK)
            .build()

        seekbarO2.setOnTouchListener(OnTouchListener { view, motionEvent -> true })

    }


    fun setHeartRate(progress: Float) {
        seekbarHeartRate.getConfigBuilder()
            .min(55F)
            .max(230F)
            .progress(progress)
            .sectionCount(20)
            .trackColor(ContextCompat.getColor(this, R.color.color_good))
            .secondTrackColor(ContextCompat.getColor(this, R.color.green))
            .thumbColor(ContextCompat.getColor(this, R.color.color_concern))
            .sectionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .sectionTextSize(16)
            .thumbTextColor(ContextCompat.getColor(this, R.color.color_bad))
            .thumbTextSize(18)
            .signColor(ContextCompat.getColor(this, R.color.color_good))
            .signTextSize(18)
            .autoAdjustSectionMark()
            .sectionTextPosition(SignSeekBar.TextPosition.BELOW_SECTION_MARK)
            .build()

        seekbarHeartRate.setOnTouchListener(OnTouchListener { view, motionEvent -> true })

    }


    fun setRR(progress: Float) {
        seekbarRR.getConfigBuilder()
            .min(6F)
            .max(40F)
            .progress(progress)
            .sectionCount(20)
            .trackColor(ContextCompat.getColor(this, R.color.color_good))
            .secondTrackColor(ContextCompat.getColor(this, R.color.green))
            .thumbColor(ContextCompat.getColor(this, R.color.color_concern))
            .sectionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .sectionTextSize(16)
            .thumbTextColor(ContextCompat.getColor(this, R.color.color_bad))
            .thumbTextSize(18)
            .signColor(ContextCompat.getColor(this, R.color.color_good))
            .signTextSize(18)
            .autoAdjustSectionMark()
            .sectionTextPosition(SignSeekBar.TextPosition.BELOW_SECTION_MARK)
            .build()

        seekbarRR.setOnTouchListener(OnTouchListener { view, motionEvent -> true })


    }

    fun setHRV(progress: Float) {
        seekbarHRV.getConfigBuilder()
            .min(10F)
            .max(220F)
            .progress(progress)
            .sectionCount(4)
            .trackColor(ContextCompat.getColor(this, R.color.color_good))
            .secondTrackColor(ContextCompat.getColor(this, R.color.green))
            .thumbColor(ContextCompat.getColor(this, R.color.color_concern))
            .sectionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .sectionTextSize(16)
            .thumbTextColor(ContextCompat.getColor(this, R.color.color_bad))
            .thumbTextSize(18)
            .signColor(ContextCompat.getColor(this, R.color.color_good))
            .signTextSize(18)
            .autoAdjustSectionMark()
            .sectionTextPosition(SignSeekBar.TextPosition.BELOW_SECTION_MARK)
            .build()

        seekbarHRV.setOnTouchListener(OnTouchListener { view, motionEvent -> true })

    }

    fun setStress(progress: Float) {
        seekbarStress.getConfigBuilder()
            .min(1F)
            .max(5F)
            .progress(progress)
            .sectionCount(4)
            .trackColor(ContextCompat.getColor(this, R.color.color_good))
            .secondTrackColor(ContextCompat.getColor(this, R.color.green))
            .thumbColor(ContextCompat.getColor(this, R.color.color_concern))
            .sectionTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .sectionTextSize(16)
            .thumbTextColor(ContextCompat.getColor(this, R.color.color_bad))
            .thumbTextSize(18)
            .signColor(ContextCompat.getColor(this, R.color.color_good))
            .signTextSize(18)
            .autoAdjustSectionMark()
            .sectionTextPosition(SignSeekBar.TextPosition.BELOW_SECTION_MARK)
            .build()

        seekbarStress.setOnTouchListener(OnTouchListener { view, motionEvent -> true })
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.viewBack -> {
                finish()
            }
        }
    }


}