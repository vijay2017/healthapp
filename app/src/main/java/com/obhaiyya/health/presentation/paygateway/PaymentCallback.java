package com.obhaiyya.health.presentation.paygateway;

import java.util.Map;

import retrofit2.Call;

public interface PaymentCallback {

    void paymentSuccess(Map<String, String> map);

    void paymentFailure(Map<String, String> map);

    void tokenFetchedSuccessfully();

    void onErrorFetchingToken();

    void onNetworkUnavailableBeforePayment(Call call, Throwable t);

    void onBackPressedFromPg();
}
