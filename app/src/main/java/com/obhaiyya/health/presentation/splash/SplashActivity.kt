package com.obhaiyya.health.presentation.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.activity.viewModels
import com.example.obhaiyyahealth.presentation.base.LoginVM
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.CustomCashFreeCB
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.utils.Navigator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers


/**
 * Created by Vijay on 7/3/20.
 */

class SplashActivity : BaseActivity(), CustomCashFreeCB.OnCustomCashFreeCallback {

    val mLoginVM: LoginVM by viewModels()
    var mobileNum = ""
    var mTermsAndConditionSelected = false
    var appUpdateManager: AppUpdateManager? = null
    var email = ""

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        email = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL) ?: ""

        if (email.isNotEmpty()) {
            observeUserDataByEmail()
        } else {
            navigateToParticularScreen(null)
        }

        appUpdateManager = AppUpdateManagerFactory.create(this)
        appUpdateCheck()


    }

    private fun observeUserDataByEmail() {
        mHealthService?.getUserDataByEmail(email)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                navigateToParticularScreen(it.active)
            }, {
                showToastMessage("Failed to load user data" + it.message)
                finish()
            })
    }

    fun navigateToParticularScreen(isActive: Boolean?) {
        val shouldNotShowUserDetails =
            SharedPreferenceManager.getBoolean(SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_USER_DETAILS)
                ?: false
        val isUserLoggedIn =
            SharedPreferenceManager.getBoolean(SharedPreferenceManager.KEY_USER_LOGGED_IN)
                ?: false

        val shouldNotShowWalkThrough =
            SharedPreferenceManager.getBoolean(SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_WALKTHROUGH)
                ?: false
        val shouldNotShowOnboard =
            SharedPreferenceManager.getBoolean(SharedPreferenceManager.KEY_SHOULD_SHOW_ONBOARD)
                ?: false
        val userIdExist = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID)
            ?: ""

        if (isActive != null) {
            SharedPreferenceManager.saveBoolean(
                SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
                isActive
            )
        }

        // regular user and company user
        val shouldNotShowPackage =
            SharedPreferenceManager.getBoolean(SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package)
                ?: false

        Handler(Looper.getMainLooper()).postDelayed({
            if (!isUserLoggedIn) {
                Navigator.navigateToLogin(this)
                finish()
            } else if (!shouldNotShowWalkThrough) {
                Navigator.navigateToWalkThrough(this)
                finish()
            } else if (!shouldNotShowUserDetails) {
                Navigator.navigateToUserDetails(this)
                finish()
            } else if (!shouldNotShowPackage) {
                Navigator.navigateToPackage(this, false)
                finish()
            } else if (!shouldNotShowOnboard) {
                Navigator.navigateToOnboardQuestion(this)
                finish()
            } else if (userIdExist.isNotEmpty() && shouldNotShowOnboard) {
                Navigator.navigateToDashboard(this)
                finish()
            }
        }, 2000)


    }

    fun appUpdateCheck() {
        val appUpdateInfoTask = appUpdateManager?.getAppUpdateInfo()
        appUpdateInfoTask?.addOnSuccessListener { appUpdateInfo: AppUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(
                    AppUpdateType.IMMEDIATE
                )
            ) {
                try {
                    appUpdateManager?.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.IMMEDIATE,
                        this,
                        1003
                    );
                } catch (e: Exception) {
                    e.printStackTrace();
                }
            }
        }
        appUpdateInfoTask?.addOnFailureListener { ai: Exception ->
            Log.d("appUpdateInfoTask", "" + ai)
        }
    }

    override fun onResume() {
        super.onResume()
        appUpdateManager
            ?.getAppUpdateInfo()
            ?.addOnSuccessListener { appUpdateInfo: AppUpdateInfo ->
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    // If an in-app update is already running, resume the update.
                    try {
                        appUpdateManager?.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.IMMEDIATE,
                            this,
                            1003
                        );
                    } catch (e: Exception) {
                        e.printStackTrace();
                    }
                }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1003) {
            if (resultCode != RESULT_OK) {
                Log.d("Vijay-----", "Success AppUpdate----1")
            }
            Log.d("Vijay-----", "Success AppUpdate----2")
        }
    }

    override fun onSuccess(value: MutableMap<String, String>?) {

    }

    override fun onFailure() {

    }

    override fun onNavigateBackWithoutPayment() {

    }

}