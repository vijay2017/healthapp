package com.obhaiyya.health.presentation.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.userdetails.UserRegistrationGmailResponse
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_nav_package.*

class NavPackageActivity : BaseActivity() {

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, NavPackageActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_package)
        setToolbarWithTitle("Package", false)
        checkCompanyOrRegularUser()

    }

    private fun checkCompanyOrRegularUser() {
        // For company user
        val userDetailJson =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_DETAILS)
        if (userDetailJson != null && userDetailJson.isNotEmpty()) {
            val jsonUserDetails =
                Gson().fromJson(userDetailJson, UserRegistrationGmailResponse::class.java)
            val isActive = jsonUserDetails.active
            if (isActive != null && isActive == true) {
                cvNavPackageCompany.visibility = View.VISIBLE
                val consumedDate = DateFormatUtils.getFormattedDateInDDMMYYYY(
                    jsonUserDetails.consumedAccessCodeDate ?: ""
                ) ?: ""
                val thirtyDaysAdded = DateFormatUtils.getNoOfDaysToDate(30, consumedDate)
                txtEndDateNavPackage.setText("" + thirtyDaysAdded)

            } else {
                cvNavPackageCompany.visibility = View.GONE
                isHealthPlanActive()
            }
        }
    }

    fun isHealthPlanActive() {
        showProgress(this)
        val userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
        mHealthService?.isActiveHealthPlan(userId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                hideProgress()
                if (it.isNotEmpty()) {
                    val layputManager =
                        LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                    rvNavPackage.layoutManager = layputManager
                    val adapter = NavPackageAdapter(this, it)
                    rvNavPackage.adapter = adapter
                } else {
                    txtNoSubscrition.visibility = View.VISIBLE
                }

            }, {
                hideProgress()
                Log.d("VIJAY", "ERROR-OTP" + it)
            })
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}