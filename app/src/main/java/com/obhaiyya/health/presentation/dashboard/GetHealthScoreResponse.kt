package com.obhaiyya.health.presentation.dashboard

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class GetHealthScoreResponse {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("healthScore")
    @Expose
    var healthScore: Double? = null

}