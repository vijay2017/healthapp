package com.obhaiyya.health.presentation.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.obhaiyyahealth.presentation.base.LoginVM
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.utils.Navigator
import kotlinx.android.synthetic.main.activity_validate_otp.*

/**
 * Created by Vijay on 7/3/20.
 */

class ValidateOTPActivity : BaseActivity(), View.OnClickListener {

    val mLoginVM: LoginVM by viewModels()
    var mobileNum = ""
    var mTermsAndConditionSelected = false


    companion object {
        fun getCallingIntent(context: Context, mobileNumber: String): Intent {
            val intent = Intent(context, ValidateOTPActivity::class.java)
            intent.putExtra("MobileNumber", mobileNumber)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validate_otp)
        btnVerify.setOnClickListener(this)
        btnSendAgain.setOnClickListener(this)

        val bundle = intent.extras
        mobileNum = bundle?.getString("MobileNumber") as String

        observeRetryGenerateOTP()
        observeValidateOTP()

    }

    private fun observeValidateOTP() {
        mLoginVM.mMutableValidateOTP.observe(this, Observer {
            if (it.success ?: false) {
                Navigator.navigateToWalkThrough(this)
                saveLogIn()
            }
            hideProgress()
        })
    }

    fun saveLogIn() {
        SharedPreferenceManager.saveBoolean(SharedPreferenceManager.KEY_USER_LOGGED_IN, true)
        SharedPreferenceManager.saveString(SharedPreferenceManager.KEY_MOBILE_NUMBER, mobileNum)
    }


    private fun observeRetryGenerateOTP() {
        mLoginVM.mMutableGenerateOTP.observe(this, Observer {
            if (it.isSuccess) {

            }
            hideProgress()
        })
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnVerify -> {
                var edOtp = edtOtp.text.toString()
                if (!edOtp.isNullOrEmpty()) {
                    showProgress(this)
                    mLoginVM.validateOTP(mobileNum, edOtp)
                }
            }
            R.id.btnSendAgain -> {
                if (mobileNum.isNotEmpty()) {
                    showProgress(this)
                    mLoginVM.generateOTP(mobileNum)
                }
            }

        }
    }


}