package com.obhaiyya.health.presentation.posthealthquestion

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CheckRiskAssesmentResponse {

    @SerializedName("covidPossibility")
    @Expose
    var covidPossibility: String? = null

}