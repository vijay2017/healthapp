package com.obhaiyya.health.presentation.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.obhaiyyahealth.dataProvider.ApiService
import com.obhaiyya.health.HealthApplication
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager


/**
 * Created by Vijay on 4/4/20.
 */

abstract class BaseFragment : Fragment() {

    var dialog: AlertDialog? = null
    var mCustomerService = ApiService.getInstance()?.call()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = LayoutInflater.from(activity).inflate(getLayout(), container, false)
        return view
    }

    abstract fun getLayout(): Int


    fun showProgress(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setView(R.layout.dialog_progress)
        dialog = builder.create()
        dialog?.show()
    }

    fun hideProgress() {
        if (dialog?.isShowing == true) {
            dialog?.dismiss()
        }
    }

    fun replaceFragment(@IdRes containerViewId: Int, @NonNull fragment: Fragment) {
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            ?.replace(containerViewId, fragment)
            ?.commit()
    }

    fun replaceChildFragment(@IdRes containerViewId: Int, @NonNull fragment: Fragment) {
        childFragmentManager?.beginTransaction()
            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
            .replace(containerViewId, fragment)
            .commit()
    }

    fun checkInternetConnection(): Boolean? {
        return HealthApplication.Companion.getAppInstance()?.isNetworkConnected()
    }

    fun checkInternetConnectionMsg() {
        Toast.makeText(activity, "Check Internet Connection", Toast.LENGTH_SHORT).show()
    }

    fun showToastMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

}