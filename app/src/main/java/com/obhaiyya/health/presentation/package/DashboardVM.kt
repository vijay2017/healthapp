package com.example.obhaiyyahealth.presentation.base

import android.util.Log
import com.appwithmeflutter.mype.presentation.base.BaseVM
import com.obhaiyya.health.presentation.Package.GetAllPlansResponse
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Created by Vijay on 22/3/20.
 */

class DashboardVM(val iDashboardVM: IDashboardVM) : BaseVM() {



}

interface IDashboardVM {
    fun isActiveResponseSuccess(it: List<GetAllPlansResponse>)
    fun isActiveResponseFailure(it: Throwable)
}
