package com.obhaiyya.health.presentation.onboard

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_first_question.*
import kotlinx.android.synthetic.main.fragment_second_question.*

class SecondQuestionFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    var firstResponse: OnboardQuestionResponse? = null
    var secondResponse: OnboardQuestionResponse? = null
    var thirdResponse: OnboardQuestionResponse? = null


    companion object {

        val KEY_QUESTION_FIRST = "KEY_QUESTION_FIRST"
        val KEY_QUESTION_SECOND = "KEY_QUESTION_SECOND"
        val KEY_QUESTION_THIRD = "KEY_QUESTION_THIRD"

        fun newInstance(
                mListResponse: OnboardQuestionResponse,
                secondQues: OnboardQuestionResponse,
                thirdQues: OnboardQuestionResponse
        ): SecondQuestionFragment {
            val firstQuestionFragment = SecondQuestionFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_QUESTION_FIRST, mListResponse)
            bundle.putSerializable(KEY_QUESTION_SECOND, secondQues)
            bundle.putSerializable(KEY_QUESTION_THIRD, thirdQues)
            firstQuestionFragment.arguments = bundle
            return firstQuestionFragment
        }

    }

    override fun getLayout(): Int {
        return R.layout.fragment_second_question
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as OnboardQuestionActivity).hideBtnVisibility()

        firstResponse = arguments?.getSerializable(KEY_QUESTION_FIRST) as OnboardQuestionResponse
        secondResponse = arguments?.getSerializable(KEY_QUESTION_SECOND) as OnboardQuestionResponse
        thirdResponse = arguments?.getSerializable(KEY_QUESTION_THIRD) as OnboardQuestionResponse

        txt4Question.text = firstResponse?.question
        txt5Question.text = secondResponse?.question
        txt6Question.text = thirdResponse?.question

        rdGrp4.setOnCheckedChangeListener(this)
        rdGrp5.setOnCheckedChangeListener(this)
        rdGrp6.setOnCheckedChangeListener(this)
        btnSecondFrag.setOnClickListener(this)

    }


    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtn4Yes -> {
                checkAnswer(4, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtn4tNo -> {
                checkAnswer(4, OnboardQuestionActivity.KEY_NO)
            }
            R.id.rdBtn5Yes -> {
                checkAnswer(5, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtn5No -> {
                checkAnswer(5, OnboardQuestionActivity.KEY_NO)
            }
            R.id.rdBtn6Yes -> {
                checkAnswer(6, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtn6No -> {
                checkAnswer(6, OnboardQuestionActivity.KEY_NO)
            }
        }
    }

    fun checkAnswer(pos: Int, ans: String) {
        when (pos) {
            4 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID)
                        ?: ""
                ansReq.answer = ans
                ansReq.question = firstResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(pos, ansReq)
                checkAllAnswered()
            }
            5 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID)
                        ?: ""
                ansReq.answer = ans
                ansReq.question = secondResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(pos, ansReq)
                checkAllAnswered()
            }
            6 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID)
                        ?: ""
                ansReq.answer = ans
                ansReq.question = thirdResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(pos, ansReq)
                checkAllAnswered()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSecondFrag -> {
            }
        }
    }

    fun checkAllAnswered() {
        val answer1 = rdBtn4Yes.isChecked || rdBtn4tNo.isChecked
        val answer2 = rdBtn5Yes.isChecked || rdBtn5No.isChecked
        val answer3 = rdBtn6Yes.isChecked || rdBtn6No.isChecked

        if (answer1 && answer2 && answer3) {
            btnSecondFrag.visibility = View.GONE
            (activity as OnboardQuestionActivity).showBtnVisibility(2)
        }

    }


}