package com.obhaiyya.health.presentation.walkthrough

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.obhaiyya.health.presentation.base.BaseFragment

class WalkThroughAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): BaseFragment {
        when (position) {
            0 -> return FirstWThroughFragment()
            1 -> return SecondWThroughFragment()
            2 -> return FinalWThroughFragment()
            else -> {
                return FirstWThroughFragment()
            }
        }
    }


}

