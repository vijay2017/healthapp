package com.obhaiyya.health.presentation.history

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TrendsLogsResponse : Serializable {

    @SerializedName("heartRate")
    @Expose
    var heartRate: List<HeartRate>? = null

    @SerializedName("temperature")
    @Expose
    var temperature: List<Temperature>? = null

    @SerializedName("respiration")
    @Expose
    var respiration: List<Respiration>? = null

    @SerializedName("spo2")
    @Expose
    var spo2: List<Spo2>? = null

    @SerializedName("stress")
    @Expose
    var stress: List<Stress>? = null

    @SerializedName("hrv")
    @Expose
    var hrv: List<HRV>? = null

    /* @SerializedName("bodyMass")
     @Expose
     var bodyMass: List<BodyMas>? = null*/

}

class HeartRate : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("healthList")
    @Expose
    var healthList: List<Health>? = null

}


class HRV : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("healthList")
    @Expose
    var healthList: List<Health__5>? = null

}

class Respiration : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("healthList")
    @Expose
    var healthList: List<Health__2>? = null
}

class Spo2 : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("healthList")
    @Expose
    var healthList: List<Health__3>? = null
}

class Stress : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("healthList")
    @Expose
    var healthList: List<Health__4>? = null

}

class Temperature : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("healthList")
    @Expose
    var healthList: List<Health__1>? = null

}

class Health : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("day")
    @Expose
    var day: String? = null

    @SerializedName("dateTime")
    @Expose
    var dateTime: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}

class Health__1 : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("day")
    @Expose
    var day: String? = null

    @SerializedName("dateTime")
    @Expose
    var dateTime: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}


class Health__2 : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("day")
    @Expose
    var day: String? = null

    @SerializedName("dateTime")
    @Expose
    var dateTime: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}


class Health__3 : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("day")
    @Expose
    var day: String? = null

    @SerializedName("dateTime")
    @Expose
    var dateTime: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}


class Health__4 : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("day")
    @Expose
    var day: String? = null

    @SerializedName("dateTime")
    @Expose
    var dateTime: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}


class Health__5 : Serializable {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("day")
    @Expose
    var day: String? = null

    @SerializedName("dateTime")
    @Expose
    var dateTime: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}


