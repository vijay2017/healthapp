package com.obhaiyya.health.presentation.history

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_logs.*


class LogsFragment : BaseFragment() {

    override fun getLayout(): Int {
        return R.layout.fragment_logs
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            showProgress(it)
        }
        observeLogsData()

    }

    private fun observeLogsData() {
        val userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
        mCustomerService?.getLogsData(userId, "30")
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                Log.d("VIJAY----", "Success")
                setRecyclerLogs(it)
                hideProgress()
            }, {
                hideProgress()
                Log.d("VIJAY----", "Failure ")
            })

    }

    fun setRecyclerLogs(list: List<LogsResponse>) {
        activity?.let {
            val linearLayoutManager = LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
            rvLogs.layoutManager = linearLayoutManager
            val adapter = LogsAdapter(it, list)
            rvLogs.adapter = adapter
        }

    }


}