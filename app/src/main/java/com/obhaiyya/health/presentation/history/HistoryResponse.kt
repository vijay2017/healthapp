package com.obhaiyya.health.presentation.history

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class HistoryResponse : Serializable {

    @SerializedName("heartRate")
    @Expose
    var heartRate: List<HeartRateHistory>? = null

    @SerializedName("temperature")
    @Expose
    var temperatureHistory: List<TemperatureHistory>? = null

    @SerializedName("respiration")
    @Expose
    var respiration: List<RespirationHistory>? = null

    @SerializedName("spo2")
    @Expose
    var spo2: List<Spo2History>? = null

    @SerializedName("stress")
    @Expose
    var stress: List<StressHistory>? = null

    @SerializedName("hrv")
    @Expose
    var hrv: List<HRVHistory>? = null

    @SerializedName("bodyMass")
    @Expose
    var bodyMass: List<BodyMasHistory>? = null

}

class BodyMasHistory {
    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}

class HeartRateHistory {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}

class HRVHistory {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}

class RespirationHistory {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}

class Spo2History {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}

class StressHistory {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null


}

class TemperatureHistory {

    @SerializedName("date")
    @Expose
    var date: String? = null

    @SerializedName("value")
    @Expose
    var value: Int? = null

}