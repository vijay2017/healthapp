package com.obhaiyya.health.presentation.paygateway;

public interface PopupCallback {

    int ON_CLICK_GPAY = 100;
    int ON_CLICK_PAY_GATEWAY = 101;
    int ON_CLICK_CASH = 102;
    int ON_POPUP_DISMISSED = 103;

    void performPopupAction(int actionCode, Object data);
}
