package com.obhaiyya.health.presentation.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.obhaiyyahealth.presentation.base.LoginVM
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.dashboard.LogoutModel
import com.obhaiyya.health.presentation.Package.UserDetails
import com.obhaiyya.health.presentation.utils.Navigator
import com.obhaiyya.health.presentation.utils.RxBusNew
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by Vijay on 7/3/20.
 */

class LoginActivity : BaseActivity(), View.OnClickListener {

    val mLoginVM: LoginVM by viewModels()
    var mobileNum = ""
    var mGoogleSignInClient: GoogleSignInClient? = null
    var mEmail = ""
    var isNormalUserSubscribed = false
    var userId = ""


    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        txtGoogle.setOnClickListener(this)
        btnSendOTP.setOnClickListener(this)

        var key = ""
//        key = getString(R.string.server_id_release)
        key = getString(R.string.server_id_debug)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(key)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        subscribeLogout()
        observeGenerateOTP()
        observeRegisterGmailUser()

    }

    private fun subscribeLogout() {
        RxBusNew.listen(LogoutModel::class.java).subscribe {
            if (it.isLogoutRequest) {
                mGoogleSignInClient?.signOut()
            }
        }
    }

    private fun observeGenerateOTP() {
        mLoginVM.mMutableGenerateOTP.observe(this, Observer {
            if (it.isSuccess) {
                if (mobileNum.isNotEmpty()) {
                    Navigator.navigateToValidateOTP(this, mobileNum)
                }
            }
            hideProgress()
        })
    }

    private fun observeRegisterGmailUser() {
        mLoginVM.mMutableRegisterGmail.observe(this, Observer {
            if (it.mGmailResponse?.isExistingUser ?: false && it.mGmailResponse?.userDetails != null) {
                userId = it.mGmailResponse?.userDetails?.id ?: ""
                saveWalkthroughStatus()
                it.mGmailResponse?.userDetails?.let {
                    isActiveHealthPlan(it.active)
                    saveUserDetails(it)
                }

            } else if (it.success ?: false) {
                Navigator.navigateToWalkThrough(this)
                SharedPreferenceManager.saveString(SharedPreferenceManager.KEY_USER_EMAIL, mEmail)
                finish()
            }
            saveLogIn()
        })
    }

    private fun saveUserDetails(userDetails: UserDetails) {
        val userDetailsJson = Gson().toJson(userDetails)
        SharedPreferenceManager.saveString(
            SharedPreferenceManager.KEY_USER_DETAILS,
            userDetailsJson
        )

        val isActive = userDetails.active ?: false
        SharedPreferenceManager.saveBoolean(
            SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
            isActive
        )
        SharedPreferenceManager.saveString(
            SharedPreferenceManager.KEY_USER_ID,
            userDetails.id ?: ""
        )
        SharedPreferenceManager.saveString(
            SharedPreferenceManager.KEY_USER_EMAIL,
            userDetails.email ?: ""
        )
    }

    fun saveWalkthroughStatus() {
        SharedPreferenceManager.saveBoolean(
            SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_WALKTHROUGH,
            true
        )
        SharedPreferenceManager.saveBoolean(
            SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_USER_DETAILS,
            true
        )
    }

    fun saveLogIn() {
        SharedPreferenceManager.saveBoolean(
            SharedPreferenceManager.KEY_USER_LOGGED_IN,
            true
        )
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtGoogle -> {
                signIn()
            }
            R.id.btnSendOTP -> {
                mobileNum = edtMobileNumber.text.toString()
                if (!mobileNum.isEmpty()) {
                    showProgress(this)
                    mLoginVM.generateOTP(mobileNum)
                } else {
//                    showToastMessage("")
                    hideProgress()
                }
            }
        }
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient?.signInIntent
        startForResult.launch(signInIntent)
    }


    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                val task: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(intent)
                handleSignInResult(task)
            } else if (result.resultCode == Activity.RESULT_CANCELED) {
                showToastMessage("Something went wrong")
            }
        }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
//            mEmail = "vijaytest@gmail.com" ?: ""
            mEmail = account?.email ?: ""
            mLoginVM.registerGmailUser(mEmail ?: "", account?.id ?: "")
            showToastMessage("Sign In successfully" + " " + mEmail);
        } catch (e: ApiException) {

        }
    }

    fun observeAllUsersAnswers(userId: String, active: Boolean?) {
        mHealthService?.getAllUsersAnswers(userId)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(Schedulers.io())
            ?.subscribe({
                if (active == null && !isNormalUserSubscribed) {
                    Navigator.navigateToPackage(this, false)
                    finish()
                } else if (active == false) {
                    Navigator.navigateToPackage(this, false)
                    finish()
                } else if (it.isEmpty()) {
                    Navigator.navigateToOnboardQuestion(this)
                    finish()
                } else {
                    Navigator.navigateToDashboard(this)
                    SharedPreferenceManager.saveBoolean(
                        SharedPreferenceManager.KEY_SHOULD_SHOW_ONBOARD,
                        true
                    )
                    finish()
                }
            }, {

            })
    }

    fun isActiveHealthPlan(active: Boolean?) {

        mHealthService?.isActiveHealthPlan(userId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                hideProgress()
                if (active == null) {
                    isNormalUserSubscribed = !it.isEmpty()
                    SharedPreferenceManager.saveBoolean(
                        SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
                        isNormalUserSubscribed
                    )
                    //company user
                } else if (active != null) {
                    SharedPreferenceManager.saveBoolean(
                        SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_Package,
                        active
                    )
                }
                observeAllUsersAnswers(userId, active)
            }, {
                hideProgress()
                Log.d("VIJAY", "ERROR-OTP")
                isNormalUserSubscribed = false
            })
    }


    override fun onDestroy() {
        try {
            subscribeLogout()
        } catch (e: Exception) {

        }
        super.onDestroy()

    }

}
