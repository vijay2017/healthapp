package com.obhaiyya.health.presentation;


import com.closerbuy.customerapp.network.CashFreeReponse;
import com.closerbuy.customerapp.network.CashFreeRequest;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface CashFreeApiService {

    @POST("cftoken/order")
    Observable<CashFreeReponse> getCFTOKEN(@Header("x-client-id") String clientId,
                                           @Header("x-client-secret") String clientSecret,
                                           @Body CashFreeRequest cashFreeRequest);

}
