package com.obhaiyya.health.presentation.notification

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.obhaiyya.health.presentation.scanner.HomeActivity
import com.obhaiyya.health.presentation.utils.Constant.Companion.IS_INTENT_FROM_NOTIF
import com.obhaiyya.health.presentation.utils.Constant.Companion.KEY_ACTION
import com.obhaiyya.health.presentation.utils.Constant.Companion.NOTIFICATION_ID
import com.obhaiyya.health.presentation.utils.Constant.Companion.RX_ACTION

class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        if (intent!!.action.equals(
                RX_ACTION,
                ignoreCase = true
            )
        ) {
            val action = intent!!.extras!!.getString(KEY_ACTION)
            val notifId = intent!!.extras!!.getInt(NOTIFICATION_ID)

            goToMainActivityWithCategory(context!!)

            val manager =
                context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.cancel(notifId)
            context!!.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
        }
    }


    private fun goToMainActivityWithCategory(
        context: Context
    ) {
        val mainIntent = Intent(context, com.obhaiyya.health.presentation.scanner.HomeActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        mainIntent.putExtra(IS_INTENT_FROM_NOTIF,
            true
        )

        context.startActivity(mainIntent)
    }
}