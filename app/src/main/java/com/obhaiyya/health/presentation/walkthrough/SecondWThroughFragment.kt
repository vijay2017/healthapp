package com.obhaiyya.health.presentation.walkthrough

import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseFragment

class SecondWThroughFragment : BaseFragment() {
    override fun getLayout(): Int {
        return R.layout.fragment_secondwalkthrough
    }
}