package com.obhaiyya.health.presentation.userdetails

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.RadioGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.dashboard.ValidateAccessCodeRequest
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import com.obhaiyya.health.presentation.utils.Navigator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_user_details.*
import java.util.*

class UserDetailsActivity : BaseActivity(), View.OnClickListener,
    RadioGroup.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    private var _day = 0
    private var _month = 0
    private var _birthYear = 0
    var mGender = "Male"
    var isAccessCodeSuccess = false
    var name = ""
    var height = ""
    var weight = ""
    var dob = ""
    var email = ""
    var mobileNumber = ""
    var isCompanyUser = false
    var accessCode = ""

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, UserDetailsActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)
        btnUserContinue.setOnClickListener(this)
        edtDOB.setOnClickListener(this)
        rdGrpGender.setOnCheckedChangeListener(this)
        rdGrpCompanyOrNormal.setOnCheckedChangeListener(this)
        rdBtnMale.isChecked = true

    }

    fun loadUserRegistration() {
        showProgress(this)
        val userDetailsRequest = UserDetailsRequest()
        userDetailsRequest.name = txtNameUserRegis.text.toString()
        userDetailsRequest.email =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL) ?: ""
        userDetailsRequest.gender = mGender
        userDetailsRequest.dateOfBirth = formatDate() ?: ""
        userDetailsRequest.height = txtHeightUserRegis.text.toString()
        userDetailsRequest.weight = txtWeightUserRegis.text.toString()
        userDetailsRequest.mobileNumber = txtMobileNumber.text.toString()
        userDetailsRequest.accessCode =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_ACCESS_CODE) ?: ""

        mHealthService?.userRegistrationUserGmail(userDetailsRequest)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                onUserRegistrationSuccess(it)
                hideProgress()
            }, {
                hideProgress()
                onUserRegistrationError(it)
                showToastMessage("Server Error" + it.message)
            })

    }

    private fun onUserRegistrationSuccess(response: UserRegistrationGmailResponse?) {
        SharedPreferenceManager.saveBoolean(
            SharedPreferenceManager.KEY_SHOULD_SHOW_NOT_USER_DETAILS,
            true
        )
        SharedPreferenceManager.saveString(
            SharedPreferenceManager.KEY_USER_ID,
            response?._id ?: ""
        )
        val userDetailsJson = Gson().toJson(response)
        SharedPreferenceManager.saveString(
            SharedPreferenceManager.KEY_USER_DETAILS,
            userDetailsJson
        )
        if (isAccessCodeSuccess) {
            Navigator.navigateToOnboardQuestion(this)
            finish()
        } else {
            Navigator.navigateToPackage(this, false)
            finish()
        }
    }

    private fun onUserRegistrationError(it: Throwable?) {
        Log.d("VIjay-error", "" + it?.message)
        showToastMessage("" + it?.message)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnUserContinue -> {
                accessCode = txtCompanyCode.text.toString()
                name = txtNameUserRegis.text.toString()
                height = txtHeightUserRegis.text.toString()
                weight = txtWeightUserRegis.text.toString()
                dob = edtDOB.text.toString()
                email = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL)
                    ?: ""
                mobileNumber = txtMobileNumber.text.toString()

                if (!isCompanyUser) {
                    accessCode = ""
                }

                if (name.isEmpty()) {
                    showToastMessage("Name cannot be empty")
                } else if (height.isEmpty()) {
                    showToastMessage("Height cannot be empty")
                } else if (weight.isEmpty()) {
                    showToastMessage("Weight cannot be empty")
                } else if (dob.isEmpty()) {
                    showToastMessage("Dob cannot be empty")
                } else if (mobileNumber.isEmpty()) {
                    showToastMessage("Mobile Number cannot be empty")
                } else if (rdBtnNormal.isChecked == false && rdBtnCompany.isChecked == false) {
                    showToastMessage("Please select Regular user or Company user")
                } else if (isCompanyUser && accessCode.isEmpty()) {
                    showToastMessage("Company user selected : Please enter your access code")
                } else if (accessCode.isNotEmpty()) {
                    val intent = Intent(this, DisclaimerActivity::class.java)
                    startForResult.launch(intent)
                } else {
                    val intent = Intent(this, DisclaimerActivity::class.java)
                    startForResult.launch(intent)
                }

            }
            R.id.edtDOB -> {
                val calendar: Calendar = Calendar.getInstance(TimeZone.getDefault())
                val dialog = DatePickerDialog(
                    this, this,
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                )
                dialog.show()
            }
        }
    }

    val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                if (accessCode.isNotEmpty()) {
                    validateAccessCode(accessCode)
                } else {
                    loadUserRegistration()
                }
            }
        }


    override fun onDateSet(p0: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        _birthYear = year;
        _month = monthOfYear;
        _day = dayOfMonth;
        updateDisplay();
    }

    private fun updateDisplay() {
        val dob = StringBuilder() // Month is 0 based so add 1
            .append(_day).append("/").append(_month + 1).append("/").append(_birthYear)
            .toString()
        edtDOB.setText(dob)
        formatDate()
    }

    private fun formatDate(): String {
        val month = _month + 1
        val day = _day + 1
        val date = "" + day + "/" + month + "/" + _birthYear
        return DateFormatUtils.getSelectedDateInUTCFormat(date) ?: ""
    }

    fun validateAccessCode(accCode: String) {
        val valiAccCode = ValidateAccessCodeRequest()
        valiAccCode.email =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL) ?: ""
        valiAccCode.accessCode = accCode
        mHealthService?.validateAccessCode(valiAccCode)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                hideProgress()
                SharedPreferenceManager.saveString(
                    SharedPreferenceManager.KEY_ACCESS_CODE,
                    accCode
                )
                isAccessCodeSuccess = true
                showToastMessage("Code Validated")
                loadUserRegistration()
            }, {
                hideProgress()
                isAccessCodeSuccess = false
                showToastMessage("Please enter valid code" + it.message)
            })
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtnMale -> {
                Log.d("VIjay-error", "Male")
                mGender = "Male"
            }
            R.id.rdBtnFeMale -> {
                mGender = "Female"
                Log.d("VIjay-error", "Female")
            }
            R.id.rdBtnOthers -> {
                mGender = "Others"
                Log.d("VIjay-error", "Others")
            }
            R.id.rdBtnNormal -> {
                txtInputCompanyUser.visibility = View.GONE
                isCompanyUser = false
                txtCompanyCode.setText("")
            }
            R.id.rdBtnCompany -> {
                txtInputCompanyUser.visibility = View.VISIBLE
                isCompanyUser = true
            }
        }
    }


}