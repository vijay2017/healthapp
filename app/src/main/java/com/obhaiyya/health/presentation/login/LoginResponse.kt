package com.obhaiyya.health.presentation.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginResponse : Serializable {

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("otp")
    @Expose
    var otp: String? = null


}