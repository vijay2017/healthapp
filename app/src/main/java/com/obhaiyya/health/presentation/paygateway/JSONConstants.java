package com.obhaiyya.health.presentation.paygateway;

public class JSONConstants {

    public static final String APP_ID = "appId";
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_AMT = "orderAmount";
    public static final String CUST_NAME = "customerName";
    public static final String CUST_MOBILE = "customerPhone";
    public static final String CUST_EMAIL = "customerEmail";
    public static final String PAYMENT_MODES = "paymentModes";
    public static final String ORDER_CURRENCY = "orderCurrency";

    public static final String TX_STATUS = "txStatus";
    public static final String TX_TIME = "txTime";
    public static final String PAYMENT_MODE = "paymentMode";
    public static final String SIGNATURE = "signature";
    public static final String SIGN = "signature";
    public static final String TX_MSG = "txMsg";
    public static final String REF_ID = "referenceId";
    public static final String SUCCESS = "SUCCESS";

    public static final String PAYMENT_REF_ID = "referenceId";
    public static final String PAYMENT_ORDER_AMT = "orderAmount";

    public static final String COMPLAINT_ID = "complaintId";
    public static final String COMPLAINT_MESG = "message";
    public static final String FEEDBACK = "feedback";
    public static final String PAYMENT_TYPE_ONLINE = "online";
    public static final String OTP = "otp";
    public static final String HEADER_ACCESS_TOKEN = "AccessToken";
    public static final String PAYEE_VPA_KEY = "pa";
    public static final String PAYEE_VPA_VALUE = "obhaiyya@idfcbank";
//    public static final String PAYEE_VPA_VALUE = "9036090370@okbizaxis";

    public static final String PAYEE_NAME_KEY = "pn";
    public static final String PAYEE_NAME_VALUE = "O Bhaiyya";
    public static final String PAYEE_MERCHANT_CODE_KEY = "mc";
    public static final String PAYEE_TX_REF_ID_KEY = "tr";
    public static final String PAYEE_TX_DESC_KEY = "tn";
    public static final String PAYEE_TX_AMT_KEY = "am";
    public static final String PAYEE_CURR_KEY = "cu";
    public static final String PAYEE_CURR_VALUE = "INR";
    public static final String PAYEE_MERCHANT_CODE_VALUE = "5311";
}
