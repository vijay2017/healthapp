package com.obhaiyya.health.presentation.paygateway;

import retrofit2.Call;

public interface GpayMakerCallback {

    void onNoNetworkFoundForGpay(Call call, Throwable t);

    void onTxRefIdFetched(String txId);

    void onErrorFetchingTxRefId();

}
