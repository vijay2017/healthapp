package com.obhaiyya.health.presentation.paygateway;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.obhaiyya.health.data.SharedPreferenceManager;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TokenOtherDataModel implements Parcelable {


    public static final Creator<TokenOtherDataModel> CREATOR = new Creator<TokenOtherDataModel>() {
        @Override
        public TokenOtherDataModel createFromParcel(Parcel in) {
            return new TokenOtherDataModel(in);
        }

        @Override
        public TokenOtherDataModel[] newArray(int size) {
            return new TokenOtherDataModel[size];
        }
    };

    private String paymentToken;
    private double orderAmount;
    private String orderId;
    private String appId;
    private String vendorId;
    private UserModel customer;
    private String cftoken;
    private String paymentMode;
    private String approvalRefNo;
    private String txnId;
    private String status;
    private String txnRef;

    protected TokenOtherDataModel(Parcel in) {
        paymentToken = in.readString();
        orderAmount = in.readDouble();
        orderId = in.readString();
        appId = in.readString();
        vendorId = in.readString();
//        customer = in.readParcelable(UserModel.class.getClassLoader());
        cftoken = in.readString();
        paymentMode = in.readString();
        approvalRefNo = in.readString();
        txnId = in.readString();
        status = in.readString();
        txnRef = in.readString();
    }

    public static Map<String, String> getParamsMap(TokenOtherDataModel tokenData) {
        Map<String, String> paramsMap = new LinkedHashMap<>();
        paramsMap.put(JSONConstants.APP_ID, tokenData.getAppId());
        paramsMap.put(JSONConstants.ORDER_ID, tokenData.getOrderId());
        paramsMap.put(JSONConstants.ORDER_AMT, String.valueOf(tokenData.getOrderAmount()));
//        paramsMap.put(JSONConstants.CUST_NAME, tokenData.getCustomer().getUserName());
//        paramsMap.put(JSONConstants.CUST_MOBILE, tokenData.getCustomer().getUserMobileNumber());
//        paramsMap.put(JSONConstants.CUST_EMAIL, tokenData.getCustomer().getUserEmail());
//        paramsMap.put(PAYMENT_MODES, "cc,dc,nb");
        paramsMap.put(JSONConstants.PAYMENT_MODES, tokenData.getPaymentMode());

        printMap(paramsMap);
        return paramsMap;
    }

    public static final String GPAY_SUCCESS = "SUCCESS";
    public static final String GPAY_SUBMITTED = "SUBMITTED";
    public static final String GPAY_TXN_ID = "txnId";
    public static final String APPROVAL_REFNO = "approvalRefNo";
    public static final String GPAY_TXN_REF = "txnRef";
    public static final String GPAY_STATUS = "Status";

    public static HashMap<String, String> getParamsMapForGpay(Intent data, double amt) {
        String mobile = SharedPreferenceManager.INSTANCE.getString(SharedPreferenceManager.INSTANCE.getKEY_MOBILE_NUMBER());
        HashMap<String, String> paramsMap = new HashMap<>();
        paramsMap.put(JSONConstants.ORDER_ID, data.getStringExtra(GPAY_TXN_REF));
        paramsMap.put(JSONConstants.ORDER_AMT, String.valueOf(amt));
        paramsMap.put(JSONConstants.REF_ID, data.getStringExtra(GPAY_TXN_ID));
        paramsMap.put(JSONConstants.PAYMENT_MODE, "gpay");
        paramsMap.put(JSONConstants.TX_STATUS, data.getStringExtra(GPAY_STATUS));

        paramsMap.put(APPROVAL_REFNO, data.getStringExtra(APPROVAL_REFNO));
        paramsMap.put(JSONConstants.CUST_MOBILE, mobile);
        paramsMap.put(JSONConstants.CUST_EMAIL, "");

        printMap(paramsMap);
        return paramsMap;
    }

    public static void printMap(Map<String, String> paramsMap) {
        for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
//            Logger.dlog("YOW! PAYMENT GATEWAY params: " + entry.getKey() + "/" + entry.getValue());
        }
    }

    public String getPaymentMode() {
        return paymentMode;
    }


    public String getCfToken() {
        return cftoken;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getAppId() {
        return appId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public UserModel getCustomer() {
        return customer;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(paymentToken);
        dest.writeDouble(orderAmount);
        dest.writeString(orderId);
        dest.writeString(appId);
        dest.writeString(vendorId);
        dest.writeParcelable(customer, flags);
        dest.writeString(cftoken);
        dest.writeString(paymentMode);
        dest.writeString(approvalRefNo);
        dest.writeString(txnId);
        dest.writeString(status);
        dest.writeString(txnRef);
    }
}
