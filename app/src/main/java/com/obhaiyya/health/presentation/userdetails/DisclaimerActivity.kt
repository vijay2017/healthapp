package com.obhaiyya.health.presentation.userdetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.navigation.ProfileHamburgerActivity
import kotlinx.android.synthetic.main.activity_disclaimer.*

class DisclaimerActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener,
    View.OnClickListener {

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, DisclaimerActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disclaimer)
        chkDisclaimer.setOnCheckedChangeListener(this)
        btnDisclaimer.setOnClickListener(this)

    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView?.id) {
            R.id.chkDisclaimer -> {
                btnDisclaimer.isEnabled = isChecked
                btnDisclaimer.setBackgroundResource(R.drawable.draw_facebook);


            }

        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnDisclaimer -> {
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
    }

}