package com.obhaiyya.health.presentation.scanner

import ai.binah.hrv.HealthMonitorManager
import ai.binah.hrv.HealthMonitorManager.HealthMonitorManagerListener
import ai.binah.hrv.api.*
import ai.binah.hrv.session.api.HealthMonitorSession
import ai.binah.hrv.session.api.HealthMonitorSession.SessionState
import ai.binah.hrv.session.api.HealthMonitorSession.StateListener
import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.obhaiyya.health.R
import com.obhaiyya.health.databinding.ActivityCameraAiBinding
import com.obhaiyya.health.presentation.scanner.scnner_new_ai.ScannerHealthMonitor
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*

class CameraAIActivity : AppCompatActivity(), HealthMonitorManagerListener,
    StateListener, HealthMonitorDeviceEvaluationSessionListener {

    private enum class UiState {
        LOADING, MEASURING, IDLE
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityCameraAiBinding.inflate(
            layoutInflater
        )
        setContentView(mBinding?.root)
        initializeUi()
        checkCameraPermission()

    }

    private fun checkCameraPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                1
            )
        } else {
            createHealthMonitorManager()
            createSession()
        }
    }

    public override fun onResume() {
        super.onResume()
        createSession()
    }

    public override fun onPause() {
        super.onPause()
        closeSession()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            createHealthMonitorManager()
            createSession()
        }
    }

    override fun onHealthManagerReady() {
        //Deprecated
    }

    override fun onHealthManagerError(errorCode: Int, messageCode: Int) {
        showMessageDialog("Invalid License ($messageCode)")
        updateUi(UiState.IDLE)
    }

    override fun onSessionStateChange(
        session: HealthMonitorSession,
        state: SessionState
    ) {
        //Deprecated
    }


    override fun onNewImage(bitmap: Bitmap, fingerDetected: Boolean) {
        runOnUiThread {
            handleImage(bitmap)
            handleRoiDetection(fingerDetected)
        }
    }

    override fun onDeviceEvaluationResult(enabledVitalSigns: HealthMonitorEnabledVitalSigns) {
        val supportedVitals: MutableList<String> =
            ArrayList()
        if (enabledVitalSigns.isHeartRateEnabled) {
            supportedVitals.add(getString(R.string.heart_rate))
        }
        if (enabledVitalSigns.isOxygenSaturationEnabled) {
            supportedVitals.add(getString(R.string.saturation))
        }
        if (enabledVitalSigns.isBreathingRateEnabled) {
            supportedVitals.add(getString(R.string.respiration))
        }
        if (enabledVitalSigns.isSdnnEnabled) {
            supportedVitals.add(getString(R.string.hrv_sdnn))
        }
        if (enabledVitalSigns.isStressLevelEnabled) {
            supportedVitals.add(getString(R.string.stress))
        }
        val message: String
        message = if (supportedVitals.isEmpty()) {
            getString(R.string.no_vital_signs_supported)
        } else {
            String.format(
                getString(R.string.enabled_vital_signs),
                java.lang.String.join(", ", supportedVitals)
            )
        }
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(R.string.continue_) { dialog, which -> finish() }
            .setNegativeButton(R.string.try_again, null)
            .show()
    }

    override fun onStateChange(
        session: HealthMonitorSession,
        state: SessionState
    ) {
        runOnUiThread {
            if (state == SessionState.ACTIVE) {
                updateUi(UiState.IDLE)
            }
        }
    }

    fun startStopButtonClicked(view: View?) {
        if (mSession == null) {
            return
        }
        if (mSession?.state == SessionState.MEASURING) {
            stopMeasuring()
        } else {
            startMeasuring()
            mBinding?.mainContentCamera?.playStopButton?.visibility = View.GONE
        }
    }

    private fun createHealthMonitorManager() {
        try {
            updateUi(UiState.LOADING)
            val licenseKey = getSharedPreferences(
                "binah_sample",
                Context.MODE_PRIVATE
            ).getString("licenseKey", "")
            mManager = HealthMonitorManager(this, LicenseData(licenseKey ?: ""), this)
        } catch (e: HealthMonitorException) {
            showMessageDialog("Error: (" + e.errorCode + ")")
        }
    }

    private fun createSession() {
        if (mManager == null || mSession != null) {
            return
        }
        updateUi(UiState.LOADING)
        try {
            mSession = mManager?.createFingerEvaluationSessionBuilder(baseContext)
                ?.withListener(this@CameraAIActivity)
                ?.withSessionStateListener(this@CameraAIActivity)
                ?.build()
        } catch (e: ExceptionInInitializerError) {
            showWarning("Error creating session - $e")
        }
    }

    private fun closeSession() {
        stopTimeCount()
        if (mSession != null) {
            mSession?.terminate()
            mSession?.removeStateListener(this)
        }
        mSession = null
    }

    private fun startMeasuring() {
        try {
            mSession?.start()
            updateUi(UiState.MEASURING)
        } catch (e: HealthMonitorException) {
            handleStartSessionException(e)
        } catch (e: IllegalStateException) {
            showWarning("Start Error - Session in illegal state")
        } catch (e: NullPointerException) {
            showWarning("Start Error - Session not initialized")
        }
    }

    private fun handleStartSessionException(e: HealthMonitorException) {
        when (e.errorCode) {
            HealthMonitorCodes.DEVICE_CODE_MINIMUM_BATTERY_LEVEL_WARNING -> showMessageDialog(
                getString(R.string.low_battery_error)
            )
            HealthMonitorCodes.DEVICE_CODE_LOW_POWER_MODE_ENABLED_ERROR -> showMessageDialog(
                getString(R.string.power_save_error)
            )
            else -> showMessageDialog(getString(R.string.cannot_start_session))
        }
    }

    private fun stopMeasuring() {
        try {
            mSession?.stop()
        } catch (e: IllegalStateException) {
            showWarning("Stop Error - Session in illegal state")
        } catch (ignore: NullPointerException) {
        }
    }

    private fun initializeUi() {
        resetTimeCount()
        mBinding?.backButton?.setOnClickListener { finish() }
    }

    private fun updateUi(state: UiState) {
        when (state) {
            UiState.LOADING -> {
                mBinding?.mainContentCamera?.cameraView?.visibility = View.INVISIBLE
                mBinding?.loadingText?.visibility = View.VISIBLE
                mBinding?.mainContentCamera?.roiWarning?.visibility = View.INVISIBLE
                mBinding?.mainContentCamera?.keepFingerOnCamera?.visibility = View.INVISIBLE
                stopTimeCount()
                resetTimeCount()
            }
            UiState.MEASURING -> {
                mBinding?.loadingText?.visibility = View.INVISIBLE
                startTimeCount()
//                mBinding?.mainContentCamera?.playStopButton?.setBackgroundResource(R.drawable.ic_stop_button)
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                mBinding?.mainContentCamera?.ppgFingerCheck?.visibility = View.INVISIBLE
                mBinding?.mainContentCamera?.keepFingerOnCamera?.visibility = View.VISIBLE
            }
            UiState.IDLE -> {
                mBinding?.loadingText?.visibility = View.INVISIBLE
                if (mBinding?.mainContentCamera?.cameraView?.visibility == View.INVISIBLE) {
                    mBinding?.mainContentCamera?.cameraView?.visibility = View.VISIBLE
                    clearCanvas()
                }
                mBinding?.mainContentCamera?.playStopButton?.isEnabled = true
//                mBinding?.mainContentCamera?.playStopButton?.setBackgroundResource(R.drawable.ic_play_scan)
                mBinding?.mainContentCamera?.roiWarning?.visibility = View.INVISIBLE
                mBinding?.mainContentCamera?.ppgFingerCheck?.visibility = View.VISIBLE
                mBinding?.mainContentCamera?.keepFingerOnCamera?.visibility = View.INVISIBLE
                stopTimeCount()
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }
    }

    private fun startTimeCount() {
        if (mTimeCountHandler != null) {
            mTimeCountHandler?.removeCallbacksAndMessages(null)
        }
        mTime = 0
        mTimeCountHandler = Handler()
        mTimeCountHandler?.post(object : Runnable {
            override fun run() {
                mBinding?.mainContentCamera?.timeTextView?.text = String.format(
                    getString(R.string.seconds_count),
                    formatTimer(mTime++.toLong())
                )
               /* mBinding?.progressView1?.setOnProgressChangeListener {
                    progressView1.labelText = "achieve ${it.toInt()}%"
                }*/
                mTimeCountHandler?.postDelayed(this, 1000)
            }
        })
    }

    private fun stopTimeCount() {
        if (mTimeCountHandler != null) {
            mTimeCountHandler?.removeCallbacksAndMessages(null)
        }
    }

    private fun resetTimeCount() {
        mTime = 0
        mBinding?.mainContentCamera?.timeTextView?.text = String.format(
            getString(R.string.seconds_count),
            formatTimer(mTime.toLong())
        )
    }

    private fun handleRoiDetection(detected: Boolean) {
        if (mSession == null || mSession?.state != SessionState.MEASURING) {
            return
        }
        if (detected) {
            mBinding?.mainContentCamera?.roiWarning?.visibility = View.INVISIBLE
            mBinding?.mainContentCamera?.keepFingerOnCamera?.visibility = View.VISIBLE
        } else {
            val roiWarning = findViewById<TextView>(R.id.roiWarningText)
            mBinding?.mainContentCamera?.roiWarning?.visibility = View.VISIBLE
            mBinding?.mainContentCamera?.keepFingerOnCamera?.visibility = View.INVISIBLE
            roiWarning.text = getString(R.string.no_finger_detected)
        }
    }

    private fun showMessageDialog(message: String) {
        if (mMessageDialog != null && mMessageDialog?.isShowing!!) {
            return
        }
        mMessageDialog = AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(R.string.ok, null)
            .show()
    }

    private fun showWarning(text: String) {
        val warningLayout = findViewById<LinearLayout>(R.id.warningLayout)
        if (warningLayout.visibility == View.VISIBLE) {
            return
        }
        if (mWarningDialogTimeoutHandler != null) {
            mWarningDialogTimeoutHandler?.removeCallbacksAndMessages(null)
        }
        val warning = warningLayout.findViewById<TextView>(R.id.warningMessage)
        warning.text = text
        warningLayout.visibility = View.VISIBLE
        mWarningDialogTimeoutHandler = Handler(mainLooper)
        mWarningDialogTimeoutHandler?.postDelayed(
            { warningLayout.visibility = View.INVISIBLE },
            5000
        )
    }

    private fun formatTimer(seconds: Long): String {
        return String.format(Locale.US, "%02d", seconds / 60 % 60) +
                ":" + String.format(Locale.US, "%02d", seconds % 60)
    }

    private fun handleImage(bitmap: Bitmap?) {
        if (bitmap == null) {
            return
        }
        val canvas = mBinding?.mainContentCamera?.cameraView?.lockCanvas()
        if (canvas != null) {
            mBinding?.mainContentCamera?.cameraView?.let {
                canvas.drawBitmap(
                    bitmap,
                    null,
                    Rect(
                        0,
                        0,
                        it.width,
                        it.bottom - it.top
                    ),
                    null
                )
            }
            mBinding?.mainContentCamera?.cameraView?.unlockCanvasAndPost(canvas)
        }
    }


    private var mBinding: ActivityCameraAiBinding? = null
    private var mManager: HealthMonitorManager? = null
    private var mSession: HealthMonitorSession? = null
    private var mTime = 0
    private var mTimeCountHandler: Handler? = null
    private var mWarningDialogTimeoutHandler: Handler? = null
    private var mMessageDialog: AlertDialog? = null
    var mScanner: ScannerHealthMonitor? = null

    private fun clearCanvas() {
        val canvas = mBinding?.mainContentCamera?.cameraView?.lockCanvas()
        canvas?.drawColor(ContextCompat.getColor(baseContext, R.color.white))
        canvas?.let { mBinding?.mainContentCamera?.cameraView?.unlockCanvasAndPost(it) }
    }
}