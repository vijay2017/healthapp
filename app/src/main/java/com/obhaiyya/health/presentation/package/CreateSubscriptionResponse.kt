package com.obhaiyya.health.presentation.Package

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CreatePackageResponse : Serializable {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("userId")
    @Expose
    var userId: String? = null

    @SerializedName("amount")
    @Expose
    var amount: Int? = null

    @SerializedName("startDate")
    @Expose
    var startDate: String? = null

    @SerializedName("endDate")
    @Expose
    var endDate: String? = null

    @SerializedName("paymentDoneDate")
    @Expose
    var paymentDoneDate: String? = null

    @SerializedName("paymentGatewayResponses")
    @Expose
    var paymentGatewayResponses: PaymentGatewayResponses? = null

    @SerializedName("planDetails")
    @Expose
    var planDetails: PlanDetails? = null

    @SerializedName("userDetails")
    @Expose
    var userDetails: UserDetails? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("__v")
    @Expose
    var v: Int? = null

}

class PaymentGatewayResponses {
    @SerializedName("orderAmount")
    @Expose
    var orderAmount: Int? = null

    @SerializedName("orderId")
    @Expose
    var orderId: String? = null

    @SerializedName("orderCurrency")
    @Expose
    var orderCurrency: String? = null

    @SerializedName("vendorId")
    @Expose
    var vendorId: String? = null

    @SerializedName("lastUpdatedTime")
    @Expose
    var lastUpdatedTime: String? = null

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("cftoken")
    @Expose
    var cftoken: String? = null
}

class PlanDetails {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("amount")
    @Expose
    var amount: Int? = null

    @SerializedName("validity")
    @Expose
    var validity: Int? = null

    @SerializedName("planName")
    @Expose
    var planName: String? = null

}

class UserDetails {
    @SerializedName("locations")
    @Expose
    var locations: List<Any>? = null

    @SerializedName("products")
    @Expose
    var products: List<Any>? = null

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("authToken")
    @Expose
    var authToken: String? = null

    @SerializedName("gmailToken")
    @Expose
    var gmailToken: String? = null

    @SerializedName("appartment")
    @Expose
    var appartment: String? = null

    @SerializedName("block")
    @Expose
    var block: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    var dateOfBirth: String? = null

    @SerializedName("flatNumber")
    @Expose
    var flatNumber: String? = null

    @SerializedName("gender")
    @Expose
    var gender: String? = null

    @SerializedName("height")
    @Expose
    var height: Double? = null

    @SerializedName("mobileNumber")
    @Expose
    var mobileNumber: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("notificationToken")
    @Expose
    var notificationToken: String? = null

    @SerializedName("weight")
    @Expose
    var weight: Double? = null

    @SerializedName("active")
    @Expose
    var active: Boolean? = null


}