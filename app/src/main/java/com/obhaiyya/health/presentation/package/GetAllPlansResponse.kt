package com.obhaiyya.health.presentation.Package

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class GetAllPlansResponse : Serializable {

    @SerializedName("_id")
    @Expose
    val id: String? = null

    @SerializedName("amount")
    @Expose
    val amount: Int? = null

    @SerializedName("validity")
    @Expose
    val validity: Int? = null

    @SerializedName("planName")
    @Expose
    val planName: String? = null

    var success: Boolean? = null

}