package com.obhaiyya.health.presentation.Package

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.DialogFragment
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.obhaiyya.health.HealthApplication
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.CustomCashFreeCB
import com.obhaiyya.health.presentation.base.BaseBottomSheet
import com.obhaiyya.health.presentation.paygateway.*
import com.obhaiyya.health.presentation.paygateway.TokenOtherDataModel.GPAY_STATUS
import com.obhaiyya.health.presentation.paygateway.TokenOtherDataModel.GPAY_SUCCESS
import kotlinx.android.synthetic.main.bottom_sheet_payment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentBottomSheet : BaseBottomSheet(), View.OnClickListener, GpayMakerCallback, IGooglePay,
        CustomCashFreeCB.OnCustomCashFreeCallback {

    var mIPaymentBottomHandler: IPaymentBottomSheethandler? = null
    var gpayMaker: GooglePayMaker? = null
    var mAmountSelected = 0.0

    companion object {

        val KEY_AMOUNT_SELECTED = "keyAmountSelected"

        fun newInstance(amount: Double): PaymentBottomSheet {
            val paymentBottomSheet = PaymentBottomSheet()
            val bundle = Bundle()
            bundle.putDouble(KEY_AMOUNT_SELECTED, amount)
            paymentBottomSheet.arguments = bundle
            return paymentBottomSheet
        }

    }

    override fun getLayout(): Int {
        return R.layout.bottom_sheet_payment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.BottomSheetDialog)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnPay.setOnClickListener(this)
        mAmountSelected = arguments?.getDouble(KEY_AMOUNT_SELECTED) as Double

        btnPay?.text = "Pay ₹ " + mAmountSelected

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnPay -> {
//                mIPaymentBottomHandler?.onPayment()
                /*if (com.obhaiyya.health.BuildConfig.DEBUG) {
                    mIPaymentBottomHandler?.onPayment()
                } else {
                    *//*activity?.let {
                        CustomCashFreeCB.getAppIdAndMakePayment(it, "1", this, mHealthService)
                    }*//*
                    onClickPay()
                }*/

                onClickPay()

            }
        }
    }


    private fun onClickPay() {
        gpayMaker = GooglePayMaker(this, this)
        gpayMaker?.getTxRefIdAndOpenGpayApp("Health Package", 1.0)
//            gpayMaker?.getTxRefIdAndOpenGpayApp("Health Package", mAmountSelected)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GooglePayMaker.TEZ_REQUEST_CODE) {
            if (null != data) {
                val status = data.getStringExtra(GPAY_STATUS)
                val paramsMap =
                        gpayMaker?.paymentAmt?.let {
                            TokenOtherDataModel.getParamsMapForGpay(data, it)
                        }
                if (GPAY_SUCCESS.equals(status, ignoreCase = true)) {
                    paymentSuccess(paramsMap)
                } else if ("SUBMITTED".equals(status, ignoreCase = true)) {
                    HealthApplication.showToast("Gpay Submitted")
                } else {
                    HealthApplication.showToast("Gpay Failed")
                }
            } else {

            }
        }
    }

    override fun onTxRefIdFetched(txId: String?) {

    }

    override fun onErrorFetchingTxRefId() {

    }

    override fun onGooglePayCallback(data: Intent) {
        val status = data.getStringExtra(GPAY_STATUS)
        HealthApplication.showToast("result gpay status$status")
        val paramsMap = TokenOtherDataModel.getParamsMapForGpay(data, gpayMaker?.paymentAmt ?: 0.0)
        if (GPAY_SUCCESS.equals(status, ignoreCase = true)) {
            paymentSuccess(paramsMap)
        }
    }

    private fun paymentSuccess(paramsMap: HashMap<String, String>?) {
        TokenOtherDataModel.printMap(paramsMap)
        paramsMap?.let {
            notifyPaymentGatewaySuccess(it)
        }
    }

    private fun notifyPaymentGatewaySuccess(map: Map<String, String>) {
        CloserByRestClient().notifyBackendPaymentSuccess(
                map,
                paymentGatewaySuccessListener(map)
        )
    }

    private fun paymentGatewaySuccessListener(map: Map<String, String>): Callback<PaymentGatewayResponse?>? {
        return object : Callback<PaymentGatewayResponse?> {
            override fun onResponse(
                    call: Call<PaymentGatewayResponse?>,
                    response: Response<PaymentGatewayResponse?>
            ) {
                if (response.isSuccessful) {
                    val res = response.body()
                    mIPaymentBottomHandler?.onPaymentSuccess(res)
                } else {
                    HealthApplication.showToast("Payment Failed")
                    showToastMessage("Payment failed")
                }
            }

            override fun onFailure(call: Call<PaymentGatewayResponse?>, t: Throwable) {
                Log.d("VIJAY", "onFailure")
                showToastMessage("Failure")
            }
        }
    }

    override fun onNoNetworkFoundForGpay(call: Call<*>?, t: Throwable?) {

    }

    interface IPaymentBottomSheethandler {
        fun onPaymentSuccess(res: PaymentGatewayResponse?)
        fun onPayment()
    }

    override fun onSuccess(value: MutableMap<String, String>?) {
        showToastMessage("Success")
        dismiss()
    }

    override fun onFailure() {
        showToastMessage("Failure")
    }

    override fun onNavigateBackWithoutPayment() {
        showToastMessage("Back Pressed")
    }

}


class PaymentGatewayResponse {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("lastUpdatedTime")
    @Expose
    var lastUpdatedTime: String? = null

    @SerializedName("orderAmount")
    @Expose
    var orderAmount: Int? = null

    @SerializedName("orderCurrency")
    @Expose
    var orderCurrency: String? = null

    @SerializedName("orderId")
    @Expose
    var orderId: String? = null

    @SerializedName("vendorId")
    @Expose
    var vendorId: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null
}
