package com.obhaiyya.health.presentation.userdetails

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class UserRegistrationGmailResponse : Serializable {

    @SerializedName("_id")
    @Expose
    var _id: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("authToken")
    @Expose
    var authToken: String? = null

    @SerializedName("gmailToken")
    @Expose
    var gmailToken: String? = null

    @SerializedName("appartment")
    @Expose
    var appartment: String? = null

    @SerializedName("block")
    @Expose
    var block: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    var dateOfBirth: String? = null

    @SerializedName("flatNumber")
    @Expose
    var flatNumber: String? = null

    @SerializedName("gender")
    @Expose
    var gender: String? = null

    @SerializedName("height")
    @Expose
    var height: Double? = null

    @SerializedName("lat")
    @Expose
    var lat: Double? = null

    @SerializedName("lng")
    @Expose
    var lng: Double? = null

    @SerializedName("mobileNumber")
    @Expose
    var mobileNumber: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("notificationToken")
    @Expose
    var notificationToken: String? = null

    @SerializedName("weight")
    @Expose
    var weight: Double? = null

    @SerializedName("mobileOTP")
    @Expose
    var mobileOTP: String? = null

    @SerializedName("accessCode")
    @Expose
    var accessCode: String? = null

    @SerializedName("active")
    @Expose
    var active: Boolean? = null

    @SerializedName("consumedAccessCodeDate")
    @Expose
    var consumedAccessCodeDate: String? = null

}