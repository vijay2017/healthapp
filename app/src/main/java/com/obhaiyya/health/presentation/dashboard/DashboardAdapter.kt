package com.obhaiyya.health.presentation.dashboard

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import kotlinx.android.synthetic.main.fragment_dashboard.*

class DashboardAdapter(
    val mContext: Context,
    var iDayClicked: IDayClicked,
    var listGetDashboardHealth: List<TodayDashboardResponse>
) : RecyclerView.Adapter<DashboardAdapter.DashboaordViewHolder>() {

    var avgMortalityRisk = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboaordViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.adapter_dashboard_new, parent, false)
        return DashboaordViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashboaordViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return listGetDashboardHealth.size ?: 0
    }

    inner class DashboaordViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val txtHeartRate = view.findViewById<TextView>(R.id.txtHeartRate)
        val txtHRV = view.findViewById<TextView>(R.id.txtHRV)
        val txtSpo2 = view.findViewById<TextView>(R.id.txtSpo2)
        val txtRespiration = view.findViewById<TextView>(R.id.txtRR)
        val txtStress = view.findViewById<TextView>(R.id.txtStress)
        val txtDateDashboard = view.findViewById<TextView>(R.id.txtDateDashboard)
        val txtIsRestOrWorkOut = view.findViewById<TextView>(R.id.txtIsRestOrWorkOut)
        val peractivity = view.findViewById<View>(R.id.peractivity)
        val postactivity = view.findViewById<View>(R.id.postactivity)

        //        var restbg = view.findViewById<LinearLayout>(R.id.restbg)
        val txtMortalityRisk = view.findViewById<TextView>(R.id.txtMortalityRisk)
        val txtMortalityRisknew = view.findViewById<TextView>(R.id.txtMortalityRisknew)
        val txtMortalityRiskStatus = view.findViewById<TextView>(R.id.txtMortalityRiskStatus)
        val imgShare = view.findViewById<ImageView>(R.id.imgShare)

        init {

            imgShare.setOnClickListener {
                iDayClicked.onShareClicked(listGetDashboardHealth.get(adapterPosition))
            }

        }


        fun bind(position: Int) {
            txtHeartRate.text = listGetDashboardHealth.get(position).heartRate ?: ""
            txtHRV.text = listGetDashboardHealth.get(position).hrv ?: ""
            txtRespiration.text = listGetDashboardHealth.get(position).respiration ?: ""
            txtSpo2.text = listGetDashboardHealth.get(position).spo2 ?: ""
            txtStress.text = listGetDashboardHealth.get(position).stress ?: ""
            txtDateDashboard.text = DateFormatUtils.convertInDateTime(
                listGetDashboardHealth.get(position)?.createdAt
                    ?: ""
            )
            val mortalityRisk = listGetDashboardHealth.get(position).mortalityRisk ?: 0
            txtMortalityRisk.text = mortalityRisk.toString()
            avgMortalityRisk = avgMortalityRisk + mortalityRisk

            if (listGetDashboardHealth.get(position).isRest == true) {
                txtIsRestOrWorkOut.text = "Resting Heart : "
                peractivity.visibility = View.VISIBLE
                postactivity.visibility = View.GONE
//                restbg.setBackgroundResource(R.drawable.outline_workout)
//                restbg.setPadding(50,50,50,50)
            } else {
                txtIsRestOrWorkOut.text = "Post activity : "
                peractivity.visibility = View.GONE
                postactivity.visibility = View.VISIBLE
//                restbg.setBackgroundResource(R.drawable.outline_resting)
//                restbg.setPadding(50,50,50,50)
            }
            if (listGetDashboardHealth.size - 1 == position) {
                iDayClicked.onDayClicked(avgMortalityRisk, listGetDashboardHealth.size)
            }
            checkHealthStatus(mortalityRisk)

        }

        private fun checkHealthStatus(mortalityRsik: Int) {
            when (mortalityRsik) {
//                123 (good) 456 (avrage) 78910 (poor)

                0 -> {
                    txtMortalityRiskStatus.text = "Good"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#FFFFFFFF"))
                    txtMortalityRisknew.text = "10"

                }

                1 -> {
                    txtMortalityRiskStatus.text = "Good"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#FFFFFFFF"))
                    txtMortalityRisknew.text = "10"
                }
                2 -> {
                    txtMortalityRiskStatus.text = "Good"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#FFFFFFFF"))
                    txtMortalityRisknew.text = "09"
                }
                3 -> {
                    txtMortalityRiskStatus.text = "Good"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#FFFFFFFF"))
                    txtMortalityRisknew.text = "08"
                }
                4 -> {
                    txtMortalityRiskStatus.text = "Average"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round_average);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#3E5481"))
                    txtMortalityRisknew.text = "07"
                }
                5 -> {
                    txtMortalityRiskStatus.text = "Average"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round_average);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#3E5481"))
                    txtMortalityRisknew.text = "06"
                }
                6 -> {
                    txtMortalityRiskStatus.text = "Average"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round_average);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#3E5481"))
                    txtMortalityRisknew.text = "05"
                }
                7 -> {
                    txtMortalityRiskStatus.text = "Poor"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round_poor);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#3E5481"))
                    txtMortalityRisknew.text = "04"
                }
                8 -> {
                    txtMortalityRiskStatus.text = "Poor"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round_poor);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#3E5481"))
                    txtMortalityRisknew.text = "03"

                }
                9 -> {
                    txtMortalityRiskStatus.text = "Poor"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round_poor);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#3E5481"))
                    txtMortalityRisknew.text = "02"
                }
                10 -> {
                    txtMortalityRiskStatus.text = "Poor"
                    txtMortalityRisknew.setBackgroundResource(R.drawable.round_poor);
                    txtMortalityRisknew.setTextColor(Color.parseColor("#3E5481"))
                    txtMortalityRisknew.text = "01"
                }
//                1, 2, 3 -> {
//                    txtMortalityRiskStatus.text = "Good"
//                    txtMortalityRisk.setBackgroundResource(R.drawable.round);
//                    txtMortalityRisk.setTextColor(Color.parseColor("#FFFFFFFF"))
//                }
//                4, 5, 6 -> {
//                    txtMortalityRiskStatus.text = "Average"
//                    txtMortalityRisk.setBackgroundResource(R.drawable.round_average);
//                    txtMortalityRisk.setTextColor(Color.parseColor("#3E5481"))
//                }
//                7, 8, 9, 10 -> {
//                    txtMortalityRiskStatus.text = "Poor"
//                    txtMortalityRisk.setBackgroundResource(R.drawable.round_poor);
//                    txtMortalityRisk.setTextColor(Color.parseColor("#3E5481"))
//                }
            }
        }
    }

}

interface IDayClicked {
    fun onDayClicked(mortalityRisk: Int, totalSize: Int)
    fun onShareClicked(data: TodayDashboardResponse)
}