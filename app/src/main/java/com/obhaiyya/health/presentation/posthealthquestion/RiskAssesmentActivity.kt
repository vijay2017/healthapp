package com.obhaiyya.health.presentation.posthealthquestion

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioGroup
import com.obhaiyya.health.HealthApplication
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.scanner.HealthDataRequest
import com.obhaiyya.health.presentation.scanner.QuestionListRequest
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.btnSubmitRisk
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.chkboxCough
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.chkboxDiarhea
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.chkboxMuscleAche
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.chkboxNoSymtoms
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.chkboxRunnyNose
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.chkboxSoreThroat
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.chkboxVomiting
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnFirstAgree
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnFirstDisagree
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnFourthNo
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnFourthYes
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnSecondNo
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnSecondYes
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnThirdNo
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdBtnThirdYes
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdGrpFifth
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdGrpFirst
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdGrpFourth
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdGrpSecond
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.rdGrpThird
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.txtFifthQuestion
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.txtFirstQuestion
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.txtFourthQuestion
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.txtSecondQuestion
import kotlinx.android.synthetic.main.bottom_sheet_risk_assessment.txtThirdQuestion

class RiskAssesmentActivity : BaseActivity(), View.OnClickListener,
    RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {

    var lossOfTaste = false
    var mAmountSelected = 0.0
    var mHealthDataRequest: HealthDataRequest? = null
    var mQuestionList = ArrayList<QuestionListRequest>()
    var mQuestionReq: QuestionListRequest? = null
    var mHashMapQuestion = HashMap<Int, QuestionListRequest?>()

    companion object {

        val KEY_HEALTH_REQUEST = "keyHealthRequest"

        fun getCallingIntent(context: Context, healthDataRequest: HealthDataRequest): Intent {
            val intent = Intent(context, RiskAssesmentActivity::class.java)
            intent.putExtra(KEY_HEALTH_REQUEST, healthDataRequest)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bottom_sheet_risk_assessment)
        setListener()

        val bundle = intent.extras
        bundle?.let {
            mHealthDataRequest = it.getSerializable(KEY_HEALTH_REQUEST) as HealthDataRequest
        }

    }

    fun setListener() {
        btnSubmitRisk.setOnClickListener(this)
        rdGrpFirst.setOnCheckedChangeListener(this)
        rdGrpSecond.setOnCheckedChangeListener(this)
        rdGrpThird.setOnCheckedChangeListener(this)
        rdGrpFourth.setOnCheckedChangeListener(this)
        rdGrpFifth.setOnCheckedChangeListener(this)

        chkboxCough.setOnCheckedChangeListener(this)
        chkboxDiarhea.setOnCheckedChangeListener(this)
        chkboxMuscleAche.setOnCheckedChangeListener(this)
        chkboxNoSymtoms.setOnCheckedChangeListener(this)
        chkboxRunnyNose.setOnCheckedChangeListener(this)
        chkboxSoreThroat.setOnCheckedChangeListener(this)
        chkboxVomiting.setOnCheckedChangeListener(this)

    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSubmitRisk -> {
                saveHealthDataApi()
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtnFirstAgree -> {
                lossOfTaste = true
                saveAnswers(0, "Agree", txtFirstQuestion.text.toString())
                checkAllAnswered()
            }
            R.id.rdBtnFirstDisagree -> {
                lossOfTaste = false
                checkAllAnswered()
                saveAnswers(0, "Disagree", txtFirstQuestion.text.toString())
            }
            R.id.rdBtnSecondYes -> {
                lossOfTaste = false
                saveAnswers(1, "Yes", txtSecondQuestion.text.toString())
                checkAllAnswered()
            }
            R.id.rdBtnSecondNo -> {
                saveAnswers(1, "No", txtSecondQuestion.text.toString())
                lossOfTaste = false
                checkAllAnswered()
            }
            R.id.rdBtnThirdYes -> {
                lossOfTaste = false
                checkAllAnswered()
                saveAnswers(2, "Yes", txtThirdQuestion.text.toString())
            }
            R.id.rdBtnThirdNo -> {
                lossOfTaste = false
                checkAllAnswered()
                saveAnswers(2, "No", txtThirdQuestion.text.toString())
            }
            R.id.rdBtnFourthYes -> {
                lossOfTaste = false
                checkAllAnswered()
                saveAnswers(3, "No", txtFourthQuestion.text.toString())
            }
            R.id.rdBtnFourthNo -> {
                lossOfTaste = false
                checkAllAnswered()
                saveAnswers(3, "No", txtFourthQuestion.text.toString())
            }
            R.id.rdBtnFifthYes -> {
                lossOfTaste = false
                checkAllAnswered()
                saveAnswers(4, "Below 104", txtFifthQuestion.text.toString())
            }
            R.id.rdBtnFifthNo -> {
                lossOfTaste = false
                checkAllAnswered()
                saveAnswers(4, "Above 104", txtFifthQuestion.text.toString())
            }
        }
    }


    fun checkAllAnswered(): Boolean {
        val answerFirst = rdBtnFirstAgree.isChecked || rdBtnFirstDisagree.isChecked
        val answerSecond = rdBtnSecondYes.isChecked || rdBtnSecondNo.isChecked
        val answerThird = rdBtnThirdYes.isChecked || rdBtnThirdNo.isChecked
        val answerFourth = rdBtnFourthYes.isChecked || rdBtnFourthNo.isChecked

        return answerFirst && answerSecond && answerThird && answerFourth
    }

    fun checkCheckBox(): Boolean {
        val answerCough = chkboxNoSymtoms.isChecked ||
                (chkboxCough.isChecked || chkboxDiarhea.isChecked || chkboxMuscleAche.isChecked
                        || chkboxRunnyNose.isChecked || chkboxSoreThroat.isChecked
                        || chkboxVomiting.isChecked)

        return answerCough
    }


    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        when (buttonView?.id) {
            R.id.chkboxCough -> {
                saveAnswers(5, "" + isChecked, "Cough")
                chkboxNoSymtoms.isChecked = false
            }
            R.id.chkboxDiarhea -> {
                saveAnswers(6, "" + isChecked, "Diarhea")
                chkboxNoSymtoms.isChecked = false
            }
            R.id.chkboxMuscleAche -> {
                saveAnswers(7, "" + isChecked, "MuscleAche")
                chkboxNoSymtoms.isChecked = false
            }
            R.id.chkboxRunnyNose -> {
                saveAnswers(8, "" + isChecked, "RunnyNose")
                chkboxNoSymtoms.isChecked = false
            }
            R.id.chkboxSoreThroat -> {
                saveAnswers(9, "" + isChecked, "SoreThroat")
                chkboxNoSymtoms.isChecked = false
            }
            R.id.chkboxVomiting -> {
                saveAnswers(10, "" + isChecked, "Vomiting")
                chkboxNoSymtoms.isChecked = false
            }
            R.id.chkboxNoSymtoms -> {
                saveAnswers(11, "" + isChecked, "No Symtoms")
                chkboxCough.isChecked = false
                chkboxDiarhea.isChecked = false
                chkboxMuscleAche.isChecked = false
                chkboxSoreThroat.isChecked = false
                chkboxRunnyNose.isChecked = false
                chkboxVomiting.isChecked = false
            }
        }
    }

    fun saveAnswers(position: Int, answers: String, question: String) {
        mQuestionReq = QuestionListRequest()
        mQuestionReq?.answer = answers
        mQuestionReq?.question = question
        mHashMapQuestion.put(position, mQuestionReq)
        if (checkAllAnswered() && checkCheckBox()) {
            btnSubmitRisk.visibility = View.VISIBLE
        }
    }

    fun saveHealthDataApi() {

        val arrListHealth = ArrayList<QuestionListRequest?>()
        mHashMapQuestion.entries.mapIndexed { index, data ->
            if (index >= 5 && index <= 11) {
                if (data.value?.answer.equals("true", false)) {
                    arrListHealth.add(data.value)
                } else {
                }
            } else {
                arrListHealth.add(data.value)
            }
        }

        mHealthDataRequest?.questionAnswer = arrListHealth

        mHealthDataRequest?.let {
            mHealthService?.postHealthData(it)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
//                    onHealthDataSuccess(it)
                    finish()
                    hideProgress()
                    HealthApplication.showToast("Data saved successfully")
                }, {
//                    onHealthDataError(it)
                    HealthApplication.showToast("Server Error")
                    hideProgress()
                })
        }

    }


}

