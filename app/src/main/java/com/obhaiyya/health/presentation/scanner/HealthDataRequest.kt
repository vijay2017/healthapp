package com.obhaiyya.health.presentation.scanner

import java.io.Serializable

class HealthDataRequest : Serializable {

    var userId = ""
    var userEmail = ""
    var trackDate = ""
    var heartRate = 0
    var respiration = 0
    var spo2 = 0
    var stress = 87
    var hrv = 0
    var bodyMass = 0
    var temperature = 102
    var lossOfTaste = false
    var isRest = false
    var covidPossibility = "unlikely"
    var questionAnswer = ArrayList<QuestionListRequest?>()

}

 class QuestionListRequest : Serializable {
    public var question = ""
    var answer = ""

}