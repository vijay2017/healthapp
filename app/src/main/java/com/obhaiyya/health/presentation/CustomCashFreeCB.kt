package com.obhaiyya.health.presentation

import android.content.Context
import com.appwithmeflutter.mype.data.network.HealthService
import com.closerbuy.customerapp.network.CashFreeRequest
import com.example.obhaiyyahealth.dataProvider.ApiService
import com.gocashfree.cashfreesdk.CFClientInterface
import com.gocashfree.cashfreesdk.CFPaymentService
import com.obhaiyya.health.HealthApplication
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.paygateway.JSONConstants
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*

object CustomCashFreeCB : CFClientInterface {

    var mOnUiCallback: OnCustomCashFreeCallback? = null

    @Synchronized
    fun getAppIdAndMakePayment(
            context: Context,
            amount: String,
            onUiCallback: OnCustomCashFreeCallback,
            mHealthService: HealthService?
    ) {
        mOnUiCallback = onUiCallback
        val userId = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID)

        mHealthService?.getPaymentCashFreeAppId(userId, amount)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    getCFTokenAndCallCashFreeSDk(it, context)
                }, {
                    HealthApplication.showToast(it.message)
                })
    }

    @Synchronized
    private fun getCFTokenAndCallCashFreeSDk(
            paymentAppIdRes: PaymentAppIdResponse?,
            context: Context
    ) {
        val mCashFreeService = ApiService.getInstance()?.initCashFreeCFToken()
        val mCashFreeRequest = CashFreeRequest()
        mCashFreeRequest.orderId = paymentAppIdRes?.orderId ?: ""
        mCashFreeRequest.orderAmount = paymentAppIdRes?.orderAmount ?: 0
        mCashFreeRequest.orderCurrency = paymentAppIdRes?.orderCurrency ?: ""

        val clientId = paymentAppIdRes?.appId
        val clientToken = paymentAppIdRes?.paymentToken

        mCashFreeService?.getCFTOKEN(clientId, clientToken, mCashFreeRequest)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    doPaymentCashFree(it.cftoken, paymentAppIdRes, context)
                }, {
                    HealthApplication.showToast(it.message)
                })

    }

    @Synchronized
    private fun doPaymentCashFree(
            cftoken: String?,
            paymentAppIdRes: PaymentAppIdResponse?,
            context: Context
    ) {
        val cfPaymentService = CFPaymentService.getCFPaymentServiceInstance()
        val paramsMap: MutableMap<String, String> = LinkedHashMap()

        paramsMap[JSONConstants.APP_ID] = paymentAppIdRes?.appId ?: ""
        paramsMap[JSONConstants.ORDER_ID] = paymentAppIdRes?.orderId ?: ""
        paramsMap[JSONConstants.ORDER_AMT] = paymentAppIdRes?.orderAmount.toString()
        paramsMap[JSONConstants.CUST_NAME] = paymentAppIdRes?.customer?.name.toString()
        paramsMap[JSONConstants.CUST_MOBILE] = paymentAppIdRes?.customer?.mobileNumber.toString()
        paramsMap[JSONConstants.CUST_EMAIL] = paymentAppIdRes?.customer?.email.toString()
        paramsMap[JSONConstants.ORDER_CURRENCY] = "INR"

        cfPaymentService.doPayment(context, paramsMap, cftoken, this, "PROD")
    }

    override fun onSuccess(value: MutableMap<String, String>?) {
        mOnUiCallback?.onSuccess(value)
    }

    override fun onFailure(p0: MutableMap<String, String>?) {
        mOnUiCallback?.onFailure()
    }

    override fun onNavigateBack() {
        mOnUiCallback?.onNavigateBackWithoutPayment()
    }

    interface OnCustomCashFreeCallback {
        fun onSuccess(value: MutableMap<String, String>?)
        fun onFailure()
        fun onNavigateBackWithoutPayment()
    }

}
