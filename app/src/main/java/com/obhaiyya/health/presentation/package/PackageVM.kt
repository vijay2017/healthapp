package com.example.obhaiyyahealth.presentation.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.appwithmeflutter.mype.presentation.base.BaseVM
import com.obhaiyya.health.presentation.Package.CreatePackageRequest
import com.obhaiyya.health.presentation.Package.CreatePackageResponse
import com.obhaiyya.health.presentation.Package.GetAllPlansResponse
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Created by Vijay on 22/3/20.
 */

class PackageVM : BaseVM() {

    var mMutableGetAllPlans = MutableLiveData<GetAllPlanModel>()

    fun getAllPlans() {
        val getAllPlans = GetAllPlanModel()
        mCustomerService?.getAllPackageHealthPlans()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                Log.d("VIJAY", "SUCCESS-OTP")
                getAllPlans.success = true
                getAllPlans.mlist = it
                mMutableGetAllPlans.value = getAllPlans
            }, {
                getAllPlans.success = false
                mMutableGetAllPlans.value = getAllPlans
                Log.d("VIJAY", "ERROR-OTP")
            })
    }

}


class GetAllPlanModel {
    var mlist: List<GetAllPlansResponse>? = null
    var success = false
}