package com.obhaiyya.health.presentation

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaymentAppIdResponse {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("orderId")
    @Expose
    var orderId: String? = null

    @SerializedName("orderCurrency")
    @Expose
    var orderCurrency: String? = null

    @SerializedName("orderAmount")
    @Expose
    var orderAmount: Int? = null

    @SerializedName("customer")
    @Expose
    var customer: CustomerResponse? = null

    @SerializedName("vendorId")
    @Expose
    var vendorId: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("__v")
    @Expose
    var v: Int? = null

    @SerializedName("appId")
    @Expose
    var appId: String? = null

    @SerializedName("paymentToken")
    @Expose
    var paymentToken: String? = null

    @SerializedName("paymentMode")
    @Expose
    var paymentMode: String? = null

}

class CustomerResponse {

    @SerializedName("locations")
    @Expose
    var locations: List<Any>? = null

    @SerializedName("products")
    @Expose
    var products: List<Any>? = null

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("mobileNumber")
    @Expose
    var mobileNumber: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("mobileOTP")
    @Expose
    var mobileOTP: Any? = null

    @SerializedName("authToken")
    @Expose
    var authToken: String? = null

    @SerializedName("appartment")
    @Expose
    var appartment: String? = null

    @SerializedName("appartmentId")
    @Expose
    var appartmentId: String? = null

    @SerializedName("bathrooms")
    @Expose
    var bathrooms: Int? = null

    @SerializedName("bhk")
    @Expose
    var bhk: String? = null

    @SerializedName("block")
    @Expose
    var block: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("flatNumber")
    @Expose
    var flatNumber: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("notificationToken")
    @Expose
    var notificationToken: String? = null
    
}