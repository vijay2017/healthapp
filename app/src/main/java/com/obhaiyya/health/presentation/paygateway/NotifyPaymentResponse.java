package com.obhaiyya.health.presentation.paygateway;

import android.os.Parcel;
import android.os.Parcelable;

public class NotifyPaymentResponse implements Parcelable {

    public static final Creator<NotifyPaymentResponse> CREATOR = new Creator<NotifyPaymentResponse>() {
        @Override
        public NotifyPaymentResponse createFromParcel(Parcel in) {
            return new NotifyPaymentResponse(in);
        }

        @Override
        public NotifyPaymentResponse[] newArray(int size) {
            return new NotifyPaymentResponse[size];
        }
    };
    private double orderAmount;
    private String status;
    private String orderId;
    private String paymentMode;
    private String orderCurrency;
    private String lastUpdatedTime;
    private String _id;
    private String vendorId;


    protected NotifyPaymentResponse(Parcel in) {
        orderAmount = in.readDouble();
        status = in.readString();
        orderId = in.readString();
        paymentMode = in.readString();
        orderCurrency = in.readString();
        lastUpdatedTime = in.readString();
        _id = in.readString();
        vendorId = in.readString();

    }

    public double getAmt() {
        return orderAmount;
    }
    public String getStatus() {
        return status;
    }
    public String getOrderId() {
        return orderId;
    }
    public String getPaymentMode() {
        return paymentMode;
    }
    public String getOrderCurrency() {
        return orderCurrency;
    }
    public String getLastUpdatedTime() {
        return lastUpdatedTime;
    }
    public String get_id() {
        return _id;
    }
    public String getVendorId() {
        return vendorId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(orderAmount);
        parcel.writeString(status);
        parcel.writeString(orderId);
        parcel.writeString(paymentMode);
        parcel.writeString(orderCurrency);
        parcel.writeString(lastUpdatedTime);
        parcel.writeString(_id);
        parcel.writeString(vendorId);

    }
}
