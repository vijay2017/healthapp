package com.obhaiyya.health.presentation.summary

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.dashboard.GetAllDashboardHealthPlanResponse

class SummaryAdapter(
    val mContext: Context,
    val mGetAllHealthDataRes: GetAllDashboardHealthPlanResponse
) :
    RecyclerView.Adapter<SummaryAdapter.DashboaordViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboaordViewHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.activity_summary, parent, false)
        return DashboaordViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashboaordViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return 1
    }

    inner class DashboaordViewHolder(view: View) : RecyclerView.ViewHolder(view) {



        fun bind(position: Int) {

        }

    }

}