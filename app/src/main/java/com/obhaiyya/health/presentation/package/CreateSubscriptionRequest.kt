package com.obhaiyya.health.presentation.Package

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CreatePackageRequest {

    var id: String? = null
    var userId: String? = null
    var amount: String? = null
    var startDate: String? = null
    var endDate: String? = null
    var paymentDoneDate: String? = null
    var paymentGatewayResponses: PaymentGatewayPackage? = null
    var planDetails: PlanDetailsPackage? = null
    var userDetails: UserDetailsPackage? = null
    var createdAt: String? = null
    var updatedAt: String? = null
    var v: Int? = null

}

class PaymentGatewayPackage {

    var orderAmount: Int? = null
    var orderId: String? = null
    var orderCurrency: String? = null
    var vendorId: String? = null
    var lastUpdatedTime: String? = null
    var _id: String? = null
    var cftoken: String? = null
}

class PlanDetailsPackage {
    var _id: String? = null
    var amount: Int? = null
    var validity: Int? = null
    var planName: String? = null

}

class UserDetailsPackage {

    var _id: String? = null
    var email: String? = null
    var createdAt: String? = null
    var updatedAt: String? = null
    var authToken: String? = null
    var gmailToken: String? = null
    var appartment: String? = null
    var block: String? = null
    var dateOfBirth: String? = null
    var flatNumber: String? = null
    var gender: String? = null
    var height: Double? = null
    var mobileNumber: String? = null
    var name: String? = null
    var notificationToken: String? = null
    var weight: Double? = null

}