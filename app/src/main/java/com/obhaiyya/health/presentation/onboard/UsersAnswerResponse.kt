package com.obhaiyya.health.presentation.onboard

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UsersAnswerResponse {

    @SerializedName("locations")
    @Expose
    var locations: List<Any>? = null

    @SerializedName("products")
    @Expose
    var products: List<Any>? = null

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("authToken")
    @Expose
    var authToken: String? = null

    @SerializedName("gmailToken")
    @Expose
    var gmailToken: String? = null

    @SerializedName("appartment")
    @Expose
    var appartment: String? = null

    @SerializedName("block")
    @Expose
    var block: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    var dateOfBirth: String? = null

    @SerializedName("flatNumber")
    @Expose
    var flatNumber: String? = null

    @SerializedName("gender")
    @Expose
    var gender: String? = null

    @SerializedName("height")
    @Expose
    var height: Double? = null

    @SerializedName("mobileNumber")
    @Expose
    var mobileNumber: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("notificationToken")
    @Expose
    var notificationToken: String? = null

    @SerializedName("weight")
    @Expose
    var weight: Double? = null

}