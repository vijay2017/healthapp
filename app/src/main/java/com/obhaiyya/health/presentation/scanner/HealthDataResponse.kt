package com.obhaiyya.health.presentation.scanner

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class HealthDataResponse {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("userId")
    @Expose
    var userId: String? = null

    @SerializedName("userEmail")
    @Expose
    var userEmail: String? = null

    @SerializedName("trackDate")
    @Expose
    var trackDate: String? = null

    @SerializedName("temperature")
    @Expose
    var temperature: Int? = null

    @SerializedName("heartRate")
    @Expose
    var heartRate: Int? = null

    @SerializedName("respiration")
    @Expose
    var respiration: Int? = null

    @SerializedName("spo2")
    @Expose
    var spo2: Int? = null

    @SerializedName("stress")
    @Expose
    var stress: Int? = null

    @SerializedName("hrv")
    @Expose
    var hrv: Int? = null

    @SerializedName("isRest")
    @Expose
    var isRestOrWalk: Boolean? = null

    @SerializedName("bodyMass")
    @Expose
    var bodyMass: Int? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("__v")
    @Expose
    var v: Int? = null


}