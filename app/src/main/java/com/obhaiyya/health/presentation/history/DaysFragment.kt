package com.obhaiyya.health.presentation.history

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseFragment
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import kotlinx.android.synthetic.main.fragment_days.*
import kotlinx.android.synthetic.main.fragment_days.lineChartHeart
import kotlinx.android.synthetic.main.fragment_days.lineChartRespiration
import kotlinx.android.synthetic.main.fragment_days.lineChartSdnn
import kotlinx.android.synthetic.main.fragment_days.lineChartSpo2
import kotlinx.android.synthetic.main.fragment_days.lineChartStress
import kotlinx.android.synthetic.main.fragment_days.txtDaysDate
import kotlinx.android.synthetic.main.fragment_thirty.*
import org.joda.time.format.ISODateTimeFormat
import java.lang.reflect.Array
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.ArrayList


class DaysFragment : BaseFragment() {


    companion object {

        val SUNDAY = "Sunday"
        val MONDAY = "Monday"
        val Tuesday = "Tuesday"
        val Wednesday = "Wednesday"
        val Thursday = "Thursday"
        val Friday = "Friday"
        val Saturday = "Saturday"

        val KEY_HISTORY_RESPONSE = "keyHistoryResponse"

        fun newInstance(historyResponse: TrendsLogsResponse): DaysFragment {
            val daysFragment = DaysFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_HISTORY_RESPONSE, historyResponse)
            daysFragment.arguments = bundle
            return daysFragment
        }
    }

    override fun getLayout(): Int {
        return R.layout.fragment_days
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val historyResponse = arguments?.getSerializable(KEY_HISTORY_RESPONSE) as TrendsLogsResponse

        setGraph(historyResponse, 0)
        setGraph(historyResponse, 1)
        setGraph(historyResponse, 2)
        setGraph(historyResponse, 3)
        setGraph(historyResponse, 4)

    }

    fun dataValues(historyResponse: TrendsLogsResponse, pos: Int): ArrayList<Entry> {

        when (pos) {
            0 -> {
                var sum = 0
                var count = 0
                val mDataVal = ArrayList<Entry>()
                val mListEntry = ArrayList<Int>()
                historyResponse.heartRate?.mapIndexed { index, heartRate ->
                    sum = 0
                    count = heartRate.healthList?.size ?: 0
                    heartRate.healthList?.mapIndexed { index, health ->
                        sum = sum + (health.value ?: 0) / count
                    }
                    mListEntry.add(sum ?: 0)
                }

                val reverseList = mListEntry.reversed()
                reverseList.mapIndexed { index, health ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            health.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            1 -> {
                var sum = 0
                var count = 0
                val mDataVal = ArrayList<Entry>()
                val mListEntry = ArrayList<Int>()
                historyResponse.spo2?.mapIndexed { index, heartRate ->
                    sum = 0
                    count = heartRate.healthList?.size ?: 0
                    heartRate.healthList?.mapIndexed { index, health ->
                        sum = sum + (health.value ?: 0) / count
                    }
                    mListEntry.add(sum ?: 0)
                }
                val reverseList = mListEntry.reversed()
                reverseList.mapIndexed { index, health ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            health.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            2 -> {
                var sum = 0
                var count = 0
                val mDataVal = ArrayList<Entry>()
                val mListEntry = ArrayList<Int>()
                historyResponse.respiration?.mapIndexed { index, heartRate ->
                    sum = 0
                    count = heartRate.healthList?.size ?: 0
                    heartRate.healthList?.mapIndexed { index, health ->
                        sum = sum + (health.value ?: 0) / count
                    }
                    mListEntry.add(sum ?: 0)
                }
                val reverseList = mListEntry.reversed()
                reverseList.mapIndexed { index, health ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            health.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            3 -> {
                var sum = 0
                var count = 0
                val mDataVal = ArrayList<Entry>()
                val mListEntry = ArrayList<Int>()
                historyResponse.hrv?.mapIndexed { index, heartRate ->
                    sum = 0
                    count = heartRate.healthList?.size ?: 0
                    heartRate.healthList?.mapIndexed { index, health ->
                        sum = sum + (health.value ?: 0) / count
                    }
                    mListEntry.add(sum ?: 0)
                }
                val reverseList = mListEntry.reversed()
                reverseList.mapIndexed { index, health ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            health.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            4 -> {
                var sum = 0
                var count = 0
                var isValueAvailable = false
                val mDataVal = ArrayList<Entry>()
                val mListEntry = ArrayList<Int>()
                historyResponse.stress?.mapIndexed { index, heartRate ->
                    sum = 0
                    isValueAvailable = false
                    count = heartRate.healthList?.size ?: 0
                    if (count != 0) {
                        heartRate.healthList?.mapIndexed { index, health ->
                            sum = sum + (health.value ?: 0) / count
                            isValueAvailable = true
                        }
                    }
                    if (isValueAvailable && sum <= 0) {
                        mListEntry.add(1)
                    } else {
                        mListEntry.add(sum)
                    }
                }
                val reverseList = mListEntry.reversed()
                reverseList.mapIndexed { index, health ->
                    mDataVal.add(
                        Entry(index.toFloat(), health.toFloat() ?: 0.0F)
                    )
                }
                return mDataVal
            }
        }
        val mDataVal = ArrayList<Entry>()
        return mDataVal
    }

    fun setGraph(historyResponse: TrendsLogsResponse, pos: Int) {

        when (pos) {
            0 -> {
                val mXAxisList = ArrayList<String>()
                val leftAxis: YAxis = lineChartHeart.getAxisLeft()
                leftAxis.axisMaximum = 230f
                leftAxis.axisMinimum = 40f
//                historyResponse?.heartRate?.mapIndexed { index, heartRate ->
//                    heartRate?.date?.let {
//                        mXAxisList.add(it)
//                    }
//                }
                lineChart(mXAxisList, lineChartHeart, pos, historyResponse)
            }
            1 -> {
                val mXAxisList = ArrayList<String>()
                val leftAxis: YAxis = lineChartSpo2.getAxisLeft()
                leftAxis.axisMaximum = 100f
                leftAxis.axisMinimum = 80f
//                historyResponse?.spo2?.map {
//                    it.date?.let { value ->
//                        mXAxisList.add(value)
//                    }
//                }
                lineChart(mXAxisList, lineChartSpo2, pos, historyResponse)
            }
            2 -> {
                val mXAxisList = ArrayList<String>()
                val leftAxis: YAxis = lineChartRespiration.getAxisLeft()
                leftAxis.axisMaximum = 40f
                leftAxis.axisMinimum = 6f
//                historyResponse?.respiration?.map {
//                    it.date?.let { value ->
//                        mXAxisList.add(value)
//                    }
//                }
                lineChart(mXAxisList, lineChartRespiration, pos, historyResponse)
            }
            3 -> {
                val mXAxisList = ArrayList<String>()
                val leftAxis: YAxis = lineChartSdnn.getAxisLeft()
                leftAxis.axisMaximum = 220f
                leftAxis.axisMinimum = 10f
//                historyResponse?.hrv?.map {
//                    it.date?.let { value ->
//                        mXAxisList.add(value)
//                    }
//                }
                lineChart(mXAxisList, lineChartSdnn, pos, historyResponse)
            }
            4 -> {
                val mXAxisList = ArrayList<String>()
                val leftAxis: YAxis = lineChartStress.getAxisLeft()
                leftAxis.axisMaximum = 5f
                leftAxis.axisMinimum = 1f
//                historyResponse?.stress?.map {
//                    it.date?.let { value ->
//                        mXAxisList.add(value)
//                    }
//                }
                lineChart(mXAxisList, lineChartStress, pos, historyResponse)
            }
        }

        txtDaysDate.text =
            DateFormatUtils.getDaysAndLastDays(-7) + " " + "to" + "  " + DateFormatUtils.getCurrentDate()

        txtSpo2Startseven.text = DateFormatUtils.getCurrentDate()
        txtSpo2Endseven.text = DateFormatUtils.getDaysAndLastDays(-7)

        txtHRStartseven.text = DateFormatUtils.getCurrentDate()
        txtHREndseven.text = DateFormatUtils.getDaysAndLastDays(-7)

        txtRespirationStartseven.text = DateFormatUtils.getCurrentDate()
        txtRespirationEndseven.text = DateFormatUtils.getDaysAndLastDays(-7)

        txtHRVStartseven.text = DateFormatUtils.getCurrentDate()
        txtHRVEndseven.text = DateFormatUtils.getDaysAndLastDays(-7)

        txtStressStartseven.text = DateFormatUtils.getCurrentDate()
        txtStressEndseven.text = DateFormatUtils.getDaysAndLastDays(-7)


    }

    fun lineChart(
        mXAxisList: ArrayList<String>,
        lineChartDays: LineChart,
        pos: Int,
        historyResponse: TrendsLogsResponse
    ) {
        var dataSets: ArrayList<ILineDataSet?> = ArrayList()

        val incomeEntries: List<Entry> = dataValues(historyResponse, pos)
        dataSets = ArrayList()
        val set1: LineDataSet

        set1 = LineDataSet(incomeEntries, "Health")
        set1.color = Color.rgb(65, 168, 121)
        set1.valueTextColor = Color.rgb(55, 70, 73)
        set1.valueTextSize = 12f
        set1.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
        dataSets.add(set1)

        lineChartDays.setTouchEnabled(false)
        lineChartDays.setScaleEnabled(false)
        lineChartDays.setPinchZoom(false)
        lineChartDays.extraLeftOffset = 25f
        lineChartDays.extraRightOffset = 25f

        val rightYAxis = lineChartDays.axisRight
        rightYAxis.isEnabled = false
        val leftYAxis = lineChartDays.axisLeft
        leftYAxis.isEnabled = false
        val xAxis = lineChartDays.xAxis
        xAxis.textSize = 8f
        xAxis.granularity = 1f
        xAxis.isEnabled = true
        lineChartDays.axisRight.setDrawAxisLine(true)
        xAxis.setDrawGridLines(false)
        xAxis.position = XAxis.XAxisPosition.BOTTOM

        set1.lineWidth = 4f
        set1.circleRadius = 3f
        set1.valueFormatter = MyValueFormatter()
        set1.circleHoleColor = resources.getColor(R.color.colorPrimary)
        set1.setCircleColor(resources.getColor(R.color.colorPrimary))
        lineChartDays.xAxis.valueFormatter = IndexAxisValueFormatter(mXAxisList)

        val data = LineData(dataSets)
        lineChartDays.data = data
        lineChartDays.animateX(3000)
        lineChartDays.invalidate()
        lineChartDays.legend.isEnabled = false
        lineChartDays.description.isEnabled = false
    }

    class MyValueFormatter : ValueFormatter() {
        private val format = DecimalFormat("###,##0")

        // override this for e.g. LineChart or ScatterChart
        override fun getPointLabel(entry: Entry?): String {
            return format.format(entry?.y)
        }

        // override this for BarChart
        override fun getBarLabel(barEntry: BarEntry?): String {
            return format.format(barEntry?.y)
        }

        // override this for custom formatting of XAxis or YAxis labels
        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            return format.format(value)
        }
        // ... override other methods for the other chart types
    }


}


