package com.obhaiyya.health.presentation.utils

import org.joda.time.format.ISODateTimeFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object DateFormatUtils {

    fun getSelectedDateInUTCFormat(date: String): String? {
        val dateFormatter = SimpleDateFormat("dd/MM/yyyy")
        dateFormatter.setTimeZone(TimeZone.getDefault())
        var value: Date? = null
        try {
            value = dateFormatter.parse(date)
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
            return formatter.format(value)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

    fun getFormattedDateInDDMMYYYY(date: String): String? {
        val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        dateFormatter.setTimeZone(TimeZone.getDefault())
        var value: Date? = null
        try {
            value = dateFormatter.parse(date)
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
            return formatter.format(value)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

    fun getCurrentDate(): String {
        val c = Calendar.getInstance().time
        println("Current time => $c")
        val df = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val formattedDate = df.format(c)
        return formattedDate
    }

    fun getNoOfDaysToDate(noOfDays: Int, date: String): String {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val c = Calendar.getInstance()
        try {
            c.time = sdf.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        c.add(
            Calendar.DATE,
            noOfDays
        ) // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE

        val sdf1 = SimpleDateFormat("dd/MM/yyyy")
        val output = sdf1.format(c.time)
        return output
    }

    fun getEndTimeDate(noOfDays: Int): String {
        val date = Date()
        val df = SimpleDateFormat("dd/MM/yyyy")
        val c1 = Calendar.getInstance()
        c1.add(Calendar.DAY_OF_YEAR, noOfDays)
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
        val resultDate = c1.time
        val dueDate = formatter.format(resultDate)
        return dueDate
    }

    fun convertInDateTime(dateTime: String?): String? {
        val dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(dateTime ?: "")
        val output: String = dateTime.toString("h:mm aa")
        return output
    }

    fun convertInDDMMYYYY(dateTime: String?): String? {
        val dateTime = ISODateTimeFormat.dateTimeParser().parseDateTime(dateTime ?: "")
        val output: String = dateTime.toString("dd/MM/yyyy")
        return output
    }

    fun getDaysAndLastDays(noOfDays: Int): String {
        val sdf = SimpleDateFormat("dd/MM ")
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_YEAR, noOfDays)
        println(sdf.format(cal.time))
        return sdf.format(cal.time)
    }


}