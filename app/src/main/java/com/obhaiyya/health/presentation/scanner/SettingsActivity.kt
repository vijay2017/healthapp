package com.obhaiyya.health.presentation.scanner;

import ai.binah.hrv.HealthMonitorManager
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.obhaiyya.health.BuildConfig
import com.obhaiyya.health.databinding.ActivitySettingsBinding


class SettingsActivity : AppCompatActivity() {

    lateinit var binding: ActivitySettingsBinding
    val LICENSE_KEY = "32051F-A6857A-42C59D-BFFBCD-86ED76-916E36"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        binding.appVersion.text = "Version: " +
                "App ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE}) " +
                "SDK: ${ai.binah.hrv.BuildConfig.VERSION_NAME} (${ai.binah.hrv.BuildConfig.VERSION_CODE})"

        binding.revalidateButton.setOnClickListener {
            HealthMonitorManager.clearFingerEvaluationResult()
            binding.revalidateText.setTextColor(Color.GRAY)
            binding.revalidateButton.setOnClickListener(null)
        }
        loadSettings()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            if (saveSettings()) {
                setResult(RESULT_OK)
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun loadSettings() {
        val preferences = getSharedPreferences("binah_sample", Context.MODE_PRIVATE)
        binding.licenseInput.setText(preferences.getString("licenseKey", "") ?: "")
        preferences.getLong("sessionTime", -1).takeIf { it != -1L }?.let {
            binding.sessionTimeInput.setText(preferences.getLong("sessionTime", it).toString())
        } ?: run {
            binding.sessionTimeInput.setText("")
        }
    }

    private fun saveSettings(): Boolean {
        val preferences = getSharedPreferences("binah_sample", Context.MODE_PRIVATE)
        preferences.edit().putString("licenseKey", LICENSE_KEY).apply()
        resolveSessionTime().takeIf { it != -1L }?.let {
            preferences.edit().putLong("sessionTime", it).apply()
        } ?: run {
            preferences.edit().remove("sessionTime").apply()
        }

        return true
    }

    private fun resolveSessionTime(): Long {
        return try {
            binding.sessionTimeInput.text.toString().toLong()
        } catch (ignore: NumberFormatException) {
            -1
        }
    }
}