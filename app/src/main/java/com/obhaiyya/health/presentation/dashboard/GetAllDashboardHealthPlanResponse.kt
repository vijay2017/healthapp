package com.obhaiyya.health.presentation.dashboard

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class GetAllDashboardHealthPlanResponse : Serializable {

    @SerializedName("isRest")
    @Expose
    var isRest: Boolean? = null

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("userId")
    @Expose
    var userId: String? = null

    @SerializedName("userEmail")
    @Expose
    var userEmail: String? = null

    @SerializedName("trackDate")
    @Expose
    var trackDate: String? = null

    @SerializedName("temperature")
    @Expose
    var temperature: Int? = null

    @SerializedName("heartRate")
    @Expose
    var heartRate: Int? = null

    @SerializedName("respiration")
    @Expose
    var respiration: Int? = null

    @SerializedName("spo2")
    @Expose
    var spo2: Int? = null

    @SerializedName("stress")
    @Expose
    var stress: Int? = null

    @SerializedName("hrv")
    @Expose
    var hrv: Int? = null

    @SerializedName("bodyMass")
    @Expose
    var bodyMass: Int? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("lossOfTaste")
    @Expose
    var lossOfTaste: Boolean? = null

    @SerializedName("temparature")
    @Expose
    var temparature: String? = null

}