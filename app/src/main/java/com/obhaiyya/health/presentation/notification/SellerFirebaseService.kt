package com.obhaiyya.health.presentation.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.obhaiyya.health.presentation.scanner.HomeActivity
import com.obhaiyya.health.presentation.utils.Constant
import com.obhaiyya.health.presentation.utils.Constant.Companion.IS_INTENT_FROM_NOTIF
import com.obhaiyya.health.presentation.utils.Constant.Companion.KEY_ACTION
import com.obhaiyya.health.presentation.utils.Constant.Companion.NOTIFICATION_ID
import com.obhaiyya.health.presentation.utils.Constant.Companion.RX_ACTION
import java.util.*

class SellerFirebaseService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.i("SellerFirebaseService ", "Message :: $remoteMessage")

        processMessage(remoteMessage.data)
    }

    private fun processMessage(data: Map<String, String>) {
        showNotification(data, data["message"]!!)
    }

    private fun showNotification( data: Map<String, String>,
                                  actionText: String){
        val notificationId = System.currentTimeMillis().toInt()
        val channelId = "channel_id_obhaiyya_health_app"
        val complaintIntent = Intent(this, NotificationReceiver::class.java)
        val soundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        //complaintIntent.putExtra(KEY_ACTION, ACTION_FOOD_ORDER)
        complaintIntent.putExtra(NOTIFICATION_ID, notificationId)
        complaintIntent.action = RX_ACTION
        val pendingCompltIntent = PendingIntent.getBroadcast(
            this,
            notificationId,
            complaintIntent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val noti = NotificationCompat.Builder(this, channelId)
            .setContentTitle(data["title"])
            .setContentText(data["message"])
            .addAction(0, actionText, pendingCompltIntent).setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(pendingCompltIntent).build()
        noti.flags = noti.flags or Notification.FLAG_AUTO_CANCEL
        val mgr: NotificationManager = getNotificationManager(channelId)
        mgr.notify(notificationId, noti)
        val notification =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        val r = RingtoneManager.getRingtone(applicationContext, notification)
        r.play()
        goToMainActivityWithCategory(this)
    }

    private fun goToMainActivityWithCategory(
        context: Context
    ) {
        val mainIntent = Intent(context, com.obhaiyya.health.presentation.scanner.HomeActivity::class.java)
        mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        mainIntent.putExtra(
            Constant.IS_INTENT_FROM_NOTIF,
            true
        )

        context.startActivity(mainIntent)
    }
    private fun getNotificationManager(channelId: String): NotificationManager {
        val manager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "channel_obhaiyya_health_app"
            val description = "channel_desc_obhaiyya_health_app"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(channelId, name, importance)
            channel.description = description
            manager.createNotificationChannel(channel)
            Log.i( "Network created ", name.toString())
        }
        return manager
    }
}