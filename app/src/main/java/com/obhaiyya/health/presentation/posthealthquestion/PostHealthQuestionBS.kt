package com.obhaiyya.health.presentation.posthealthquestion

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseBottomSheet
import kotlinx.android.synthetic.main.bs_post_health_question.*

class PostHealthQuestionBS : BaseBottomSheet(), View.OnClickListener,
    RadioGroup.OnCheckedChangeListener {

    var mIPaymentBottomHandler: IPostHealthQuestionHandler? = null
    var lossOfTaste = false

    var mAmountSelected = 0.0

    companion object {

        val KEY_AMOUNT_SELECTED = "keyAmountSelected"

        fun newInstance(): PostHealthQuestionBS {
            val paymentBottomSheet = PostHealthQuestionBS()
            return paymentBottomSheet
        }

    }

    override fun getLayout(): Int {
        return R.layout.bs_post_health_question
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.BottomSheetDialog)
        isCancelable = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSubmitHealthQuestion.setOnClickListener(this)
        rdGrpSmell.setOnCheckedChangeListener(this)


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSubmitHealthQuestion -> {
                if (checkAllAnswered()) {
                    val temparature = edtTemparature.text.toString()
                    dismiss()
//                    mIPaymentBottomHandler?.onSubmitClick(temparature, lossOfTaste)
//                    btnSubmitHealthQuestion.visibility = View.INVISIBLE
                } else {
                    activity?.let {
                        Toast.makeText(it, "Field cannot be empty", Toast.LENGTH_SHORT).show()
                    }
                }

            }
        }
    }

    interface IPostHealthQuestionHandler {
        fun onSubmitClick(temparature: String, lossOfTaste: Boolean)
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtnSmellYes -> {
                lossOfTaste = true
                checkAllAnswered()
            }
            R.id.rdBtnSmellNo -> {
                lossOfTaste = false
                checkAllAnswered()
            }
        }
    }


    fun checkAllAnswered(): Boolean {
        val answer1 = edtTemparature.text.toString().isNotEmpty()
        val answer2 = rdBtnSmellYes.isChecked || rdBtnSmellNo.isChecked
        return answer1 && answer2
    }

}

