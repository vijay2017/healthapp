package com.obhaiyya.health.presentation.history

import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.tabs.TabLayout
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_history_parent.*


class HistoryParentHolderFragment : BaseFragment() {

    override fun getLayout(): Int {
        return R.layout.fragment_history_parent
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val firstTab: TabLayout.Tab = tabHistoryParent.newTab()
        firstTab.text = "Trends"
        tabHistoryParent.addTab(firstTab)
        val secondTab: TabLayout.Tab = tabHistoryParent.newTab()
        secondTab.text = "Logs"
        tabHistoryParent.addTab(secondTab)

        replaceFragment(R.id.frame_parent_history, HistoryChildHolderFragment())

        tabHistoryParent.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                Log.d("onTabReselected", "0 position")
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                Log.d("onTabUnselected", "0 position")
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        Log.d("onTabSelected", "0 position")
                        replaceFragment(R.id.frame_parent_history, HistoryChildHolderFragment())
                    }
                    1 -> {
                        Log.d("onTabSelected", "1 position")
                        replaceFragment(R.id.frame_parent_history, LogsFragment())
                    }
                }
            }
        })

    }


}