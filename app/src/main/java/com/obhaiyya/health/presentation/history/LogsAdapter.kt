package com.obhaiyya.health.presentation.history

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.obhaiyya.health.R

class LogsAdapter(
    val mContext: Context,
    val mLogsList: List<LogsResponse>
) :
    RecyclerView.Adapter<LogsAdapter.DashboaordViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboaordViewHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.adapter_logs, parent, false)
        return DashboaordViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashboaordViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mLogsList.size
    }

    inner class DashboaordViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val rvListLogs = view.findViewById<RecyclerView>(R.id.rvLogsList)
        val logsDate = view.findViewById<TextView>(R.id.logsDate)
        val txtNoLogsData = view.findViewById<View>(R.id.txtNoLogsData)

        fun bind(position: Int) {
            logsDate.text = mLogsList.get(position).date
            val logHealthList = mLogsList.get(position).healthList

            if (logHealthList?.isEmpty() == true) {
                rvListLogs.visibility = View.GONE
                txtNoLogsData.visibility = View.VISIBLE
            } else {
                rvListLogs.visibility = View.VISIBLE
                txtNoLogsData.visibility = View.GONE
                setLogListAdapter(logHealthList)
            }

        }

        private fun setLogListAdapter(logHealthList: List<HealthLogs>?) {
            val linearLayoutManager =
                LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
            rvListLogs.layoutManager = linearLayoutManager
            val adapter = LogsListAdapter(mContext, logHealthList)
            rvListLogs.adapter = adapter
        }

    }

}