package com.obhaiyya.health.presentation.scanner

import ai.binah.hrv.HealthMonitorManager
import ai.binah.hrv.HealthMonitorManager.HealthMonitorManagerInfoListener
import ai.binah.hrv.api.*
import ai.binah.hrv.session.api.HealthMonitorSession
import ai.binah.hrv.session.api.HealthMonitorSession.SessionState
import ai.binah.hrv.session.api.HealthMonitorSession.StateListener
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.*
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.obhaiyya.health.HealthApplication
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.databinding.ActivityHomeBinding
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.dashboard.GetAllDashboardHealthPlanResponse
import com.obhaiyya.health.presentation.posthealthquestion.CheckRiskRequest
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import com.obhaiyya.health.presentation.utils.Navigator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.toolbarhome
import java.util.*

class HomeActivity : BaseActivity(), HealthMonitorManagerInfoListener,
    StateListener, HealthMonitorFaceSessionListener, HealthMonitorFingerSessionListener {

    private enum class SessionMode {
        FACE, FINGER
    }

    enum class UiState {
        LOADING, MEASURING, IDLE
    }

    private var mBinding: ActivityHomeBinding? = null
    private val SETTINGS_ACTIVITY_REQUEST_CODE = 12345
    private var mManager: HealthMonitorManager? = null
    private var mSession: HealthMonitorSession? = null
    private var mTime = 0
    private var mTimeCountHandler: Handler? = null
    private var mWarningDialogTimeoutHandler: Handler? = null
    private var mMeasurementTimeHandler: Handler? = null
    private var mMeasurementEndTime: Long = 0
    private var mMessageDialog: AlertDialog? = null
    private var mTestMode = SessionMode.FACE
    private var mDeviceEnabledVitalSigns: HealthMonitorEnabledVitalSigns? = null
    private var mLicenseEnabledVitalSigns: HealthMonitorEnabledVitalSigns? = null
    var heartRate = ""
    var saturation = ""
    var respiration = ""
    var sdnn = ""
    var stressLevel = ""
    var mPercentage = 0
    var licenseKey = ""
    var mIsRest = false
    var mIsFromStartNow = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(mBinding?.root)

        setSupportActionBar(toolbarhome)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        initializeUi()
        checkPermisson()

        val bundle = intent.extras
        bundle?.let {
            licenseKey = it.getString(KEY_LICENSE) as String
            mIsRest = it.getBoolean(KEY_REST_WALK) as Boolean
            mIsFromStartNow = it.getBoolean(KEY_FROM_START_NOW) as Boolean
        }
        settingsActivityCode("")

    }


    private fun checkPermisson() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                1
            )
        } else {
            createHealthMonitorManager()
            createSession()
        }
    }

    private fun settingsActivityCode(licenseKey: String) {
        val LICENSE_KEY = "929B1D-FEB149-430D86-A1D5B0-491F7F-0238B1"
        val preferences = getSharedPreferences("binah_sample", Context.MODE_PRIVATE)
        preferences.edit().putString("licenseKey", LICENSE_KEY).apply()
    }

    public override fun onResume() {
        super.onResume()
        createSession()
    }

    public override fun onPause() {
        super.onPause()
        closeSession()
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            createHealthMonitorManager()
            createSession()
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SETTINGS_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            createHealthMonitorManager()
            createSession()
        }
    }

    // HealthMonitorManager callbacks ============================================================
    override fun onHealthManagerReady() {
        //Deprecated
    }

    override fun onHealthManagerError(errorCode: Int, messageCode: Int) {
        showErrorDialog("Invalid License ($messageCode)")
        updateUi(UiState.IDLE)
    }

    override fun onSessionStateChange(
        session: HealthMonitorSession,
        state: SessionState
    ) {
        //Deprecated
    }

    @SuppressLint("SetTextI18n")
    override fun onHealthMonitorManagerInfo(infoCode: Int, info: Any) {
        if (infoCode == HealthMonitorInfoCodes.HEALTH_MONITOR_INFO_MEASUREMENT_COUNTING) {
            val offlineMeasurementsInfo =
                info as HealthMonitorLicenseInfoOfflineMeasurements
//            mBinding?.mainContent?.measurementsCount?.text =
//                offlineMeasurementsInfo.remainingMeasurements
//                    .toString() + "/" + offlineMeasurementsInfo.offlineMeasurements
            startMeasurementTimeout(offlineMeasurementsInfo.measurementEndTimestampInSeconds)
        } else if (infoCode == HealthMonitorInfoCodes.HEALTH_MONITOR_INFO_LICENSE_ENABLED_VITAL_SIGNS) {
            mLicenseEnabledVitalSigns = info as HealthMonitorEnabledVitalSigns
        }
    }

    // HealthMonitorListener callbacks ============================================================
    override fun onHealthMonitorMessage(messageType: Int, message: Any) {
        runOnUiThread { handleMonitorMessage(messageType, message) }
    }

    override fun onHealthMonitorError(error: Int) {
        runOnUiThread {
            handleMonitorError(error)
        }
    }

    override fun onNewImage(bitmap: Bitmap, fingerDetected: Boolean) {
        if (mTestMode == SessionMode.FINGER) {
            runOnUiThread {
                handleImage(bitmap, null)
                handleRoiDetection(fingerDetected)
            }
        }
    }

    override fun onNewImage(bitmap: Bitmap, faceRect: RectF?) {
        if (mTestMode == SessionMode.FACE) {
            runOnUiThread {
                handleImage(bitmap, faceRect)
                handleRoiDetection(faceRect != null)
            }
        }
    }

    override fun onStateChange(
        session: HealthMonitorSession,
        state: SessionState
    ) {
        runOnUiThread {
            if (state == SessionState.ACTIVE) {
                updateUi(UiState.IDLE)
            }
        }
    }

    fun startStopButtonClicked(view: View?) {
        if (mSession == null) {
            return
        }
        if (mSession?.state == SessionState.MEASURING) {
            stopMeasuring()
        } else if (mTestMode == SessionMode.FINGER && mDeviceEnabledVitalSigns == null) {
            //TODO
        } else {
            startMeasuring()
            mBinding?.mainContent?.playStopButton?.visibility = View.GONE
            mBinding?.mainContent?.materialTextView?.visibility = View.GONE
        }
    }

    fun changeModeClicked(view: View?) {
        try {
            closeSession()
            mTestMode = if (mTestMode == SessionMode.FACE) SessionMode.FINGER else SessionMode.FACE
            createSession()
        } catch (e: IllegalStateException) {
            showWarning(getString(R.string.change_session_mode_error))
        }
    }

    private fun handleMonitorMessage(messageType: Int, message: Any) {
        val measurementsLayout = mBinding?.mainContent?.measurementsLayout
        when (messageType) {
            HealthMonitorMessages.HEALTH_MONITOR_HEART_RATE -> {
                measurementsLayout?.heartRate?.imgMeasurementInActive?.visibility = View.GONE
                measurementsLayout?.heartRate?.value?.visibility = View.VISIBLE
                measurementsLayout?.heartRate?.imgMeasurementActive?.visibility =
                    View.VISIBLE
                measurementsLayout?.heartRate?.imgMeasurementActive?.setImageDrawable(
                    resources.getDrawable(
                        R.drawable.ic_heartrate_new
                    )
                )
                mBinding?.mainContent?.measurementsLayout?.heartRate?.value?.text =
                    formatValue(message as Double)
            }
            HealthMonitorMessages.HEALTH_MONITOR_OXYGEN_SATURATION -> {
                measurementsLayout?.saturation?.imgMeasurementInActive?.visibility = View.GONE
                measurementsLayout?.saturation?.value?.visibility = View.VISIBLE
                measurementsLayout?.saturation?.imgMeasurementActive?.visibility =
                    View.VISIBLE
                measurementsLayout?.saturation?.imgMeasurementActive?.setImageDrawable(
                    resources.getDrawable(
                        R.drawable.ic_o2_active
                    )
                )
                mBinding?.mainContent?.measurementsLayout?.saturation?.value?.text =
                    formatValue(message as Double)
            }
            HealthMonitorMessages.HEALTH_MONITOR_BREATHING_RATE -> {
                measurementsLayout?.respiration?.imgMeasurementInActive?.visibility = View.GONE
                measurementsLayout?.respiration?.value?.visibility = View.VISIBLE
                measurementsLayout?.respiration?.imgMeasurementActive?.visibility =
                    View.VISIBLE
                measurementsLayout?.respiration?.imgMeasurementActive?.setImageDrawable(
                    resources.getDrawable(
                        R.drawable.ic_reparation_new
                    )
                )
                mBinding?.mainContent?.measurementsLayout?.respiration?.value?.text =
                    formatValue(message as Double)
            }
            HealthMonitorMessages.HEALTH_MONITOR_SDNN -> {
                measurementsLayout?.sdnn?.imgMeasurementInActive?.visibility = View.GONE
                measurementsLayout?.sdnn?.value?.visibility = View.VISIBLE
                measurementsLayout?.sdnn?.imgMeasurementActive?.visibility =
                    View.VISIBLE
                measurementsLayout?.sdnn?.imgMeasurementActive?.setImageDrawable(
                    resources.getDrawable(
                        R.drawable.ic_hrv_active
                    )
                )
                measurementsLayout?.sdnn?.value?.text =
                    formatValue(message as Double)
            }
            HealthMonitorMessages.HEALTH_MONITOR_STRESS_LEVEL -> {
                measurementsLayout?.stressLevel?.imgMeasurementInActive?.visibility = View.GONE
                measurementsLayout?.stressLevel?.value?.visibility = View.VISIBLE
                measurementsLayout?.stressLevel?.imgMeasurementActive?.visibility =
                    View.VISIBLE
                measurementsLayout?.stressLevel?.imgMeasurementActive?.setImageDrawable(
                    resources.getDrawable(
                        R.drawable.ic_stress_active
                    )
                )
                val level = message as HealthMonitorStressLevel
                mBinding?.mainContent?.measurementsLayout?.stressLevel?.value?.text =
                    if (level == HealthMonitorStressLevel.UNKNOWN) "--" else level.name
            }
            HealthMonitorMessages.HEALTH_MONITOR_REPORT -> publishReport(
                message,
                message as HealthMonitorReport
            )
        }

    }

    private fun handleMonitorError(error: Int) {

        when (error) {
            HealthMonitorCodes.DEVICE_CODE_UNSUPPORTED_ORIENTATION_ERROR -> if (mSession != null && mSession?.state == SessionState.MEASURING) {
                showWarning(getString(R.string.orientation_not_supported))
            }
            HealthMonitorCodes.MEASUREMENT_CODE_MULTIPLE_INTERRUPTIONS_ERROR -> showErrorDialog(
                getString(if (mTestMode == SessionMode.FACE) R.string.face_reset_error else R.string.finger_reset_error)
            )
            HealthMonitorCodes.MEASUREMENT_CODE_MISSING_FRAMES_ERROR -> showErrorDialog(getString(if (mTestMode == SessionMode.FACE) R.string.frames_lost_face_error else R.string.frames_lost_finger_error))
            HealthMonitorCodes.MEASUREMENT_CODE_LICENSE_ACTIVATION_FAILED_ERROR -> {
                resetMeasurements()
                showErrorDialog(
                    getString(if (mTestMode == SessionMode.FACE) R.string.license_activation_failed else R.string.license_activation_failed)
                )
            }
            HealthMonitorCodes.MEASUREMENT_CODE_MULTIPLE_INTERRUPTIONS_WARNING -> {
                resetMeasurements()
                showWarning(getString(R.string.reset_warning))
            }
            HealthMonitorCodes.MEASUREMENT_CODE_MISSING_FRAMES_WARNING -> showWarning(getString(R.string.frames_lost_warning))
        }
    }

    private fun createHealthMonitorManager() {
        try {
            updateUi(UiState.LOADING)
            val licenseKey = getSharedPreferences(
                "binah_sample",
                Context.MODE_PRIVATE
            ).getString("licenseKey", "")
            mManager = HealthMonitorManager(this, LicenseData(licenseKey ?: ""), this)
        } catch (e: HealthMonitorException) {
            showErrorDialog("Error: (" + e.errorCode + ")")
        }
    }

    private fun createSession() {
        if (mManager == null || mSession != null) {
            return
        }
        updateUi(UiState.LOADING)
        val sessionTime = getSharedPreferences(
            "binah_sample",
            Context.MODE_PRIVATE
        ).getLong("sessionTime", 0)
        try {
            if (mTestMode == SessionMode.FINGER) {
                mSession = mManager?.createFingerSessionBuilder(baseContext)
                    ?.withListener(this@HomeActivity)
                    ?.withSessionStateListener(this@HomeActivity)
                    ?.withProcessingTime(sessionTime)
                    ?.build()
                mDeviceEnabledVitalSigns = HealthMonitorManager.getFingerEvaluationResult()
            } else {
                mSession = mManager?.createFaceSessionBuilder(baseContext)
                    ?.withListener(this@HomeActivity)
                    ?.withStateListener(this@HomeActivity)
                    ?.withProcessingTime(sessionTime)
                    ?.build()
            }
        } catch (e: ExceptionInInitializerError) {
            showWarning("Error creating session - $e")
        }
    }

    private fun closeSession() {
        stopTimeCount()
        if (mSession != null) {
            mSession?.terminate()
            mSession?.removeStateListener(this)
        }
        mSession = null
    }

    private fun startMeasuring() {
        try {
            mSession?.start()
            updateUi(UiState.MEASURING)
        } catch (e: HealthMonitorException) {
            handleStartSessionException(e)
        } catch (e: IllegalStateException) {
            showWarning("Start Error - Session in illegal state")
        } catch (e: NullPointerException) {
            showWarning("Start Error - Session not initialized")
        }
    }

    private fun handleStartSessionException(e: HealthMonitorException) {
        when (e.errorCode) {
            HealthMonitorCodes.DEVICE_CODE_MINIMUM_BATTERY_LEVEL_WARNING -> showErrorDialog(
                getString(R.string.low_battery_error)
            )
            HealthMonitorCodes.DEVICE_CODE_LOW_POWER_MODE_ENABLED_ERROR -> showErrorDialog(
                getString(
                    R.string.power_save_error
                )
            )
            else -> showErrorDialog(getString(R.string.cannot_start_session))
        }
    }

    private fun stopMeasuring() {
        try {
            mSession?.stop()
        } catch (e: IllegalStateException) {
            showWarning("Stop Error - Session in illegal state")
        } catch (ignore: NullPointerException) {
        }
    }

    private fun initializeUi() {
        val measurementsLayout =
            mBinding?.mainContent?.measurementsLayout
        measurementsLayout?.heartRate?.title?.setText(R.string.heart_rate)
        measurementsLayout?.saturation?.title?.setText(R.string.saturation)
        measurementsLayout?.respiration?.title?.setText(R.string.respiration)
        measurementsLayout?.sdnn?.title?.setText(R.string.hrv_sdnn)
        measurementsLayout?.stressLevel?.title?.setText(R.string.stress)

        // set visibility gone
        measurementsLayout?.heartRate?.imgMeasurementActive?.visibility = View.GONE
        measurementsLayout?.saturation?.imgMeasurementActive?.visibility = View.GONE
        measurementsLayout?.respiration?.imgMeasurementActive?.visibility = View.GONE
        measurementsLayout?.sdnn?.imgMeasurementActive?.visibility = View.GONE
        measurementsLayout?.stressLevel?.imgMeasurementActive?.visibility = View.GONE

        measurementsLayout?.heartRate?.imgMeasurementInActive?.visibility = View.VISIBLE
        measurementsLayout?.saturation?.imgMeasurementInActive?.visibility = View.VISIBLE
        measurementsLayout?.respiration?.imgMeasurementInActive?.visibility = View.VISIBLE
        measurementsLayout?.sdnn?.imgMeasurementInActive?.visibility = View.VISIBLE
        measurementsLayout?.stressLevel?.imgMeasurementInActive?.visibility = View.VISIBLE
        measurementsLayout?.heartRate?.value?.visibility = View.GONE
        measurementsLayout?.saturation?.value?.visibility = View.GONE
        measurementsLayout?.respiration?.value?.visibility = View.GONE
        measurementsLayout?.sdnn?.value?.visibility = View.GONE
        measurementsLayout?.stressLevel?.value?.visibility = View.GONE

        measurementsLayout?.heartRate?.imgMeasurementInActive?.setImageDrawable(
            resources.getDrawable(
                R.drawable.ic_heart
            )
        )
        measurementsLayout?.saturation?.imgMeasurementInActive?.setImageDrawable(
            resources.getDrawable(
                R.drawable.ic_o2
            )
        )
        measurementsLayout?.respiration?.imgMeasurementInActive?.setImageDrawable(
            resources.getDrawable(
                R.drawable.ic_reparation
            )
        )
        measurementsLayout?.sdnn?.imgMeasurementInActive?.setImageDrawable(resources.getDrawable(R.drawable.ic_hrv_inactive))
        measurementsLayout?.stressLevel?.imgMeasurementInActive?.setImageDrawable(
            resources.getDrawable(
                R.drawable.ic_ioc_stress
            )
        )


//        mBinding?.settingsButton?.setOnClickListener {
//            startActivityForResult(
//                Intent(this@MainActivity, SettingsActivity::class.java),
//                SETTINGS_ACTIVITY_REQUEST_CODE
//            )
//            mManager = null
//        }
        resetTimeCount()
    }

    fun updateUi(state: UiState?) {
        when (state) {
            UiState.LOADING -> {
                mBinding?.mainContent?.cameraView?.visibility = View.INVISIBLE
                mBinding?.loadingText?.visibility = View.VISIBLE
                mBinding?.mainContent?.roiWarning?.visibility = View.INVISIBLE
                mBinding?.measurementModeButton?.visibility = View.INVISIBLE
                stopTimeCount()
                resetTimeCount()
            }
            UiState.MEASURING -> {
                mBinding?.loadingText?.visibility = View.INVISIBLE
                setupResultsUi()
                resetMeasurements()
                startTimeCount()
//                mBinding?.mainContent?.playStopButton?.setBackgroundResource(R.drawable.ic_stop_button)
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
//                mBinding?.mainContent?.licenseInfoLayout?.visibility = View.INVISIBLE
                mBinding?.mainContent?.measurementsLayout?.root?.visibility = View.VISIBLE
                mBinding?.mainContent?.placeFingerHint?.visibility = View.INVISIBLE
                mBinding?.measurementModeButton?.visibility = View.INVISIBLE
            }
            UiState.IDLE -> {
                mBinding?.loadingText?.visibility = View.INVISIBLE
                if (mBinding?.mainContent?.cameraView?.visibility == View.INVISIBLE) {
                    mBinding?.mainContent?.cameraView?.visibility = View.VISIBLE
                    clearCanvas()
                }
//                mBinding?.mainContent?.playStopButton?.isEnabled = true
//                mBinding?.mainContent?.playStopButton?.setBackgroundResource(R.drawable.ic_play_scan)
//                mBinding?.mainContent?.licenseInfoLayout?.visibility = View.INVISIBLE
                mBinding?.mainContent?.roiWarning?.visibility = View.INVISIBLE
                mBinding?.mainContent?.placeFingerHint?.visibility =
                    if (mTestMode == SessionMode.FINGER) View.VISIBLE else View.INVISIBLE
                mBinding?.measurementModeButton?.visibility = View.GONE
                stopTimeCount()
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }
    }

    private fun startTimeCount() {
        mBinding?.mainContent?.grpProgress?.visibility = View.VISIBLE
        if (mTimeCountHandler != null) {
            mTimeCountHandler?.removeCallbacksAndMessages(null)
        }
        mTime = 0
        mTimeCountHandler = Handler(Looper.getMainLooper())
        mTimeCountHandler?.post(object : Runnable {
            override fun run() {
                mTime++
//                mBinding?.mainContent?.timeTextView?.text = String.format(
//                    getString(R.string.seconds_count),
//                    formatTimer(mTime++.toLong())
//                )
                setProgressBar(mTime)
                mTimeCountHandler?.postDelayed(this, 1000)
            }
        })
    }

    fun setProgressBar(mTime: Int) {

        val max_progress = 100
        mBinding?.mainContent?.progressBar?.setProgressTintList(ColorStateList.valueOf(Color.BLUE));
        mBinding?.mainContent?.progressBar?.progress = 0
        mBinding?.mainContent?.progressBar?.max = max_progress
        mBinding?.mainContent?.progressBar?.progress = mTime
        val percentage = ((mTime.toDouble() / max_progress) * 100).toInt()
        mBinding?.mainContent?.txtProgress?.text = "$percentage%"
        mPercentage = percentage

        if (percentage == 100) {
            if (mSession == null) {
                return
            }
            if (mSession?.state == SessionState.MEASURING) {
                stopMeasuring()
            }
        }
    }

    private fun stopTimeCount() {
//        mPercentage = 0
//        resetMeasurements()
        mBinding?.mainContent?.grpProgress?.visibility = View.GONE
        if (mTimeCountHandler != null) {
            mTimeCountHandler?.removeCallbacksAndMessages(null)
        }
    }

    private fun resetTimeCount() {
        mBinding?.mainContent?.grpProgress?.visibility = View.GONE
//        resetMeasurements()
        mTime = 0
        mPercentage = 0
        mBinding?.mainContent?.txtProgress?.text = "$0%"
//        mBinding?.mainContent?.timeTextView?.text = String.format(
//            getString(R.string.seconds_count),
//            formatTimer(mTime.toLong())
//        )
    }

    private fun handleRoiDetection(detected: Boolean) {
        if (mSession == null || mSession?.state != SessionState.MEASURING) {
            return
        }
        if (detected) {
            mBinding?.mainContent?.roiWarning?.visibility = View.INVISIBLE
            mBinding?.mainContent?.measurementsLayout?.root?.visibility = View.VISIBLE
        } else {
            mBinding?.mainContent?.roiWarning?.visibility = View.VISIBLE
            mBinding?.mainContent?.measurementsLayout?.root?.visibility = View.INVISIBLE
            mBinding?.mainContent?.roiWarningText?.text =
                getString(if (mTestMode == SessionMode.FACE) R.string.no_face_detected else R.string.no_finger_detected)
        }
    }

    private fun setupResultsUi() {
        val enabledVitalSigns = resolveEnabledVitalSigns() ?: return
        val measurementsBinding =
            mBinding?.mainContent?.measurementsLayout
        measurementsBinding?.heartRate?.root?.visibility =
            if (enabledVitalSigns.isHeartRateEnabled) View.VISIBLE else View.GONE
        measurementsBinding?.saturation?.root?.visibility =
            if (enabledVitalSigns.isOxygenSaturationEnabled) View.VISIBLE else View.GONE
        measurementsBinding?.respiration?.root?.visibility =
            if (enabledVitalSigns.isBreathingRateEnabled) View.VISIBLE else View.GONE
        measurementsBinding?.sdnn?.root?.visibility =
            if (enabledVitalSigns.isSdnnEnabled) View.VISIBLE else View.GONE
        measurementsBinding?.stressLevel?.root?.visibility =
            if (enabledVitalSigns.isStressLevelEnabled) View.VISIBLE else View.GONE
    }

    private fun resetMeasurements() {
        val measurementsBinding =
            mBinding?.mainContent?.measurementsLayout
        measurementsBinding?.heartRate?.value?.text = "--"
        measurementsBinding?.saturation?.value?.text = "--"
        measurementsBinding?.respiration?.value?.text = "--"
        measurementsBinding?.sdnn?.value?.text = "--"
        measurementsBinding?.stressLevel?.value?.text = "--"
    }

    private fun publishReport(message: Any, report: HealthMonitorReport) {
        val measurementsBinding =
            mBinding?.mainContent?.measurementsLayout
        measurementsBinding?.heartRate?.value?.text = formatValue(report.heartRate)
        measurementsBinding?.saturation?.value?.text = formatValue(report.oxygenSaturation)
        measurementsBinding?.respiration?.value?.text = formatValue(report.breathingRate)

        if (formatValue(report.sdnn) != "0") {
            measurementsBinding?.sdnn?.imgMeasurementInActive?.visibility = View.GONE
            measurementsBinding?.sdnn?.value?.visibility = View.VISIBLE
            measurementsBinding?.sdnn?.imgMeasurementActive?.visibility =
                View.VISIBLE
            measurementsBinding?.sdnn?.imgMeasurementActive?.setImageDrawable(
                resources.getDrawable(
                    R.drawable.ic_hrv_active
                )
            )
            measurementsBinding?.sdnn?.value?.text = formatValue(report.sdnn)
//            measurementsBinding?.sdnn?.value?.text =
//                formatValue(message as Double)
        }
        if (report.stressLevel.ordinal.toString() != "0") {
            measurementsBinding?.stressLevel?.imgMeasurementInActive?.visibility = View.GONE
            measurementsBinding?.stressLevel?.value?.visibility = View.VISIBLE
            measurementsBinding?.stressLevel?.imgMeasurementActive?.visibility =
                View.VISIBLE
            measurementsBinding?.stressLevel?.imgMeasurementActive?.setImageDrawable(
                resources.getDrawable(
                    R.drawable.ic_stress_active
                )
            )
            measurementsBinding?.stressLevel?.value?.text =
                report.stressLevel.ordinal.toString().replace("0", "--")
//            val level = message as HealthMonitorStressLevel
//            mBinding?.mainContent?.measurementsLayout?.stressLevel?.value?.text =
//                if (level == HealthMonitorStressLevel.UNKNOWN) "--" else level.name
        }
        updateUi(UiState.IDLE)
        setHeartRateValue(report)
    }

    private fun setHeartRateValue(report: HealthMonitorReport) {
        heartRate = formatValue(report.heartRate)
        saturation = formatValue(report.oxygenSaturation)
        respiration = formatValue(report.breathingRate)
        sdnn = formatValue(report.sdnn)
        stressLevel = report.stressLevel.ordinal.toString().replace("0", "--")

        if (heartRate != "0" && saturation != "0" && respiration != "0" && sdnn != "0" && sdnn != "--" && stressLevel != "0" && stressLevel != "--") {
            Handler(Looper.getMainLooper()).postDelayed({
                if (!mIsFromStartNow) {
                    saveHealthData("", false)
                } else {
                    checkRiskAssesmentApi()
                }
            }, 1500)
        } else {
            showErrorDialog("Cannot read some value. Please try again. Try to be on more light")
        }

    }

    private fun checkRiskAssesmentApi() {

      /*  val checkRiskRequest = CheckRiskRequest()
        checkRiskRequest.heartRate = heartRate.toInt()
        checkRiskRequest.spo2 = saturation.toInt()
        checkRiskRequest.hrv = sdnn.toInt()
        checkRiskRequest.stress = stressLevel.toInt()
        checkRiskRequest.lossOfTaste = false
        checkRiskRequest.respiration = respiration.toInt()

        mHealthService?.getRiskAssesmentPossibility(checkRiskRequest)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                saveStartNowHealthData(it.covidPossibility)
                hideProgress()
                HealthApplication.showToast("Data saved successfully")
            }, {
                onHealthDataError(it)
                HealthApplication.showToast("Server Error")
                hideProgress()
            })*/
    }

    private fun formatValue(value: Double?): String {
        if (value == null || value == 0.0) {
            return "--"
        }
        return if (value < 1.0) String.format(
            Locale.US,
            "%.2f",
            value
        ) else String.format(
            Locale.US,
            "%" + (Math.log10(value).toInt() + 1) + ".0f",
            value
        )
    }

    private fun showErrorDialog(message: String) {
        try {
            if (mMessageDialog != null && mMessageDialog?.isShowing!!) {
                return
            }
            mMessageDialog = AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.ok, object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        finish()
                    }
                })
                .show()
        } catch (e: Exception) {

        }
    }

    private fun showWarning(text: String) {
        if (mBinding?.mainContent?.warningLayout?.visibility == View.VISIBLE) {
            return
        }
        if (mWarningDialogTimeoutHandler != null) {
            mWarningDialogTimeoutHandler?.removeCallbacksAndMessages(null)
        }
        mBinding?.mainContent?.warningMessage?.text = text
        mBinding?.mainContent?.warningLayout?.visibility = View.VISIBLE
        mWarningDialogTimeoutHandler = Handler(mainLooper)
        mWarningDialogTimeoutHandler?.postDelayed({
            mBinding?.mainContent?.warningLayout?.visibility = View.INVISIBLE
        }, 5000)
    }

    private fun startMeasurementTimeout(endTimestamp: Long) {
        if (mMeasurementTimeHandler != null) {
            mMeasurementTimeHandler?.removeCallbacksAndMessages(null)
        }
        mMeasurementEndTime =
            Math.max(endTimestamp - Date().time / 1000, 0)
        if (mMeasurementEndTime == 0L) {
            return
        }
//        mBinding?.mainContent?.measurementTimeout?.text = formatTimer(mMeasurementEndTime)
        mMeasurementTimeHandler = Handler(mainLooper)
        mMeasurementTimeHandler?.postDelayed(object : Runnable {
            override fun run() {
//                mBinding?.mainContent?.measurementTimeout?.text = formatTimer(--mMeasurementEndTime)
                if (mMeasurementEndTime <= 0) {
                    return
                }
                mMeasurementTimeHandler?.postDelayed(this, 1000)
            }
        }, 1000)
    }

    private fun formatTimer(seconds: Long): String {
        return String.format(Locale.US, "%02d", seconds / 60 % 60) +
                ":" + String.format(Locale.US, "%02d", seconds % 60)
    }

    private fun handleImage(bitmap: Bitmap?, faceRect: RectF?) {
        if (bitmap == null) {
            return
        }
        val canvas = mBinding?.mainContent?.cameraView?.lockCanvas()
        if (canvas != null) {
            if (mTestMode == SessionMode.FACE) {
                flipCanvas(canvas)
            }
            mBinding?.mainContent?.cameraView?.let {
                canvas.drawBitmap(
                    bitmap,
                    null,
                    Rect(
                        0,
                        0,
                        it.width,
                        it.bottom - it.top
                    ),
                    null
                )
            }

            if (faceRect != null) {
                paintRect(canvas, rescaleFaceRect(bitmap, faceRect))
            }
            mBinding?.mainContent?.cameraView?.unlockCanvasAndPost(canvas)
        }
    }

    private fun flipCanvas(canvas: Canvas) {
        val matrix = Matrix()
        matrix.preScale(-1f, 1f)
        matrix.postTranslate(canvas.width.toFloat(), 0f)
        canvas.setMatrix(matrix)
    }

    private fun paintRect(canvas: Canvas, faceRect: RectF) {
        val paint =
            Paint(Paint.DITHER_FLAG)
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 12f
        paint.color = resources.getColor(R.color.white, null)
        paint.strokeJoin = Paint.Join.MITER
        val path = Path()
        path.addRect(faceRect, Path.Direction.CW)
        canvas.drawPath(path, paint)
    }

    private fun rescaleFaceRect(bitmap: Bitmap, faceRect: RectF): RectF {
        val rect = RectF(faceRect)
        val width = bitmap.width.toFloat()
        val height = bitmap.height.toFloat()
        val m = Matrix()
        m.postScale(1f, 1f, width / 2f, height / 2f)
        m.postScale(
            mBinding?.mainContent!!.cameraView.width / width,
            mBinding?.mainContent!!.cameraView.height / height
        )
        m.mapRect(rect)
        return rect
    }

    private fun clearCanvas() {
        val canvas = mBinding?.mainContent?.cameraView?.lockCanvas()
        canvas?.drawColor(ContextCompat.getColor(baseContext, R.color.white))
        canvas?.let {
            mBinding?.mainContent?.cameraView?.unlockCanvasAndPost(canvas)
        }
    }

    private fun resolveEnabledVitalSigns(): HealthMonitorEnabledVitalSigns? {
        if (mDeviceEnabledVitalSigns == null) {
            return null
        }
        return if (mLicenseEnabledVitalSigns == null) {
            mDeviceEnabledVitalSigns
        } else HealthMonitorEnabledVitalSigns(
            mDeviceEnabledVitalSigns?.isHeartRateEnabled ?: false && mLicenseEnabledVitalSigns?.isHeartRateEnabled ?: false,
            mDeviceEnabledVitalSigns?.isBreathingRateEnabled ?: false && mLicenseEnabledVitalSigns?.isBreathingRateEnabled ?: false,
            mDeviceEnabledVitalSigns?.isOxygenSaturationEnabled ?: false && mLicenseEnabledVitalSigns?.isOxygenSaturationEnabled ?: false,
            mDeviceEnabledVitalSigns?.isSdnnEnabled ?: false && mLicenseEnabledVitalSigns?.isSdnnEnabled ?: false,
            mDeviceEnabledVitalSigns?.isStressLevelEnabled ?: false && mLicenseEnabledVitalSigns?.isStressLevelEnabled ?: false
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun saveHealthData(temparature: String, lossOfTaste: Boolean) {
        showProgress(this)
        try {
            val healthDataRequest = HealthDataRequest()
            healthDataRequest.userId =
                SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
            healthDataRequest.userEmail =
                SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL) ?: ""
            healthDataRequest.trackDate = DateFormatUtils.getCurrentDate()

            val heartValue = heartRate ?: "0"
            val respiration = respiration ?: "0"
            val stressLevel = stressLevel ?: "0"
            val saturation = saturation ?: "0"
            val sdnn = sdnn ?: "0"

            healthDataRequest.heartRate = heartValue.toInt()
            healthDataRequest.hrv = sdnn.toInt()
            healthDataRequest.respiration = respiration.toInt()
            healthDataRequest.spo2 = saturation.toInt()
            healthDataRequest.stress = stressLevel.toInt()
            healthDataRequest.temperature = 102
//            healthDataRequest.lossOfTaste = lossOfTaste
            healthDataRequest.isRest = mIsRest

            mHealthService?.postHealthData(healthDataRequest)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    onHealthDataSuccess(it)
                    hideProgress()
                    HealthApplication.showToast("Data saved successfully")
                }, {
                    onHealthDataError(it)
                    HealthApplication.showToast("Server Error")
                    hideProgress()
                })
        } catch (e: Exception) {
            e.printStackTrace()
            hideProgress()
        }
    }

    fun saveStartNowHealthData(covidPossibility: String?) {
        showProgress(this)
        try {
            val healthDataRequest = HealthDataRequest()
            healthDataRequest.userId =
                SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
            healthDataRequest.userEmail =
                SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL) ?: ""
            healthDataRequest.trackDate = DateFormatUtils.getCurrentDate()

            val heartValue = heartRate ?: "0"
            val respiration = respiration ?: "0"
            val stressLevel = stressLevel ?: "0"
            val saturation = saturation ?: "0"
            val sdnn = sdnn ?: "0"

            healthDataRequest.heartRate = heartValue.toInt()
            healthDataRequest.hrv = sdnn.toInt()
            healthDataRequest.respiration = respiration.toInt()
            healthDataRequest.spo2 = saturation.toInt()
            healthDataRequest.stress = stressLevel.toInt()
            healthDataRequest.isRest = mIsRest
            healthDataRequest.covidPossibility = covidPossibility ?: ""
            Navigator.navigateToRiskAssessment(this, healthDataRequest)
            finish()

        } catch (e: Exception) {
            e.printStackTrace()
            hideProgress()
        }
    }

    private fun onHealthDataSuccess(it: HealthDataResponse?) {

        val response = GetAllDashboardHealthPlanResponse()
        response.heartRate = it?.heartRate ?: 0
        response.spo2 = it?.spo2 ?: 0
        response.respiration = it?.respiration ?: 0
        response.stress = it?.stress ?: 0
        response.hrv = it?.hrv ?: 0
        response.isRest = it?.isRestOrWalk ?: false
        Navigator.navigateToSummary(this, response)
        finish()

    }

    private fun onHealthDataError(it: Throwable?) {
        HealthApplication.showToast("Server Error")
    }


    companion object {
        val KEY_REST_WALK = "keyRestOrWalk"
        val KEY_LICENSE = "keyLicense"
        val KEY_FROM_START_NOW = "keyFromStartNow"

        fun getCallingIntent(
            context: Context?,
            isRestOrWalk: Boolean,
            licenseKey: String,
            isFromStartNow: Boolean
        ): Intent {
            val intent = Intent(context, HomeActivity::class.java)
            intent.putExtra(KEY_REST_WALK, isRestOrWalk)
            intent.putExtra(KEY_LICENSE, licenseKey)
            intent.putExtra(KEY_FROM_START_NOW, isFromStartNow)
            return intent
        }
    }
}