package com.obhaiyya.health.presentation.paygateway;

import android.os.Parcel;
import android.os.Parcelable;


public class UserModel implements Parcelable {

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
    private String appartment;
    private String mobileNumber;
    private String flatNumber;
    private String name;
    private String block;
    private String email;
    private String appartmentId;
    private String[] vendorId;
    private String _id;
    private String bhk;
    private Integer bathrooms;
    private String notificationToken;
    private String lat;
    private String lng;
    private String mobileOTP;
    private String authToken;

    public UserModel() {

    }

    protected UserModel(Parcel in) {
        appartment = in.readString();
        mobileNumber = in.readString();
        flatNumber = in.readString();
        name = in.readString();
        block = in.readString();
        email = in.readString();
        appartmentId = in.readString();
        vendorId = in.createStringArray();
        _id = in.readString();
        notificationToken = in.readString();
        bhk = in.readString();
        bathrooms = in.readInt();
        lat = in.readString();
        lng = in.readString();
        mobileOTP = in.readString();
        authToken = in.readString();
    }

    public String getAppartment() {
        return appartment;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }

    public String get_id() {
        return _id;
    }

    public String getUserApartment() {
        return appartment;
    }

    public String getUserMobileNumber() {
        return mobileNumber;
    }

    public String getUserFlatNumber() {
        return flatNumber;
    }

    public String getUserName() {
        return name;
    }

    public String getUserBlock() {
        return block;
    }

    public String getUserEmail() {
        return email;
    }

    public String getAppartmentId() {
        return appartmentId;
    }

    public String getBhk() {
        return bhk;
    }

    public Integer getBathrooms() {
        return bathrooms;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String[] getVendorId() {
        return vendorId;
    }

    public void setAppartment(String appartment) {
        this.appartment = appartment;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAppartmentId(String appartmentId) {
        this.appartmentId = appartmentId;
    }

    public void setVendorId(String[] vendorId) {
        this.vendorId = vendorId;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setBhk(String bhk) {
        this.bhk = bhk;
    }

    public void setBathrooms(Integer bathrooms) {
        this.bathrooms = bathrooms;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getMobileOTP() {
        return mobileOTP;
    }

    public void setMobileOTP(String mobileOTP) {
        this.mobileOTP = mobileOTP;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(appartment);
        dest.writeString(mobileNumber);
        dest.writeString(flatNumber);
        dest.writeString(name);
        dest.writeString(block);
        dest.writeString(email);
        dest.writeString(appartmentId);
        dest.writeStringArray(vendorId);
        dest.writeString(_id);
        dest.writeString(notificationToken);
        dest.writeString(bhk);
        dest.writeInt(bathrooms);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(mobileOTP);
        dest.writeString(authToken);
    }
}
