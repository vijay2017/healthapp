package com.obhaiyya.health.presentation.onboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import androidx.viewpager2.widget.ViewPager2.SCROLL_STATE_DRAGGING
import com.appwithmeflutter.mype.data.network.HealthService
import com.example.obhaiyyahealth.dataProvider.ApiService
import com.obhaiyya.health.HealthApplication
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.utils.Navigator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_onboard_question.*
import kotlinx.android.synthetic.main.activity_walkthrough.btn_next


/**
 * Created by Vijay on 7/3/20.
 */

class OnboardQuestionActivity : FragmentActivity(), View.OnClickListener {

    var mHealthService: HealthService? = null
    var mHashMapAnswer = HashMap<Int, UsersAnswerRequest>()
    var isFromRegularFlow = false
    var mPosition = 0

    companion object {

        val KEY_ANSWER = "Yes"
        val KEY_NO = "No"
        val KEY_IS_FROM_REGULAR_FLOW = "isFromRegularFlow"

        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, OnboardQuestionActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboard_question)
        mHealthService = ApiService.getInstance()?.call()

        btn_next.setOnClickListener(this)
        observeOnboardQuestion()
    }

    fun setViewPagerAdapter(mListResponse: List<OnboardQuestionResponse>) {
        val adapter = OnboardQuestionAdapter(this, mListResponse)
        vp_onboard_question.adapter = adapter

        view_dot.setViewPager(vp_onboard_question)

        vp_onboard_question.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)

            }

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0, 1 -> {
//                        btn_next.visibility = View.GONE
//                        btn_skip.setVisibility(View.VISIBLE);
                    }
                    2 -> {
//                        btn_next.visibility = View.VISIBLE
//                        btn_skip.setVisibility(View.GONE);
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
                vp_onboard_question.isUserInputEnabled =
                        !(state == SCROLL_STATE_DRAGGING && vp_onboard_question.currentItem == 0)

            }
        })
    }

    private fun observeOnboardQuestion() {
        mHealthService?.getOnboardQuestion()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    if (!it.isEmpty()) {
                        setViewPagerAdapter(it)
                    }
                }, {

                })
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_next -> {
                nextFragment(mPosition)
            }
            R.id.btn_skip -> {
//                Navigator.navigateToUserDetails(this)
//                finish()
            }
        }
    }

    fun saveAnswer(position: Int, req: UsersAnswerRequest) {
        mHashMapAnswer.put(position, req)
        Log.d("VIJAY----saveAnswer", "" + position)
    }

    fun showBtnVisibility(position: Int) {
        mPosition = position
        btn_next.visibility = View.VISIBLE
    }

    fun hideBtnVisibility() {
        btn_next.visibility = View.GONE
    }

    fun nextFragment(position: Int) {
        when (position) {
            1 -> {
                vp_onboard_question.setCurrentItem(position, true)
            }
            2 -> {
                vp_onboard_question.setCurrentItem(position, true)
            }
            3 -> {
                submit()
            }
        }
    }


    fun submit() {
        saveUsersQuestionAnswer()
    }

    private fun saveUsersQuestionAnswer() {
        val mList = ArrayList<UsersAnswerRequest>()
        mHashMapAnswer.entries.map {
            mList.add(it.value)
        }
        mHealthService?.postUsersAnswer(mList)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    Navigator.navigateToDashboard(this)
                    SharedPreferenceManager.saveBoolean(
                            SharedPreferenceManager.KEY_SHOULD_SHOW_ONBOARD,
                            true
                    )
                }, {
                    HealthApplication.showToast("Server Error")
                })
    }


}