package com.obhaiyya.health.presentation.utils

import android.content.Context
import com.obhaiyya.health.presentation.userdetails.UserDetailsActivity
import com.obhaiyya.health.presentation.dashboard.DashboardActivity
import com.obhaiyya.health.presentation.dashboard.GetAllDashboardHealthPlanResponse
import com.obhaiyya.health.presentation.scanner.HomeActivity
import com.obhaiyya.health.presentation.login.LoginActivity
import com.obhaiyya.health.presentation.history.HistoryActivity
import com.obhaiyya.health.presentation.login.ValidateOTPActivity
import com.obhaiyya.health.presentation.navigation.InformationActivity
import com.obhaiyya.health.presentation.navigation.NavPackageActivity
import com.obhaiyya.health.presentation.navigation.ProfileHamburgerActivity
import com.obhaiyya.health.presentation.onboard.OnboardQuestionActivity
import com.obhaiyya.health.presentation.posthealthquestion.RiskAssesmentActivity
import com.obhaiyya.health.presentation.scanner.HealthDataRequest
import com.obhaiyya.health.presentation.splash.SplashActivity
import com.obhaiyya.health.presentation.Package.PackageActivity
import com.obhaiyya.health.presentation.summary.SummaryActivity
import com.obhaiyya.health.presentation.userdetails.DisclaimerActivity
import com.obhaiyya.health.presentation.walkthrough.WalkthroughActivity

object Navigator {

    fun navigateToLogin(context: Context) {
        context.startActivity(LoginActivity.getCallingIntent(context))
    }

    fun navigateToDashboard(context: Context) {
        context.startActivity(DashboardActivity.getCallingIntent(context))
    }

    fun navigateToUserDetails(context: Context) {
        context.startActivity(UserDetailsActivity.getCallingIntent(context))
    }

    fun navigateToWalkThrough(context: Context) {
        context.startActivity(WalkthroughActivity.getCallingIntent(context))
    }

    fun navigateToSplash(context: Context) {
        context.startActivity(SplashActivity.getCallingIntent(context))
    }

    fun navigateToPackage(context: Context, isFromHome: Boolean) {
        context.startActivity(PackageActivity.getCallingIntent(context, isFromHome))
    }

    fun navigateToOnboardQuestion(context: Context) {
        context.startActivity(OnboardQuestionActivity.getCallingIntent(context))
    }


    fun navigateToNavPackage(context: Context) {
        context.startActivity(NavPackageActivity.getCallingIntent(context))
    }

    fun navigateToMain(
        context: Context,
        isRestOrWalk: Boolean,
        licenseKey: String,
        isFromStartNow: Boolean
    ) {
        context.startActivity(
            HomeActivity.getCallingIntent(
                context,
                isRestOrWalk,
                licenseKey,
                isFromStartNow
            )
        )
    }

    fun navigateToSummary(context: Context, response: GetAllDashboardHealthPlanResponse) {
        context.startActivity(SummaryActivity.getCallingIntent(context, response))
    }

    fun navigateToProfile(context: Context) {
        context.startActivity(ProfileHamburgerActivity.getCallingIntent(context))
    }

    fun navigateToDisclaimer(context: Context) {
        context.startActivity(DisclaimerActivity.getCallingIntent(context))
    }

    fun navigateToRiskAssessment(context: Context, healthDataRequest: HealthDataRequest) {
        context.startActivity(RiskAssesmentActivity.getCallingIntent(context, healthDataRequest))
    }

    fun navigateToHistory(context: Context) {
        context.startActivity(HistoryActivity.getCallingIntent(context))
    }

    fun navigateToValidateOTP(context: Context, mobileNumber: String) {
        context.startActivity(ValidateOTPActivity.getCallingIntent(context, mobileNumber))
    }

    fun navigateToInformation(context: Context) {
        context.startActivity(InformationActivity.getCallingIntent(context))
    }

}