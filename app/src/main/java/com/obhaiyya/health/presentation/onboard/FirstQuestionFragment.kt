package com.obhaiyya.health.presentation.onboard

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_first_question.*
import kotlinx.android.synthetic.main.fragment_third_question.*

class FirstQuestionFragment : BaseFragment(), RadioGroup.OnCheckedChangeListener,
        View.OnClickListener {

    var firstResponse: OnboardQuestionResponse? = null
    var secondResponse: OnboardQuestionResponse? = null
    var thirdResponse: OnboardQuestionResponse? = null


    companion object {

        val KEY_QUESTION_FIRST = "KEY_QUESTION_FIRST"
        val KEY_QUESTION_SECOND = "KEY_QUESTION_SECOND"
        val KEY_QUESTION_THIRD = "KEY_QUESTION_THIRD"

        fun newInstance(
                mListResponse: OnboardQuestionResponse,
                secondQues: OnboardQuestionResponse,
                thirdQues: OnboardQuestionResponse
        ): FirstQuestionFragment {
            val firstQuestionFragment = FirstQuestionFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_QUESTION_FIRST, mListResponse)
            bundle.putSerializable(KEY_QUESTION_SECOND, secondQues)
            bundle.putSerializable(KEY_QUESTION_THIRD, thirdQues)
            firstQuestionFragment.arguments = bundle
            return firstQuestionFragment
        }

    }

    override fun getLayout(): Int {
        return R.layout.fragment_first_question
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firstResponse = arguments?.getSerializable(KEY_QUESTION_FIRST) as OnboardQuestionResponse
        secondResponse = arguments?.getSerializable(KEY_QUESTION_SECOND) as OnboardQuestionResponse
        thirdResponse = arguments?.getSerializable(KEY_QUESTION_THIRD) as OnboardQuestionResponse

        txtFirstQuestion.text = firstResponse?.question
        txtSecondQuestion.text = secondResponse?.question
        txtThirdQuestion.text = thirdResponse?.question

        rdGrpFirst.setOnCheckedChangeListener(this)
        rdGrpSecond.setOnCheckedChangeListener(this)
        rdGrpThird.setOnCheckedChangeListener(this)
        btnFirstFrag.setOnClickListener(this)

    }


    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtnFirstYes -> {
                checkAnswer(1, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtnFirstNo -> {
                checkAnswer(1, OnboardQuestionActivity.KEY_NO)
            }
            R.id.rdBtnSecondYes -> {
                checkAnswer(2, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtnSecondNo -> {
                checkAnswer(2, OnboardQuestionActivity.KEY_NO)
            }
            R.id.rdBtnThirdYes -> {
                checkAnswer(3, OnboardQuestionActivity.KEY_ANSWER)
            }
            R.id.rdBtnThirdNo -> {
                checkAnswer(3, OnboardQuestionActivity.KEY_NO)
            }
        }
    }

    fun checkAnswer(pos: Int, ans: String) {
        when (pos) {
            1 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId =
                        SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
                ansReq.answer = ans
                ansReq.question = firstResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(1, ansReq)
                checkAllAnswered()
            }
            2 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId =
                        SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
                ansReq.answer = ans
                ansReq.question = secondResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(2, ansReq)
                checkAllAnswered()
            }
            3 -> {
                val ansReq = UsersAnswerRequest()
                ansReq.userId =
                        SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
                ansReq.answer = ans
                ansReq.question = thirdResponse?.question ?: ""
                (activity as OnboardQuestionActivity).saveAnswer(3, ansReq)
                checkAllAnswered()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnFirstFrag -> {

            }
        }
    }

    fun checkAllAnswered() {
        val answer1 = rdBtnFirstYes.isChecked || rdBtnFirstNo.isChecked
        val answer2 = rdBtnSecondYes.isChecked || rdBtnSecondNo.isChecked
        val answer3 = rdBtnThirdYes.isChecked || rdBtnThirdNo.isChecked

        if (answer1 && answer2 && answer3) {
            btnFirstFrag.visibility = View.GONE
            (activity as OnboardQuestionActivity).showBtnVisibility(1)

        }

    }


}