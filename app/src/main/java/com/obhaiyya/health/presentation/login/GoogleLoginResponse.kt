package com.obhaiyya.health.presentation.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.obhaiyya.health.presentation.Package.UserDetails
import java.io.Serializable


class GoogleLoginResponse : Serializable {

    @SerializedName("isExistingUser")
    @Expose
    var isExistingUser: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("success")
    @Expose
    var success: Boolean? = null

    @SerializedName("authToken")
    @Expose
    var authToken: String? = null

    @SerializedName("userDetails")
    @Expose
    var userDetails: UserDetails? = null

}

class UserDetails {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("mobileNumber")
    @Expose
    var mobileNumber: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

    @SerializedName("mobileOTP")
    @Expose
    var mobileOTP: String? = null

    @SerializedName("authToken")
    @Expose
    var authToken: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    var dateOfBirth: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("gender")
    @Expose
    var gender: String? = null

    @SerializedName("height")
    @Expose
    var height: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("weight")
    @Expose
    var weight: Int? = null

    @SerializedName("gmailToken")
    @Expose
    var gmailToken: String? = null

    @SerializedName("accessCode")
    @Expose
    var accessCode: String? = null

}