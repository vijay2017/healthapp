package com.obhaiyya.health.presentation.navigation

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.obhaiyya.health.presentation.Package.PaymentGatewayResponses
import com.obhaiyya.health.presentation.Package.PlanDetails


class GetNavPackageResponse {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("userId")
    @Expose
    var userId: String? = null

    @SerializedName("amount")
    @Expose
    var amount: Int? = null

    @SerializedName("startDate")
    @Expose
    var startDate: String? = null

    @SerializedName("endDate")
    @Expose
    var endDate: String? = null

    @SerializedName("paymentDoneDate")
    @Expose
    var paymentDoneDate: String? = null

    @SerializedName("paymentGatewayResponses")
    @Expose
    var paymentGatewayResponses: PaymentGatewayResponses? = null

    @SerializedName("planDetails")
    @Expose
    var planDetails: PlanDetailsNavResponse? = null

//    @SerializedName("userDetails")
//    @Expose
//    var userDetails: UserDetails? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null

}

class PlanDetailsNavResponse {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("amount")
    @Expose
    var amount: Int? = null

    @SerializedName("validity")
    @Expose
    var validity: Int? = null

    @SerializedName("planName")
    @Expose
    var planName: String? = null

}