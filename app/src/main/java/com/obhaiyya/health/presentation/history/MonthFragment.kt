package com.obhaiyya.health.presentation.history

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_days.*
import kotlin.collections.ArrayList


class MonthFragment : BaseFragment() {

    companion object {

        val KEY_HISTORY_RESPONSE = "keyHistoryResponse"

    }

    override fun getLayout(): Int {
        return R.layout.fragment_month
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val historyResponse = arguments?.getSerializable(KEY_HISTORY_RESPONSE) as HistoryResponse
//
//        setGraph(historyResponse, 0)
//        setGraph(historyResponse, 1)
//        setGraph(historyResponse, 2)
//        setGraph(historyResponse, 3)
//        setGraph(historyResponse, 4)


    }

    fun dataValues(historyResponse: HistoryResponse, pos: Int): ArrayList<Entry> {

        when (pos) {
            0 -> {
                val mDataVal = ArrayList<Entry>()
                historyResponse.heartRate?.mapIndexed { index, heartRateHistory ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            heartRateHistory.value?.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            1 -> {
                val mDataVal = ArrayList<Entry>()
                historyResponse.spo2?.mapIndexed { index, heartRateHistory ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            heartRateHistory.value?.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            2 -> {
                val mDataVal = ArrayList<Entry>()
                historyResponse.respiration?.mapIndexed { index, heartRateHistory ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            heartRateHistory.value?.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            3 -> {
                val mDataVal = ArrayList<Entry>()
                historyResponse.hrv?.mapIndexed { index, heartRateHistory ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            heartRateHistory.value?.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
            4 -> {
                val mDataVal = ArrayList<Entry>()
                historyResponse.stress?.mapIndexed { index, heartRateHistory ->
                    mDataVal.add(
                        Entry(
                            index.toFloat(),
                            heartRateHistory.value?.toFloat() ?: 0.0F
                        )
                    )
                }
                return mDataVal
            }
        }
        val mDataVal = ArrayList<Entry>()
        historyResponse.heartRate?.mapIndexed { index, heartRateHistory ->
            mDataVal.add(
                Entry(
                    index.toFloat(),
                    heartRateHistory.value?.toFloat() ?: 0.0F
                )
            )
        }
        return mDataVal
    }

    fun setGraph(historyResponse: HistoryResponse, pos: Int) {

        when (pos) {
            0 -> {
                val mXAxisList = ArrayList<String>()
                historyResponse?.heartRate?.map {
                    it.date?.let { value ->
                        mXAxisList.add(value)
                    }
                }
                lineChart(mXAxisList, lineChartHeart, pos, historyResponse)
            }
            1 -> {
                val mXAxisList = ArrayList<String>()
                historyResponse?.spo2?.map {
                    it.date?.let { value ->
                        mXAxisList.add(value)
                    }
                }
                lineChart(mXAxisList, lineChartSpo2, pos, historyResponse)
            }
            2 -> {
                val mXAxisList = ArrayList<String>()
                historyResponse?.respiration?.map {
                    it.date?.let { value ->
                        mXAxisList.add(value)
                    }
                }
                lineChart(mXAxisList, lineChartRespiration, pos, historyResponse)
            }
            3 -> {
                val mXAxisList = ArrayList<String>()
                historyResponse?.hrv?.map {
                    it.date?.let { value ->
                        mXAxisList.add(value)
                    }
                }
                lineChart(mXAxisList, lineChartSdnn, pos, historyResponse)
            }
            4 -> {
                val mXAxisList = ArrayList<String>()
                historyResponse?.stress?.map {
                    it.date?.let { value ->
                        mXAxisList.add(value)
                    }
                }
                lineChart(mXAxisList, lineChartStress, pos, historyResponse)
            }
        }
    }

    fun lineChart(
        mXAxisList: ArrayList<String>,
        lineChartDays: LineChart,
        pos: Int,
        historyResponse: HistoryResponse
    ) {
        var dataSets: ArrayList<ILineDataSet?> = ArrayList()

        val incomeEntries: List<Entry> = dataValues(historyResponse, pos)
        dataSets = ArrayList()
        val set1: LineDataSet

        set1 = LineDataSet(incomeEntries, "Health")
        set1.color = Color.rgb(65, 168, 121)
        set1.valueTextColor = Color.rgb(55, 70, 73)
        set1.valueTextSize = 20f
        set1.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
        dataSets.add(set1)

        lineChartDays.setTouchEnabled(true)
        lineChartDays.setScaleEnabled(false)
        lineChartDays.extraLeftOffset = 25f
        lineChartDays.extraRightOffset = 25f
        lineChartDays.extraTopOffset = 15f

        val rightYAxis = lineChartDays.axisRight
        rightYAxis.isEnabled = false
        val leftYAxis = lineChartDays.axisLeft
        leftYAxis.isEnabled = false
        val xAxis = lineChartDays.xAxis
        xAxis.textSize = 15f
        xAxis.granularity = 1f
        xAxis.isEnabled = true
        lineChartDays.axisRight.setDrawAxisLine(true)
        xAxis.setDrawGridLines(false)
        xAxis.position = XAxis.XAxisPosition.TOP

        set1.lineWidth = 4f
        set1.circleRadius = 3f
        set1.circleHoleColor = resources.getColor(R.color.colorPrimary)
        set1.setCircleColor(resources.getColor(R.color.colorPrimary))
        lineChartDays.xAxis.valueFormatter = IndexAxisValueFormatter(mXAxisList)

        val data = LineData(dataSets)
        lineChartDays.data = data
        lineChartDays.animateX(3000)
        lineChartDays.invalidate()
        lineChartDays.legend.isEnabled = false
        lineChartDays.description.isEnabled = false
    }

}


