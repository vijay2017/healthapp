package com.obhaiyya.health.presentation.navigation

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.RadioGroup
import com.google.gson.Gson
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseActivity
import com.obhaiyya.health.presentation.Package.UserDetails
import com.obhaiyya.health.presentation.userdetails.UserDetailsRequest
import com.obhaiyya.health.presentation.userdetails.UserRegistrationGmailResponse
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_profile.*
import org.joda.time.format.ISODateTimeFormat
import java.util.*

class ProfileHamburgerActivity : BaseActivity(), View.OnClickListener,
        RadioGroup.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    private var _day = 0
    private var _month = 0
    private var _birthYear = 0
    var mGender = "Male"
    var isAccessCodeSuccess = false
    var name = ""
    var height = ""
    var weight = ""
    var dob = ""
    var email = ""
    var isDateChanged = false
    var shouldEditProfile = false

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, ProfileHamburgerActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setToolbarWithTitle("Profile", false)
        btnEditProfile.setOnClickListener(this)
        btnUserContinueProfile.setOnClickListener(this)
        edtDOB.setOnClickListener(this)
        rdGrpGender.setOnCheckedChangeListener(this)

        val userDetailJson =
                SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_DETAILS)
        val jsonUserDetails =
                Gson().fromJson(userDetailJson, UserRegistrationGmailResponse::class.java)
        txtNameUserRegis.setText(jsonUserDetails.name ?: "")

        val date = ISODateTimeFormat.dateTimeParser().parseDateTime(jsonUserDetails.dateOfBirth)
        val formattedDate = date.toString("dd/MM/yyyy")

        edtDOB.setText("" + formattedDate)
        txtWeightUserRegis.setText("" + jsonUserDetails.weight ?: "")
        txtHeightUserRegis.setText("" + jsonUserDetails.height)

        when (jsonUserDetails.gender) {
            "Male" -> {
                rdBtnMale.isChecked = true
            }
            "Female" -> {
                rdBtnFeMale.isChecked = true
            }
            "Others" -> {
                rdBtnOthers.isChecked = true
            }
        }


    }

    fun loadUserRegistration() {
        showProgress(this)
        val userDetailsRequest = UserDetailsRequest()
        userDetailsRequest.name = txtNameUserRegis.text.toString()
        userDetailsRequest.email =
                SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL) ?: ""
        userDetailsRequest.gender = mGender
        userDetailsRequest.dateOfBirth =
                DateFormatUtils.getSelectedDateInUTCFormat(edtDOB.text.toString() ?: "") ?: ""
        userDetailsRequest.height = txtHeightUserRegis.text.toString()
        userDetailsRequest.weight = txtWeightUserRegis.text.toString()
        userDetailsRequest.accessCode =
                SharedPreferenceManager.getString(SharedPreferenceManager.KEY_ACCESS_CODE) ?: ""

        val userID = SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""

        mHealthService?.updateUserDetails(userID, userDetailsRequest)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    it?.let {
                        saveUserDetails(it)
                    }
                    hideProgress()
                    setEnabledAllViews(false)
                    showToastMessage("Success")
                }, {
                    hideProgress()
                    onUserRegistrationError(it)
                })

    }

    private fun saveUserDetails(userDetails: UserDetails) {
        val userDetailsJson = Gson().toJson(userDetails)
        SharedPreferenceManager.saveString(
                SharedPreferenceManager.KEY_USER_DETAILS,
                userDetailsJson
        )
    }

    private fun onUserRegistrationError(it: Throwable?) {
        Log.d("VIjay-error", "" + it?.message)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnEditProfile -> {
                setEnabledAllViews(true)
            }
            R.id.btnUserContinueProfile -> {
                name = txtNameUserRegis.text.toString()
                height = txtHeightUserRegis.text.toString()
                weight = txtWeightUserRegis.text.toString()
                dob = edtDOB.text.toString()
                email =
                        SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_EMAIL)
                                ?: ""
                if (name.isEmpty()) {
                    showToastMessage("Name cannot be empty")
                } else if (height.isEmpty()) {
                    showToastMessage("Height cannot be empty")
                } else if (weight.isEmpty()) {
                    showToastMessage("Weight cannot be empty")
                } else if (dob.isEmpty()) {
                    showToastMessage("Dob cannot be empty")
                } else {
                    loadUserRegistration()
                }
            }
            R.id.edtDOB -> {
                if (shouldEditProfile) {
                    val calendar: Calendar = Calendar.getInstance(TimeZone.getDefault())
                    val dialog = DatePickerDialog(
                            this, this,
                            calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)
                    )
                    dialog.show()
                }
            }
        }
    }


    override fun onDateSet(p0: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        _birthYear = year;
        _month = monthOfYear;
        _day = dayOfMonth;
        updateDisplay();
    }

    private fun updateDisplay() {
        val dob = StringBuilder() // Month is 0 based so add 1
                .append(_day).append("/").append(_month + 1).append("/").append(_birthYear)
                .toString()
        edtDOB.setText(dob)
        formatDate()
    }

    private fun formatDate(): String {
        isDateChanged = true
        val month = _month + 1
        val day = _day + 1
        val date = "" + day + "/" + month + "/" + _birthYear
        return DateFormatUtils.getSelectedDateInUTCFormat(date) ?: ""
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rdBtnMale -> {
                Log.d("VIjay-error", "Male")
                mGender = "Male"
            }
            R.id.rdBtnFeMale -> {
                mGender = "Female"
                Log.d("VIjay-error", "Female")
            }
            R.id.rdBtnOthers -> {
                mGender = "Others"
                Log.d("VIjay-error", "Others")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    fun setEnabledAllViews(isValue: Boolean) {
        txtNameUserRegis.isEnabled = isValue
        txtWeightUserRegis.isEnabled = isValue
        txtHeightUserRegis.isEnabled = isValue
        rdBtnMale.isEnabled = isValue
        rdBtnFeMale.isEnabled = isValue
        rdBtnOthers.isEnabled = isValue
        shouldEditProfile = isValue
        if (isValue) {
            btnUserContinueProfile.visibility = View.VISIBLE
        } else {
            btnUserContinueProfile.visibility = View.GONE
        }
    }


}