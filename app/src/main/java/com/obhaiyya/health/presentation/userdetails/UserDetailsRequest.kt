package com.obhaiyya.health.presentation.userdetails

class UserDetailsRequest {

    var mobileNumber = ""
    var name = ""
    var gender = ""
    var dateOfBirth = ""
    var weight = ""
    var height = ""
    var email = ""
    var accessCode = ""

}