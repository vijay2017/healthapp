package com.obhaiyya.health.presentation.utils

class Constant {
    companion object {
        val NOTIFICATION_ID = "notif_id"
        val KEY_ACTION = "action_todo"
        val RX_ACTION = "detect_notification_action"
        val IS_INTENT_FROM_NOTIF = "intent_from_notif"
    }
}