package com.obhaiyya.health.presentation.restbottomsheet

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import androidx.fragment.app.DialogFragment
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseBottomSheet
import kotlinx.android.synthetic.main.bottom_sheet_rest_or_walk.*
import kotlinx.android.synthetic.main.bs_post_health_question.*

class RestOrWalkBottomSheet : BaseBottomSheet(), View.OnClickListener {

    var mIRestOrWalkHandler: IRestOrWalkHandler? = null
    var lossOfTaste = false
    var mAmountSelected = 0.0


    companion object {

        val KEY_AMOUNT_SELECTED = "keyAmountSelected"

        fun newInstance(): RestOrWalkBottomSheet {
            val paymentBottomSheet = RestOrWalkBottomSheet()
            return paymentBottomSheet
        }

    }

    override fun getLayout(): Int {
        return R.layout.bottom_sheet_rest_or_walk
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.BottomSheetDialog)
        isCancelable = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        llRestBtn.setOnClickListener(this)
        llWorkoutBtn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.llRestBtn -> {
                setRestOrWalk(true)
            }
            R.id.llWorkoutBtn -> {
                setRestOrWalk(false)
            }
        }
    }


    fun setRestOrWalk(status: Boolean) {
        mIRestOrWalkHandler?.onStatusSelectedSuccess(status)
        dismiss()
    }

    interface IRestOrWalkHandler {
        fun onStatusSelectedSuccess(status: Boolean)
    }

    fun checkAllAnswered(): Boolean {
        val answer1 = edtTemparature.text.toString().isNotEmpty()
        val answer2 = rdBtnSmellYes.isChecked || rdBtnSmellNo.isChecked
        return answer1 && answer2
    }

}

