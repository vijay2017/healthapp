package com.obhaiyya.health.presentation.navigation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.utils.DateFormatUtils

class NavPackageAdapter(
    val mContext: Context,
    val mGetAllHealthDataRes: List<GetNavPackageResponse>
) :
    RecyclerView.Adapter<NavPackageAdapter.DashboaordViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboaordViewHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.adapter_nav_package, parent, false)
        return DashboaordViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashboaordViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mGetAllHealthDataRes.size
    }

    inner class DashboaordViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val textEndDate = view.findViewById<TextView>(R.id.txtEndDateNavPackage)
        val txtPlanNavDetails = view.findViewById<TextView>(R.id.txtPlanNavDetails)
        val txtPlanNavDetailsprice = view.findViewById<TextView>(R.id.txtPlanNavDetailsprice)

        fun bind(position: Int) {

            textEndDate.text =
                DateFormatUtils.convertInDDMMYYYY(mGetAllHealthDataRes.get(position).endDate)

            val validity = mGetAllHealthDataRes.get(position).planDetails?.validity
            val amount = mGetAllHealthDataRes.get(position).planDetails?.amount
            when (validity) {
                30 -> {
                    txtPlanNavDetails.text = "Monthly"
                    txtPlanNavDetailsprice.text = "₹ " + amount
                }
                365 -> {
                    txtPlanNavDetails.text = "Yealry"
                    txtPlanNavDetailsprice.text = "₹ " + amount
                }
            }

        }

    }

}