package com.obhaiyya.health.presentation.posthealthquestion

class CheckRiskRequest {
    var lossOfTaste = false
    var spo2 = 0
    var heartRate = 0
    var temperature = 101
    var stress = 0
    var respiration = 0
    var hrv = 0
}