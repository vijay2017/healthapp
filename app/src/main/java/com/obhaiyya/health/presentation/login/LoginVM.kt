package com.example.obhaiyyahealth.presentation.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.appwithmeflutter.mype.presentation.base.BaseVM
import com.obhaiyya.health.presentation.login.GoogleLoginResponse
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Created by Vijay on 22/3/20.
 */

class LoginVM : BaseVM() {

    var mMutableGenerateOTP = MutableLiveData<LoginGenerateOTPModel>()
    var mMutableValidateOTP = MutableLiveData<ValidateOTPModel>()
    var mMutableRegisterGmail = MutableLiveData<RegisterGmailModel>()


    fun generateOTP(mobileNumber: String) {
        val mLoginOTPModel = LoginGenerateOTPModel()
        mCustomerService?.generateMobileNumber(mobileNumber)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                Log.d("VIJAY", "SUCCESS")
                mLoginOTPModel.isSuccess = true
                mLoginOTPModel.otp = it.otp
                mMutableGenerateOTP.value = mLoginOTPModel
            }, {
                Log.d("VIJAY", "ERROR")
                mLoginOTPModel.isSuccess = false
                mMutableGenerateOTP.value = mLoginOTPModel
            })
    }

    fun validateOTP(mobileNumber: String, otp: String) {
        val validateOTPModel = ValidateOTPModel()
        mCustomerService?.validateOTP(mobileNumber, otp)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                Log.d("VIJAY", "SUCCESS-OTP")
                validateOTPModel.isExistingUser = it.isExistingUser ?: false
                validateOTPModel.success = it.success ?: false
                mMutableValidateOTP.value = validateOTPModel
            }, {
                Log.d("VIJAY", "ERROR-OTP")
                validateOTPModel.isExistingUser = false
                validateOTPModel.success = false
                mMutableValidateOTP.value = validateOTPModel
            })
    }

    fun registerGmailUser(gmailId: String, gToken: String) {
        val registerGmail = RegisterGmailModel()
        mCustomerService?.registerGmailApi(gmailId, gToken)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                Log.d("VIJAY", "SUCCESS-OTP")
                registerGmail.success = true
                registerGmail.mGmailResponse = it
                mMutableRegisterGmail.value = registerGmail
            }, {
                registerGmail.success = false
                mMutableRegisterGmail.value = registerGmail
                Log.d("VIJAY", "ERROR-OTP")
            })
    }

}

class LoginGenerateOTPModel {
    var isSuccess: Boolean = false
    var otp: String? = null
}

class ValidateOTPModel {
    var isExistingUser: Boolean = false
    var success: Boolean? = null
}

class RegisterGmailModel {
    var success: Boolean? = null
    var mGmailResponse: GoogleLoginResponse? = null

}