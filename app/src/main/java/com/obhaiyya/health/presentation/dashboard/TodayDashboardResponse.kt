package com.obhaiyya.health.presentation.dashboard

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TodayDashboardResponse {

    @SerializedName("spo2")
    @Expose
    var spo2: String? = null

    @SerializedName("mortalityRisk")
    @Expose
    var mortalityRisk: Int? = null

    @SerializedName("heartRate")
    @Expose
    var heartRate: String? = null

    @SerializedName("hrv")
    @Expose
    var hrv: String? = null

    @SerializedName("stress")
    @Expose
    var stress: String? = null

    @SerializedName("respiration")
    @Expose
    var respiration: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("isRest")
    @Expose
    var isRest: Boolean? = null


}