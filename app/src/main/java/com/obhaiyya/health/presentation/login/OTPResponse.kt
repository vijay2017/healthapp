package com.obhaiyya.health.presentation.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OTPResponse : Serializable {

    @SerializedName("isExistingUser")
    @Expose
    var isExistingUser: Boolean? = null

    @SerializedName("success")
    @Expose
    var success: Boolean? = null


}