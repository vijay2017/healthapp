package com.obhaiyya.health.presentation.history

import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.tabs.TabLayout
import com.obhaiyya.health.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.obhaiyya.health.presentation.base.BaseFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_history_child_holder.*
import java.lang.Exception


class HistoryChildHolderFragment : BaseFragment() {

    var mHistoryResponse: TrendsLogsResponse? = null

    override fun getLayout(): Int {
        return R.layout.fragment_history_child_holder
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            showProgress(it)
        }

        val firstTab: TabLayout.Tab = tabHistoryChild.newTab()
        firstTab.text = "7 days"
        tabHistoryChild.addTab(firstTab)
        val secondTab: TabLayout.Tab = tabHistoryChild.newTab()
        secondTab.text = "Last 14 days"
        tabHistoryChild.addTab(secondTab)
        val thirdTab: TabLayout.Tab = tabHistoryChild.newTab()
        thirdTab.text = "Last 21 days"
        tabHistoryChild.addTab(thirdTab)

        getHistory("7", 0)

        tabHistoryChild.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                Log.d("VIJAY---", "onTabReselected" + tab?.id)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                Log.d("VIJAY---", "onTabUnselected" + tab?.id)

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                Log.d("VIJAY---", "Child 0 position" + tab?.id)
                when (tab?.position) {
                    0 -> {
                        Log.d("onTabSelected", "Child 0 position")
                        getHistory("7", 0)
                    }
                    1 -> {
                        Log.d("onTabSelected", "Child 1 position")
                        getHistory("15", 1)
                    }
                    2 -> {
                        Log.d("onTabSelected", "Child 1 position")
                        getHistory("30", 2)
                    }
                }
            }
        })


    }

    fun getHistory(noOfDays: String, pos: Int) {
        val userId =
            SharedPreferenceManager.getString(SharedPreferenceManager.KEY_USER_ID) ?: ""
        mCustomerService?.getGroupsTrendsGraph(userId, noOfDays)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe({
                hideProgress()
                when (pos) {
                    0 -> {
                        replaceChildFragment(
                            R.id.frame_child_history,
                            DaysFragment.newInstance(it)
                        )
                        mHistoryResponse = it
                    }
                    1 -> {
                        replaceChildFragment(
                            R.id.frame_child_history,
                            FifteenFragment.newInstance(it)
                        )
                        mHistoryResponse = it
                    }
                    2 -> {
                        replaceChildFragment(
                            R.id.frame_child_history,
                            ThirtyFragment.newInstance(it)
                        )
                        mHistoryResponse = it
                    }
                }
            }, {
                hideProgress()
            })


    }

}