package com.obhaiyya.health.presentation.history

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.utils.DateFormatUtils
import org.joda.time.format.ISODateTimeFormat
import java.util.*


class LogsListAdapter(
    val mContext: Context,
    val mLogHealthList: List<HealthLogs>?
) :
    RecyclerView.Adapter<LogsListAdapter.DashboaordViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboaordViewHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.adapter_list_logs, parent, false)
        return DashboaordViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashboaordViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mLogHealthList?.size ?: 0
    }

    inner class DashboaordViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val hearRate = view.findViewById<TextView>(R.id.txtHeartRate)
        val spo2 = view.findViewById<TextView>(R.id.txtSpo2)
        val respiration = view.findViewById<TextView>(R.id.txtRespiration)
        val sdnn = view.findViewById<TextView>(R.id.txtSDNN)
        val stress = view.findViewById<TextView>(R.id.txtStress)
        val logsTime = view.findViewById<TextView>(R.id.logsTime)

        fun bind(position: Int) {

            val data = mLogHealthList?.get(position)
            logsTime.text = DateFormatUtils.convertInDateTime(data?.createdAt ?: "")
            hearRate.text = data?.heartRate.toString()
            spo2.text = data?.spo2.toString()
            respiration.text = data?.respiration.toString()
            sdnn.text = data?.hrv.toString()
            stress.text = data?.stress.toString()

        }

    }

}