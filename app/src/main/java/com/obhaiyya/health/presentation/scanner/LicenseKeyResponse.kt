package com.obhaiyya.health.presentation.scanner

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LicenseKeyResponse : Serializable {

    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("appVersion")
    @Expose
    var appVersion: String? = null

    @SerializedName("displayEvent")
    @Expose
    var displayEvent: Boolean? = null

    @SerializedName("deliveryCharge")
    @Expose
    var deliveryCharge: Int? = null

    @SerializedName("aprtDeliveryCharge")
    @Expose
    var aprtDeliveryCharge: Int? = null

    @SerializedName("binahLicenseKey")
    @Expose
    var binahLicenseKey: String? = null

    @SerializedName("createdAt")
    @Expose
    var createdAt: String? = null

    @SerializedName("updatedAt")
    @Expose
    var updatedAt: String? = null


}