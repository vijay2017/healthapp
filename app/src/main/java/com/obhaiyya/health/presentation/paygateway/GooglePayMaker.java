 package com.obhaiyya.health.presentation.paygateway;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import androidx.fragment.app.Fragment;

import com.facebook.stetho.okhttp3.BuildConfig;
import com.obhaiyya.health.HealthApplication;
import com.obhaiyya.health.data.SharedPreferenceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GooglePayMaker {


    public static final int TEZ_REQUEST_CODE = 123;

    private static final boolean IS_TEST_MODE = BuildConfig.DEBUG;
    private static final String GOOGLE_TEZ_PACKAGE_NAME = "com.google.android.apps.nbu.paisa.user";
    public static final String SCHEME_UPI = "upi";
    public static final String AUTHORITY_PAY = "pay";
    private Fragment fragCtx;
    private IGooglePay iGooglePayCallback;
    private GpayMakerCallback uiCallback;
    private double paymentAmt;
    private Boolean mFlagProMaid = false;
    private Activity actCtx;

    public GooglePayMaker(GpayMakerCallback uiCallback, Fragment fragCtx) {
        this.uiCallback = uiCallback;
        this.fragCtx = fragCtx;
    }

    public GooglePayMaker(GpayMakerCallback uiCallback, Activity actCtx) {
        this.uiCallback = uiCallback;
        this.actCtx = actCtx;
        this.iGooglePayCallback = (IGooglePay) actCtx;
    }


    public GooglePayMaker(IGooglePay iGooglePayCallback) {
        this.iGooglePayCallback = iGooglePayCallback;
    }

    public double getPaymentAmt() {
        return paymentAmt;
    }


    public void getTxRefIdAndOpenGpayApp(String txDesc, double paymentAmt) {
        if (HealthApplication.Companion.getAppInstance().isNetworkConnected()) {
            this.paymentAmt = paymentAmt;
            String userId = SharedPreferenceManager.INSTANCE.getString(SharedPreferenceManager.INSTANCE.getKEY_USER_ID());
            CloserByRestClient closerByRestClient = new CloserByRestClient();
            closerByRestClient.getTxIdForGpay(userId, paymentAmt, txIdListener(txDesc, paymentAmt));
        } else {
            this.uiCallback.onNoNetworkFoundForGpay(null, new Throwable(CloserByRestClient.NETWORK_UNAVAILABLE));
        }
    }

    private Callback<TokenOtherDataModel> txIdListener(final String txDesc, final double paymentAmt) {
        return new Callback<TokenOtherDataModel>() {
            @Override
            public void onResponse(Call<TokenOtherDataModel> call, Response<TokenOtherDataModel> response) {
                if (response.isSuccessful()) {
                    HealthApplication.Companion.showToast("Token Fetched Successfully");
                    String txRefId = response.body().getOrderId();
                    if (fragCtx != null) {
                        uiCallback.onTxRefIdFetched(txRefId);
                    } else if (actCtx != null) {
                        uiCallback.onTxRefIdFetched(txRefId);
                    }
                    openGooglePayApp(txRefId, txDesc, String.valueOf(paymentAmt));
                } else {
                    HealthApplication.Companion.showToast("Payment Failed");
                    if (fragCtx != null) {
                        uiCallback.onErrorFetchingTxRefId();
                    } else if (actCtx != null) {
                        uiCallback.onErrorFetchingTxRefId();
                    }

                }
            }

            @Override
            public void onFailure(Call<TokenOtherDataModel> call, Throwable t) {
               /* if (mFlagProMaid) {
                    iGooglePayCallback.onErrorFetchingTxRefId();
                } else {
                    uiCallback.onErrorFetchingTxRefId();
                }*/
                if (fragCtx != null) {
                    uiCallback.onErrorFetchingTxRefId();
                } else if (actCtx != null) {
                    uiCallback.onErrorFetchingTxRefId();
                }

            }
        };
    }


    private void openGooglePayApp(String txRefId, String txDesc, String txAmt) {
        try {
            Uri uri = new Uri.Builder()
                    .scheme(SCHEME_UPI)
                    .authority(AUTHORITY_PAY)
                    .appendQueryParameter(JSONConstants.PAYEE_VPA_KEY, JSONConstants.PAYEE_VPA_VALUE)
                    .appendQueryParameter(JSONConstants.PAYEE_NAME_KEY, JSONConstants.PAYEE_NAME_VALUE)
                    .appendQueryParameter(JSONConstants.PAYEE_MERCHANT_CODE_KEY, JSONConstants.PAYEE_MERCHANT_CODE_VALUE)
                    .appendQueryParameter(JSONConstants.PAYEE_TX_REF_ID_KEY, txRefId)
                    .appendQueryParameter(JSONConstants.PAYEE_TX_DESC_KEY, txDesc)
                    .appendQueryParameter(JSONConstants.PAYEE_TX_AMT_KEY, txAmt)
                    .appendQueryParameter(JSONConstants.PAYEE_CURR_KEY, JSONConstants.PAYEE_CURR_VALUE)
                    .build();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(uri);
            intent.setPackage(GOOGLE_TEZ_PACKAGE_NAME);


            if (fragCtx != null) {
                fragCtx.startActivityForResult(intent, TEZ_REQUEST_CODE);
            } else if (actCtx != null) {
                actCtx.startActivityForResult(intent, TEZ_REQUEST_CODE);
            }

        } catch (Exception e) {
            if (actCtx != null) {
                Uri uri = Uri.parse("market://details?id=com.google.android.apps.nbu.paisa.user");
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                actCtx.startActivity(goToMarket);
            }
        }

    }

}
