package com.obhaiyya.health.presentation.paygateway

import android.content.Intent
import retrofit2.Call

/**
 * Created by Vijay on 31/5/20.
 */
interface IGooglePay {

    fun onGooglePayCallback(data : Intent)

    fun onNoNetworkFoundForGpay(call: Call<*>?, t: Throwable?)

    fun onTxRefIdFetched(txId: String?)

    fun onErrorFetchingTxRefId()

}