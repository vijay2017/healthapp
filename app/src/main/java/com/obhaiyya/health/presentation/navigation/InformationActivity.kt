package com.obhaiyya.health.presentation.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.obhaiyya.health.R
import com.obhaiyya.health.presentation.base.BaseActivity

class InformationActivity : BaseActivity() {

    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, InformationActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information)
        setToolbarWithTitle("Information", false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}